var eventBroker
$(function(){
	eventBroker = _({}).extend(require('chaplin/lib/event_broker'));
	eventBroker.publishEvent("#fetch", { type : 'state' }, function(state) {
		fetchCallBack(state);
	});
	eventBroker.subscribeEvent("#doSave", function(message){
		console.log('oe!');
		eventBroker.publishEvent("#save",saveCallBack());
	})
	eventBroker.subscribeEvent("#finish", function(message){
		console.log('finishCallBack');
		eventBroker.publishEvent("#finish",finishCallBack());
	})
});
