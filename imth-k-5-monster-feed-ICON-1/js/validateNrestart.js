/*********************************validateNrestart.js**************************
******************************************************************************
**/

var myScore= myScore || {};

myScore.service={
    Score:0,
    value: 0,
    trueAnswer: 0,
setScore:function(score){
    if((myScore.service.Score+score)>=0)
        myScore.service.Score+=score;
},

validate: function(draggable){
	myScore.service.value=draggable.find(".bubble").html();
    var score1=0;
    if(MathOperations.builder.getAnswer()===parseInt(myScore.service.value)){
      $(".box").each(function( index, value ){
    	$(this).addClass("hide");      	
      })
        score1++;
        this.trueAnswer=String($('#oparation').find('h2').html())
        myScore.service.timeOutMouth('.bocaEating', '.eating')
        draggable.addClass("hide");
        myApp.audioX.setAudio('#audioTrue')
        myApp.audioX.playA()
        setTimeout(function(){
		draggable.removeClass("hide");
		}, 500);
        myScore.service.playAgainEvent();
        value=0;
		setTimeout(function(){
			myScore.service.removeMonsterAndFruits();
			myScore.service.displayCongratulationsMessage();
		}, 500);
		         
    }else{
        score1--;
       myScore.service.timeOutMouth('.bocaEww', '.wrong')
       myApp.audioX.setAudio('#audioFalse')
       myApp.audioX.playA()
      draggable.removeClass('dragDropHidden').addClass('dragDropVisible')

    }

     myScore.service.setScore(score1);
     $("#total").html(myScore.service.Score);
},
 timeOutMouth: function (replace1, replace2) {
    $('.bocaNormal').hide();
    $('.face.normal').hide();
    $(replace1).show();
    $('.face' + replace2).show();
    setTimeout(function () {
      $(replace1).hide();
      $('.face' + replace2).hide();
      $('.face.normal').show();
      $('.bocaNormal').show();
    }, 500)
},

restart:function(isItAgain){
	
		if(typeof options === "undefined"){
			var container = $(".box");
			container.each(function( index, value ){  
				$(this).removeClass().addClass("box");
				$(this).addClass("hide");
				$(this).find(".fondo").removeClass().addClass("fondo");
				$(this).find(".fruitHolder").removeClass().addClass("fruitHolder");
				$(this).find(".bubble").html();       
			});		
			if(isItAgain){
				setTimeout(function(){
					startGame();
				},500);
			}
		}
		MathOperations.builder.generateMathOperations(2);
	
	    
},

displayCongratulationsMessage: function(){
	var divCongrat = $('div').filter('#congratFeedback');
	 $('div').filter('#startGame').hide();
	divCongrat.removeClass('congratFeedback-hide')
	divCongrat.find('span').filter('.score').html(myScore.service.trueAnswer + ' = '+eval(myScore.service.trueAnswer));
	myScore.service.removeMonsterAndFruits()
    $('div#congratFeedback').click(function () {
    divCongrat.addClass('congratFeedback-hide')
      setTimeout(function () {
        myScore.service.playAgainEvent()
      }, 500)
    })
},

removeMonsterAndFruits: function(){
	myApp.values.pearsonAppAudioFlag = false
    myApp.audioX.setAudio('#audioBack')
    myApp.audioX.stopA()
	$('div').filter('#sign').removeClass('signAppear');
	myScore.service.restart(false);
	$('div').filter('.MonsterHolder').hide();
},

playAgainEvent:function(){
		setTimeout(function(){
			$('div').filter('#startGame').removeClass('hide')
			$('div').filter('.MonsterHolder').show(function(){
				$('div').filter('#sign').addClass('signAppear');
				$('div').filter('#startGame').addClass('signAppear');
			});
			
			$('div').filter('#startGame').filter('.go').show();
		},500);
}

};

$(document).ready(function(){
	$('div').filter('#sign').addClass('signAppear');
});