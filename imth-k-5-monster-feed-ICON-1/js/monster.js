/*************************Monster.js ( main )***************************************
*************************************************************************
**/

$(document).on("ready",buildGame);

var myApp = myApp || {};
total = 0;
classNames = ["banana", "carrots", "cheese", "cherries", "grapes", "kiwis", "meats", "milks", "oranges", "strawberries", "tires", "tomatoes", "watermelons"];

myApp.values = {
  pearsonAppAudioFlag: false
};

myApp.audioX = {  /** firset set the auido ID same as in the html an then use play,pause, stop*/
  $audio: $('#audio')[0], // default
  setAudio: function (audioName) {
    this.$audio = $(audioName)[0]
  },
  playA: function () {
    this.$audio.load()
    this.$audio.play()
  },
  stopA: function () {
    this.$audio.pause()
    this.$audio.currentTime = 0
  },
  pauseA: function () {
    this.$audio.load()
    this.$audio.pause()
  },
  getTimeA: function () {
    return this.$audio.currentTime
  }
}

myApp.modal = {
show: function () {
    $('.box').css('animation-play-state', 'paused')
    myApp.audioX.setAudio('#audioBack')
    myApp.audioX.pauseA() 
    $("#_black").css("display","block")
    myApp.audioX.setAudio('#audio')
    myApp.audioX.playA()
    $('._iModalBody img').click(function(){
      myApp.audioX.playA()
    });   
},
hide: function () {
    $('.box').css('animation-play-state', 'running')
    $("#_black").css("display","none")
     myApp.audioX.setAudio('#audioBack')
    if ((myApp.audioX.getTimeA() === 0) && (myApp.values.pearsonAppAudioFlag)) { /** time of audio is <> 0 in pearson app so added a flag */
      myApp.audioX.playA()
    }
    myApp.audioX.setAudio('#audio')
    myApp.audioX.stopA()
  }
}

function buildGame(){    
        myApp.modal.show(); 
    /*************** close PopUp ***********************/
    $("#icoClose").click(function(){
        myApp.modal.hide();
        $("#cloudUno").addClass("cloudMoveLeft");        
    }); 
    $("#startGame").click(function(){
        myApp.values.pearsonAppAudioFlag = true
        $(this).hide();
        startGame();
        myApp.audioX.setAudio('#audioBack')
        myApp.audioX.playA()
    });

    $(".contentFeedback .btInfo").click(function(){
        myApp.modal.show();
    });

    $(".contentFeedback .icoReset").click(function(){
        myScore.service.removeMonsterAndFruits();
        myScore.service.playAgainEvent();
        $(".icoReset").addClass("icoResetMove");
        $('div').filter('#congratFeedback').addClass('congratFeedback-hide');
        setTimeout(function(){
           $(".icoReset").removeClass("icoResetMove");
         },400); 
    });
                
    myDragDrop.service.drag(".ui-draggable");
    myDragDrop.service.drop(".monstercito");
}
function startGame(){
    var container = $(".box");
    var fruitsN = Array();
    var  fruitN = Array();
    container.each(function( index, value ){
        var numberFruit = myRandom.service.getRandomClass(1,10,fruitsN);
        var fruitNameN = myRandom.service.getRandomClass(0,11,fruitN);       
        var fruitName = classNames[fruitNameN]; 
        $(this).removeClass("hide");
        $(this).find(".fondo")
          .addClass(fruitName)
          .addClass(fruitName+"-"+ numberFruit)
          .addClass('fondoShake')
        $(this).find(".fruitHolder").addClass("fruitHolder-"+fruitName).addClass(fruitName+"-"+ numberFruit);
        $(this).find(".bubble").html(numberFruit);
        $(this).addClass("move"+(index + 1));
        if(index >= 9)
            fruitsN.length = 0;            
        fruitsN.push(numberFruit);        
        fruitN.push(fruitNameN);
    });
}

