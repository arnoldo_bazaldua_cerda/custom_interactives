/* DragDrop.js using  Jquery-UI */
var myDragDrop = myDragDrop || {}
myDragDrop.values = {}
myDragDrop.service = {
  drag: function (draggableSelector) {
    $(draggableSelector).draggable({
      helper: function () {
        var clone = $(this).clone()
        clone
          .addClass('bigger')
          .parent()
          .removeAttr()
          .removeClass()
        clone
            .find('.fondo')
            .removeClass('fondoShake')
        return clone
      },
      /* hides the real element when dragging */
      start: function (event, ui) {
        $(this).removeClass('dragDropVisible').addClass('dragDropHidden')
        /* invisible method is sset in the JqPluggins */
      },
      appendTo: 'section',
      containment: '#mainHolder',
      stop: function (event, ui) {
        $(this).removeClass('dragDropHidden').addClass('dragDropVisible')
      }
    })
  },
  drop: function (DroppableSelector) {
    $(DroppableSelector).droppable({
      drop: function (event, ui) {
        myScore.service.validate(ui.draggable)
      }
    })
  }
}
