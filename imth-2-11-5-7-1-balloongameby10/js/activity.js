(function () {
  var stars = 0
  var nextLevel = 1
  var currentLevel = 1
  var answerInterval = 10
  var currentAnswer = answerInterval
  var clickedBalloon = false
  var balloonWaveTimer
  var levelScores = [0, 0, 0, 0, 0]
  var balloonColor = ['balloonBlue', 'balloonRed', 'balloonGreen', 'balloonOrange', 'balloonViolet', 'balloonYellow']
  var balloons = [1, 2, 3, 4, 5, 6]
  var num = 0
  var showBalloons
  var last = 0
  var iteracion = 0
  var pause = false
  
  $('.stars').text(stars)
 
  removeClickResetIcon()
    // Math.random() * 20+1
  function setAnswers () {
    for (var i = 0; i <= 5; i++) {
      $('#balloon' + (i + 1) + ' .balloonHead').html(parseInt((Math.random() * (answerInterval+10) + (currentAnswer-5)), 10))
    }
    console.log(parseInt(Math.random() * 6 + 1, 10))
    $('#balloon' + parseInt(Math.random() * 6 + 1, 10) + ' .balloonHead').html(currentAnswer)
  }
 
  $(document).ready(function () {
    $('.pausePlay').click(function () {
      switch ($(this).children('a').text()) {
        case 'Start':
          playGame()
          init()
          addClickResetIcon()
          $(this).children('a').html('Pause')
          $(this).children('img').attr('src', 'images/pause.png')
          break
        case 'Pause':
          pauseGame()
          $('.pausePlay').children('a').html('Continue')
          $('.pausePlay').children('img').attr('src', 'images/play.png')
          break
        case 'Continue':
          playGame()
          $('.pausePlay').children('a').html('Pause')
          $('.pausePlay').children('img').attr('src', 'images/pause.png')
          break
      }
    })
   
    $('.icoInfo').click(function () {
      pauseGame()
     
      if ($('.pausePlay').children('a').text() != 'Start') {
        $('.pausePlay').children('a').html('Continue')
        $('.pausePlay').children('img').attr('src', 'images/play.png')
      }
      $('#blocker').removeClass('layer-hide')
      $('.restartCont').addClass('layer-hide')
      $('.question').removeClass('layer-hide')
      removeClickInfoIcon()
    })
   
    $('.closeBtn').click(function () {
      $('.question').addClass('layer-hide')
      $('#blocker').addClass('layer-hide')
      addClickInfoIcon()
    })
   
    $('.icoReset').click(function () {
      removeClickResetIcon()
     
      $('#blocker').removeClass('layer-hide')
      $('.question').addClass('layer-hide')
      $('.restartCont').removeClass('layer-hide')
     
      pauseGame()
     
      $('.levelModal').removeClass('veryGoodBackground finishBackground')
      
      $('.levelModal').addClass('showLevel')
      $('.levelModal').addClass('resetBackground')
      $('#starScores').addClass('layer-hide')
      $('#restartLevel').removeClass('layer-hide')
      $('#levelMsg').addClass('layer-hide')
      $('#completeMsg').addClass('layer-hide')
      $('.borderBallons').addClass('levelAnimateIn')
      $('.bottomImage').addClass('levelAnimateIn')
    })
   
    $('.restartConfirmation').click(function () {
      addClickResetIcon()
     
      $('#blocker').addClass('layer-hide')
     
      $('.pausePlay').children('a').html('Continue')
      $('.pausePlay').children('img').attr('src', 'images/play.png')
      $('.levelModal').removeClass('showLevel')
      //$('#restartLevel').addClass('layer-hide')
      //$('#levelMsg').addClass('layer-hide')
      //$('#completeMsg').addClass('layer-hide')
      $('.borderBallons').removeClass('levelAnimateIn')
      $('.bottomImage').removeClass('levelAnimateIn')
    })
 
    $('.balloon').click(function () {
      var thisBalloonColor = $(this).attr('class').split(' ')[2]
      $(this).removeClass('balloonBlue balloonRed balloonGreen balloonOrange balloonViolet balloonYellow')
      $(this).children('.test').children('.balloonHead').html()
      $(this).children('.test').children('.tales').addClass('layer-hide')
 
      if ($(this).text() == currentAnswer) {
        boom.play()
        $(this).children('.test').addClass('explosionAnimation ' + thisBalloonColor + 'Explode')
        $('.txtNumbersCont.txtNumbersDefault.notAnswered').first().removeClass('notAnswered').addClass('txtNumberFound animated fadeInRight false').html(currentAnswer)
        $('.txtNumbersCont.txtNumbersDefault.notAnswered').first().addClass('txtNumbersNext').html('?')
        addStar()
        currentAnswer += answerInterval
        $('.currentAnswer').text(currentAnswer)
      } else {
        deflate.play()
        $(this).children('.test').addClass('deflateAnimation ' + thisBalloonColor + 'Deflate')
        removeStar()
      }
      clickedBalloon = true
      if ($('.txtNumbersCont.txtNumbersDefault.notAnswered').length == 0) {
        claps.play()
       
        clearInterval(balloonWaveTimer)
       
        if (levelScores[currentLevel - 1] < stars) {
          levelScores[currentLevel - 1] = stars
        }
        if (nextLevel == currentLevel) {
          nextLevel++
          $('.levels.levelslocked').first().removeClass('levelslocked').addClass('levelsOpen')
        }
 
        $('.balloon').addClass('paused')
       
        $('#blocker').removeClass('layer-hide')
        $('.question').addClass('layer-hide')
        $('.restartCont').removeClass('layer-hide')
       
        $('.levelModal').removeClass('resetBackground finishBackground')
        $('.levelModal').addClass('veryGoodBackground')
        $('.levelModal').addClass('showLevel')
 
        $('#starScores').addClass('layer-hide')
        if (currentLevel == 5) {
          $('#restartLevel').addClass('layer-hide')
          $('#levelMsg').addClass('layer-hide')
          $('#completeMsg').removeClass('layer-hide')
          
          $('.levelModal').addClass('finishBackground')
          $('.levelModal').removeClass('veryGoodBackground resetBackground')
        } else {
          $('#restartLevel').addClass('layer-hide')
          $('#completeMsg').addClass('layer-hide')
          $('#levelMsg').removeClass('layer-hide')
        }
 
        $('.starsCollected').html(stars)
        $('#nextLevel').html(nextLevel)
        $('#answerInterval').html(answerInterval)
        $('#nextLevelCounter').html(currentAnswer)
 
        $('.borderBallons').addClass('levelAnimateIn')
        $('.bottomImage').addClass('levelAnimateIn')
      }
 
      $(this).children('.test').children('.balloonHead').html('')
    })
  })
 
  $(document).on('click', '.levelsOpen', function () {
    resetGame()
    pauseGame()
    removeClickResetIcon()
    $('.pausePlay').children('a').html('Start')
    $('.pausePlay').children('img').attr('src', 'images/play.png')
   
    stars = 0
    currentAnswer = (answerInterval * (parseInt($(this).text(), 10) - 1) * 20) + answerInterval
 
    currentLevel = $(this).children('h1').text()
    $('.levelDisplay').html('Level ' + currentLevel)
 
    $('.balloon').removeClass('speed1 speed2 speed3 speed4 speed5').addClass('speed' + currentLevel)
    $('.backgroundImage').removeClass('day afternoon sunset dusk night')
    $('.sunMoon').removeClass('sun moon')
    switch (currentLevel) {
      case '1':
        $('.backgroundImage').addClass('day')
        $('.sunMoon').addClass('sun')
        break
      case '2':
        $('.backgroundImage').addClass('afternoon')
        $('.sunMoon').addClass('sun')
        break
      case '3':
        $('.backgroundImage').addClass('sunset')
        $('.sunMoon').addClass('sun')
        break
      case '4':
        $('.backgroundImage').addClass('dusk')
        $('.sunMoon').addClass('moon')
        break
      case '5':
        $('.backgroundImage').addClass('night')
        $('.sunMoon').addClass('moon')
        break
    }
 
    $('.txtNumbersCont.txtNumbersDefault').removeClass('txtNumberFound animated fadeInRight false txtNumbersNext').addClass('notAnswered').html('')
 
    $('.txtNumbersCont.txtNumbersDefault.notAnswered').first().removeClass('notAnswered').addClass('txtNumberFound animated fadeInRight false').html( answerInterval * 20 * (currentLevel - 1))
    $('.txtNumbersCont.txtNumbersDefault.notAnswered').first().addClass('txtNumbersNext').html('?')
    $('.starPoints.filledStar').removeClass('filledStar').addClass('emptyStar')
   
    $('#blocker').addClass('layer-hide')
    $('.levelModal').removeClass('showLevel')
    $('.borderBallons').removeClass('levelAnimateIn')
    $('.bottomImage').removeClass('levelAnimateIn')
    //$('.balloon').removeClass('paused')
    
    $(this).children('a').html('Start')
    $(this).children('img').attr('src', 'images/play.png')
  })
 
  $(document).on('click', '.bestScore', function () {
    for (var i = 0; i < 5; i++) {
      $('#scoreLevel' + (i + 1)).html(levelScores[i])
    }
    $('#restartLevel').addClass('layer-hide')
    $('#completeMsg').addClass('layer-hide')
    $('#levelMsg').addClass('layer-hide')
    $('#starScores').removeClass('layer-hide')
  })
  
  function resetGame () {
    $('.balloon').removeClass('balloonAnimation')
  }
 
  function pauseGame () {
    pause = true
    $('.balloonHead').addClass('layer-hide')
    $('.balloon').addClass('paused')
  }
 
  function playGame () {
    pause = false
    $('.balloonHead').removeClass('layer-hide')
    $('.balloon').removeClass('paused')
  }
 
  function addClickInfoIcon () {
    $('.icoInfo').css('opacity', '1')
    $('.icoInfo').css('pointer-events', 'auto')
  }
 
  function removeClickInfoIcon () {
    $('.icoInfo').css('opacity', '0.5')
    $('.icoInfo').css('pointer-events', 'none')
  }
 
  function addClickResetIcon () {
    $('.icoReset').css('opacity', '1')
    $('.icoReset').css('pointer-events', 'auto')
  }
 
  function removeClickResetIcon () {
    $('.icoReset').css('opacity', '0.5')
    $('.icoReset').css('pointer-events', 'none')
  }
 
  function addStar () {
   stars++
    $('.starPoints.emptyStar').first().removeClass('emptyStar').addClass('filledStar')
    $('.stars').text(stars)
  }
 
  function removeStar () {
    if (stars > 0) {
      $('.starPoints.filledStar').last().removeClass('filledStar').addClass('emptyStar')
      stars--
    }
    $('.stars').text(stars)
  }
 
  function balloonWave () {
      if ($('#balloon' + balloons[5]).position().top < -250) {
        $('.balloon').removeClass('balloonBlue balloonRed balloonGreen balloonOrange balloonViolet balloonYellow')
       
        
        $('.tales').removeClass('layer-hide')
        $('.balloon > .test').removeClass('deflateAnimation explosionAnimation balloonBlueExplode balloonRedExplode balloonGreenExplode balloonOrangeExplode balloonVioletExplode balloonYellowExplode balloonBlueDeflate balloonRedDeflate balloonGreenDeflate balloonOrangeDeflate balloonVioletDeflate balloonYellowDeflate')
        setAnswers()
        if (clickedBalloon) {
          clickedBalloon = false
        } else {
          setAnswers()
          removeStar()
          $('.stars').text(stars)
        }
        clearInterval(balloonWaveTimer)
        init()
      }
  }
 
  function init () {
    randomSort();
    iteracion++
    $('.tales').removeClass('layer-hide')
    $('.balloon').removeClass('balloonAnimation balloonBlue balloonRed balloonGreen balloonOrange balloonViolet balloonYellow')
    showBalloons = setInterval(inflate, 350)
   
    setAnswers()
    balloonWaveTimer = setInterval(balloonWave, 10)
    setColor()
  }
 
  function setColor () {
    for (var i = 1; i <= 6; i++) {
      $('#balloon' + i).addClass(balloonColor[parseInt(Math.random() * 5, 10)])
    }
  }
 
  function inflate () {
    if (!pause){
      var x = parseInt(Math.random() * 10, 10)
      if (num <= 6) {
        switch (true) {
          case (x >= 9):
            $('#balloon' + balloons[num]).addClass('balloonAnimation')
            $('#tail' + balloons[num++]).addClass('tales')
            $('#balloon' + balloons[num]).addClass('balloonAnimation')
            $('#tail' + balloons[num++]).addClass('tales')
            $('#balloon' + balloons[num]).addClass('balloonAnimation')
            $('#tail' + balloons[num++]).addClass('tales')
            break
          case (x >= 8):
            $('#balloon' + balloons[num]).addClass('balloonAnimation')
            $('#tail' + balloons[num++]).addClass('tales')
            $('#balloon' + balloons[num]).addClass('balloonAnimation')
            $('#tail' + balloons[num++]).addClass('tales')
            break
          case (x < 8):
            $('#balloon' + balloons[num]).addClass('balloonAnimation')
            $('#tail' + balloons[num++]).addClass('tales')
            break
        }
      } else {
        num = 0
        clearInterval(showBalloons)
      }
    }
  }
 
  function randomSort(){
   
    balloons.sort(function() { return 0.5 - Math.random() });
   
  }
})()