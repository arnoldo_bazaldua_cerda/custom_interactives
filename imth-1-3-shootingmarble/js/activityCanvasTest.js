var background = new Image();
var marbleImage = new Image();
var points = [];
var gameSpeed = 1;

var mainCanvas = document.getElementById("bubbles");
var mainContext = mainCanvas.getContext('2d');
mainCanvas.addEventListener("mousedown", getPosition, false);

var circles = new Array();

var numbers = []
var num = 0
var difficult
var gameTimer = 0
var gameTimerTotal = 0
var settings = [0, 0, 0]
var i = 0
var k = 0
var z = 0
var tiempo
var pause = false
var actual
var addAnimation
var gameStatus = false
var currentGameSpeed = 0;
var marble
var clickMultiplier = 0
var validateMarble = {
  validateClick: false
}
var pauseMarbles = true
var marblePauseTimer = 0
var canvasAnimation

var flagPortrait
var drawIterations = 0
if (window.matchMedia('(max-width: 770px)').matches) {
  OnResizeCalled()
  $('#bubbles').removeClass('bubblesL2')
  $('#bubbles').addClass('bubblesP')
  flagPortrait = true
}
 
if (window.matchMedia('(min-width: 1020px)').matches) {
  OnResizeCalled()
  $('#bubbles').removeClass('bubblesL2')
  $('#bubbles').addClass('bubblesL')
  flagPortrait = false
}
var requestAnimationFrame = window.requestAnimationFrame ||
  window.mozRequestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.msRequestAnimationFrame;

var cancelAnimationFrame = window.cancelAnimationFrame || window.mozCancelAnimationFrame;

function getPosition(event) {
  var x = event.x;
  var y = event.y;
  var offsetX = event.x;
  var offsetY = event.y;
  marblePauseTimer = 0
  var canvas = document.getElementById("bubbles");

  offsetX -= canvas.offsetLeft;
  offsetY -= canvas.offsetTop;

  validateMarble = removeMarble(x, y);
  if (validateMarble.validateClick) {
    marble = circles[validateMarble.marbleClicked]
    $('#num').addClass('numh')

    $('.correctAnswer').removeClass('layer-hide')
    if (window.matchMedia('(min-width: 769px)').matches) {
      $('.correctAnswer').text(marble.answer).css({
        top: marble.yPos + 22,
        left: marble.xPos + 22,
        visibility: 'visible'
      }).animate({
        top: '0.5%',
        left: '54.6%'
      }, 600, 'linear')
    } else {
      $('.correctAnswer').text(marble.answer).css({
        top: marble.yPos*.75,
        left: marble.xPos*.75+20,
        visibility: 'visible'
      }).animate({
        top: '0.4%',
        left: '56.5%'
      }, 600, 'linear')
    }
    k--;
    circles.splice(validateMarble.marbleClicked, 1);
  }
}

function removeMarble(clickPosX, clickPosY) {
  //borrar aqui con ctrl+z
  pauseMarbles = true;

  for (var i = 0; i < circles.length; i++) {

    if (window.matchMedia('(max-width: 768px)').matches) {
      if (circles[i].xPos < clickPosX / .75 && circles[i].xPos > clickPosX / .75 - 50 && circles[i].yPos < clickPosY / .75+15 && circles[i].yPos > clickPosY / .75 - 35) {
        if (window.matchMedia('(max-width: 1030px)').matches) {
          $('.slingshot').css({
            left: clickPosX - 125
          })
        } else {
          $('.slingshot').css({
            left: clickPosX - 120
          })
        }
        mainCanvas.removeEventListener("mousedown", getPosition, false);
        $('.rock').css({
          top: $('.rock').offset().margin
        }).animate({
          top: clickPosY
        }, 250, 'linear')
        setTimeout(function() {
          $('.rock').addClass('layer-hide')
        }, 250)
        if (circles[i].answer == num) {
          audioCorrect.play()
          audioS1.play()
          return {
            validateClick: true,
            marbleClicked: i
          }
        } else {
          audioRebote.play()
        }
      }
    } else if (window.matchMedia('(max-width: 1024px)').matches) {

      if (circles[i].xPos < clickPosX && circles[i].xPos > clickPosX - 50 && circles[i].yPos < clickPosY && circles[i].yPos > clickPosY - 50) {
        if (window.matchMedia('(max-width: 1024px)').matches) {
          $('.slingshot').css({
            left: clickPosX - 125
          })
        } else {
          $('.slingshot').css({
            left: clickPosX - 120
          })
        }
        mainCanvas.removeEventListener("mousedown", getPosition, false);
        $('.rock').css({
          top: $('.rock').offset().margin
        }).animate({
          top: clickPosY
        }, 250, 'linear')
        setTimeout(function() {
          $('.rock').addClass('layer-hide')
        }, 250)

        if (circles[i].answer == num) {
          audioCorrect.play()
          audioS1.play()
          return {
            validateClick: true,
            marbleClicked: i
          }
        } else {
          audioRebote.play()
        }

      }
    } else {
      clickMultiplier = ($(window).width() - 1024) / 2
      if (circles[i].xPos < clickPosX-clickMultiplier && circles[i].xPos > clickPosX - 50-clickMultiplier && circles[i].yPos < clickPosY && circles[i].yPos > clickPosY - 50) {
        if (window.matchMedia('(max-width: 1024px)').matches) {
          $('.slingshot').css({
            left: clickPosX - 125 - clickMultiplier
          })
        } else {
          $('.slingshot').css({
            left: clickPosX - 120 - clickMultiplier
          })
        }
        mainCanvas.removeEventListener("mousedown", getPosition, false);
        $('.rock').css({
          top: $('.rock').offset().margin
        }).animate({
          top: clickPosY
        }, 250, 'linear')
        setTimeout(function() {
          $('.rock').addClass('layer-hide')
        }, 250)

        if (circles[i].answer == num) {
          audioCorrect.play()
          audioS1.play()
          return {
            validateClick: true,
            marbleClicked: i
          }
        } else {
          audioRebote.play()
        }

      }
    }

  }
  return {
    validateClick: false
  }
}

function initCanvas() {
  background.src = "../../interactives/images/niveles2.jpg";
  //marbleImage.src = "../../interactives/images/penguin1.png";
  canvasAnimation = requestAnimationFrame(draw);
}

function draw() {
  //i++;
  drawIterations++
  marblePauseTimer++
  //console.log(circles[0].counter)
  if (circles.length != 0) {
    if (circles[k].xPos < 840 && k < circles.length - 1) {
      k++;
    }
    mainContext.clearRect(0, 0, 1024, 1080);
    //3
    
    if (validateMarble.validateClick && pauseMarbles) {
      
      for (var j = 0; j <= k; j++) {
        if (j < validateMarble.marbleClicked && validateMarble.marbleClicked != circles.length)
          circles[j].update(0);
        else
          circles[j].update(gameSpeed);
      }
      if (marblePauseTimer > 225 / gameSpeed) {
        console.log('||||||||||||||||||||||||')
        console.log(validateMarble.marbleClicked)
        console.log(circles.length-1)
        if (validateMarble.marbleClicked == circles.length) {
          console.log('ultima canica')
        }
        $('.rock').removeClass('layer-hide')
        $('.rock').css('top', '')
        mainCanvas.addEventListener("mousedown", getPosition, false);
        next()
        marblePauseTimer = 0
        pauseMarbles = false
      }
    } else {
      
      for (var j = 0; j <= k; j++) {
        circles[j].update(gameSpeed);
      }
      if (marblePauseTimer > 75 / gameSpeed) {
        $('.rock').removeClass('layer-hide')
        $('.rock').css('top', '')
        mainCanvas.addEventListener("mousedown", getPosition, false);
        marblePauseTimer = 0
        pauseMarbles = false
      }
    }
    if (circles[k].xPos < 840 && k < circles.length - 1) {
      k++;
    }
    canvasAnimation = requestAnimationFrame(draw);
  } else {


    $('.sling').css('visibility', 'hidden')

    $('.sling').css('visibility', 'hidden')

    if (window.matchMedia('(max-width: 1030px)').matches) {
      $('.penguin3').css({
        left: actual + 450
      })
    } else {
      $('.penguin3').css({
        left: actual + 150
      })
    }
    $('.rock').addClass('layer-hide')

    $('.penguin3').css('visibility', 'visible')
    $('.penguin3').addClass('walking')

    $('.cube').addClass('walking')

    setTimeout(function() {
      $('.penguin3').removeClass('walking').css({
        'visibility': 'hidden',
        'left': '400px'
      })
      $('.slingshot').addClass('layer-hide')

      audioAplausos.play()
      audioWakeUp.play()

      $('#winScreen').removeClass('layer layer-hide').addClass('winScreen')
      $('.bear').css('display', 'none')
      $('.left-slide').css('display', 'none')

    }, 3500)
  }
}

function Circle(xPos, yPos, id) {
  this.id = id;
  this.xPos = xPos;
  this.yPos = yPos;
  this.mWidth = 50;
  this.mHeight = 50;
  this.counter = 0;
}

Circle.prototype.update = function(speedMultiplier) {
 // speedMultiplier = 0
  checkRoutePosition[this.counter](this)
  var addX = routeArray[this.counter][0] * speedMultiplier;
  var addY = routeArray[this.counter][1] * speedMultiplier;
  mainContext.drawImage(marbleImage, 0, 0, 76, 76, this.xPos + addX, this.yPos + addY, this.mWidth, this.mHeight);
  /*mainContext.drawImage(marbleImage, 0, 0, 76, 76, this.xPos - 43.5, this.yPos + addY, this.mWidth, this.mHeight);
  mainContext.drawImage(marbleImage, 0, 0, 76, 76, this.xPos - 87, this.yPos + addY, this.mWidth, this.mHeight);
  mainContext.drawImage(marbleImage, 0, 0, 76, 76, this.xPos - 130.5, this.yPos + addY, this.mWidth, this.mHeight);
  mainContext.drawImage(marbleImage, 0, 0, 76, 76, this.xPos - 169, this.yPos + 20, this.mWidth, this.mHeight);
  mainContext.drawImage(marbleImage, 0, 0, 76, 76, this.xPos - 190, this.yPos + 58, this.mWidth, this.mHeight);
  mainContext.drawImage(marbleImage, 0, 0, 76, 76, this.xPos - 218, this.yPos + 92, this.mWidth, this.mHeight);
  mainContext.drawImage(marbleImage, 0, 0, 76, 76, this.xPos - 258, this.yPos + 111, this.mWidth, this.mHeight);*/
  
  mainContext.font = '24px mclarenregular';
  mainContext.fillStyle = 'white'
  mainContext.textAlign = 'center'
  mainContext.fillText(this.answer, this.xPos + (this.mWidth / 2.2) + addX, this.yPos + (this.mHeight / 1.7) + addY);
  this.xPos += addX;
  this.yPos += addY;
  //mainContext.translate(this.speed,0);
};

//remove to current route
var a1 = function(currentMarble) {
  if (drawIterations == 75) {
    console.log('a1')
    drawIterations = 0
    currentMarble.counter++
  }
}

var a2 = function(currentMarble) {
  if (drawIterations == 150) {
    console.log('a2')
    drawIterations = 0
    currentMarble.counter++
  }
}

var a2a = function(currentMarble) {
if (drawIterations%100 == 0) {
    drawIterations = 0
    currentMarble.counter++
  }
}

var a2b = function(currentMarble) {
  if (currentMarble.xPos < 509) {
    currentMarble.counter++
  }
}

var a4 = function(currentMarble) {
  if (currentMarble.xPos < 400) {
    currentMarble.counter++
  }
}

var a5 = function(currentMarble) {
  if (currentMarble.xPos < 330) {
    currentMarble.counter++
  }
}

var a5a = function(currentMarble) {
  if (currentMarble.xPos < 380) {
    currentMarble.counter++
  }
}


var a6 = function(currentMarble) {
  if (currentMarble.xPos < 240) {
    currentMarble.counter++
  }
}

var a7aa = function(currentMarble) {
  if (currentMarble.xPos < 220) {
    currentMarble.counter++
  }
}

var a7 = function(currentMarble) {
  if (currentMarble.xPos < 190) {
    currentMarble.counter++
  }
}

var a7a = function(currentMarble) {
  if (currentMarble.xPos > 220) {
    currentMarble.counter++
  }
}

var a7ab = function(currentMarble) {
  if (currentMarble.xPos > 270) {
    currentMarble.counter++
  }
}


var a7b = function(currentMarble) {
  if (currentMarble.xPos > 340) {
    currentMarble.counter++
  }
}

var a9 = function(currentMarble) {
  if (currentMarble.xPos > 450) {
    currentMarble.counter++
  }
}

var a10 = function(currentMarble) {
  // if (currentMarble.xPos > 690) {
  if (currentMarble.xPos > 650) {
    currentMarble.counter++
  }
}

var a10a = function(currentMarble) {
  if (currentMarble.xPos > 670) {
    currentMarble.counter++
  }
}


var a10ab = function(currentMarble) {
  if (currentMarble.xPos > 740) {
    //if (currentMarble.xPos > 790) {
    currentMarble.counter++
  }
}

var a11 = function(currentMarble) {
  if (currentMarble.xPos > 810) {
    currentMarble.counter++
  }
}


var a12 = function(currentMarble) {
  if (currentMarble.xPos > 840) {
    currentMarble.counter++
  }
}

var a13 = function(currentMarble) {

  if (currentMarble.xPos < 820) {
    currentMarble.counter++
  }
}

var a13a = function(currentMarble) {

  if (currentMarble.xPos < 770) {
    currentMarble.counter++
  }
}

var a14 = function(currentMarble) {
  if (currentMarble.xPos < 620) {
    currentMarble.counter++
  }
}

var a14b = function(currentMarble) {
  if (currentMarble.xPos < 350) {
    currentMarble.counter++
  }
}

var a16 = function(currentMarble) {
  if (currentMarble.xPos < 200) {
    currentMarble.counter++
  }
}
var a17 = function(currentMarble) {
  currentMarble.counter++
  window.cancelAnimationFrame(canvasAnimation)
  gameSpeed = 0
  currentGameSpeed = 0
  summonBear()
}

var a18 = function(currentMarble) {

}

var checkRoutePosition = [
  a1, 
  a2,
  a2a,
  a2b,
  a4,
  a5,
  a6,
  a7aa,
  a7,
  a7a,
  a7ab,
  a7b,
  a9,
  a10,
  a10a,
  a10ab,
  a11,
  a12,
  a13,
  a13a,
  a14,
  a14b, // a15,
  a16,
  a17,
  a18
];

//remove to reset
var routeArray = [
  [-0.2, 0], //1                    20
  [-0.12, 0.17], //2                29
  [-.19, .04], //a2a                23
  [-.19, -.04], //a2b               23
  [-.14, -.14], //4                 28
  [-0.18, -0.07], //5               25
  [-0.18, 0.07], //6                25
  [-.15, .15], //a7aa               30
  [-0.07, 0.19], //7                26
  [0.07, 0.19], //7a                26
  [0.15, 0.15], //a7ab              30
  [.19, .05],   //                  24
  [0.2, 0], //9                     20
  [.19, -.07],  //                  26
  [0.19, -0.09], //  15 a10a        28
  [.2, .03], //a10a                 23
  [.19, .08], //11                  27
  [0.07, 0.19], //12                26
  [-0.07, 0.19], //13               26
  [-.16, .12], //a13a               28
  [-.19, .02], //                   21
  [-.19, -.04], //14                23
  //  [-.19, -.055], //15     
  [-.2, 0], //16        
  [0, 0],
  [0, 0]
];

function drawCircles() {
  circles = []
  var widthMultiplier = 0;
  var marbleXPosition = 0;
  var marbleYPosition = 0;
  if (window.innerWidth >= 1024) {
    marbleXPosition = mainCanvas.width * .865;
    marbleYPosition = mainCanvas.height * .1;
  } else {
    marbleXPosition = mainCanvas.width * .865;
    marbleYPosition = mainCanvas.height * .13;
  }
  for (var i = 0; i < 20; i++) {
    var circle = new Circle(marbleXPosition, marbleYPosition, i);
    circles.push(circle);
  }
}

function summonBear() {
  if (!pause) {
    $('.bear').css('display', 'none')
    $('.rock').addClass('layer-hide')
    $('.left-slide').css('background', 'none')
    $('.bearWalking').css('left', '1500px').css('visibility', 'visible')
    $('.sling').css('visibility', 'hidden')
    $('.penguin2').css('visibility', 'visible')
    $('.penguin2').css({

      left: $('.penguin2').position().left
    }).animate({
      left: '500%'
    }, 1600, 'linear')

    audioOsoPinguino.play()
    setTimeout(function() {
      audioSorry.play()
      $('#container').css('pointer-events', 'inherit')
      $('.feed-screen').css('display', 'block')
    }, 3500)
  }
}

function generateNumbers() {
  switch (difficult) {
    case 'Easy':
      marbleImage.src = "../../interactives/images/penguin@2x.png"
      currentGameSpeed = 1

      break
    case 'Medium':
      marbleImage.src = "../../interactives/images/penguin3x.png"
      currentGameSpeed = 3//1.1
      break
    case 'Hard':
      marbleImage.src = "../../interactives/images/penguin2x.png"
      currentGameSpeed = 1.3
      break
  }
  for (var i = 0; i < 20; i++) {
    numbers.push(parseInt(Math.random() * 20 + 1, 10))
  }
}

function getNumber() {
  var index = parseInt(Math.random() * numbers.length, 10)
  num = numbers[index]
  numbers.splice(index, 1)
}

function init() {
   if (window.matchMedia('(max-width: 770px)').matches) {
//  OnResizeCalled()
    $('#bubbles').removeClass('bubblesL2')
    $('#bubbles').removeClass('bubblesL')
    $('#bubbles').addClass('bubblesP')
    flagPortrait = true
  }

  if (window.matchMedia('(min-width: 1020px)').matches) {
  // OnResizeCalled()
    $('#bubbles').removeClass('bubblesL2')
    $('#bubbles').removeClass('bubblesP')
    $('#bubbles').addClass('bubblesL')
  }
  $('.feed-screen').css('display', 'none')
  $('#winScreen').addClass('layer-hide')
  //callback aqui
  //audioOsoPinguino.play()
  validateMarble = {
    validateClick: false
  }
  pauseMarbles = true
  gameStatus = true
  k = 0
  z = 0
  marblePauseTimer = 0

  $('#num').addClass('numh')
  $('.correctAnswer').addClass('layer-hide')
  $('.marble').show()
  numbers = []
  generateNumbers()
  gameSpeed = currentGameSpeed
  setNumbers()

  next()
  i = 0
  $('.rock').removeClass('layer-hide')
  $('.left-slide').css('display', 'block')

  $('.penguin3').removeClass('walking').css({
    'visibility': 'hidden',
    'left': '400px'
  })
  $('.penguin2').css('visibility', 'hidden')
  $('.penguin2').removeClass('walking')

  $('.cube').removeClass('walking')
  $('.bear').css('display', 'block')
  $('.bearWalking').css({
    'visibility': 'hidden',
    'left': '0px'
  })
  $('.sling').css('visibility', 'visible')
  $('.penguin2').removeClass('walking').css({
    'visibility': 'hidden',
    'left': '100px'
  })
  $('.slingshot').removeClass('layer-hide').css('left', '')
  $('#game').removeClass('layer-hide').addClass('game')
  $('#difficulty').removeClass('difficulty').addClass('layer-hide')
}

function next() {
  if (numbers.length != 0) {

    getNumber()
      // setTimeout(function() {
    $('.correctAnswer').addClass('layer-hide')
    $('#num').removeClass('numh')
    getOperandos()
      //}, settings[0])
  }
}

function getOperandos() {
  var op1, op2
  do {
    op1 = parseInt(Math.random() * 20, 10)
  } while (op1 >= num)
  op2 = Math.abs(op1 - num)
  $('#num1').html(op1)
  $('#num2').html(op2)
  $('.sign').removeClass('layer-hide')
}

function setNumbers() {
  for (var i = 1; i <= numbers.length; i++) {

    $('#div' + i).html(numbers[i - 1]).removeClass('hide').addClass(' marble')
    circles[i - 1].answer = numbers[i - 1]
  }
}

var closeButton = document.querySelector('#closeButton')
closeButton.addEventListener('click', closeInstructions)

$('.play').on('click', function() {
  $('#winScreen').removeClass('winScreen').addClass('layer layer-hide')
  $('.feed-screen').css('display', 'none')
  $('#game').removeClass('game').addClass('layer-hide')
  $('#difficulty').removeClass('layer-hide').addClass('difficulty')
  audioSorry.pause()
  audioSorry.pause()
  audioAplausos.pause()
  audioWakeUp.pause()
  audioAplausos.currentTime = 0
  audioWakeUp.currentTime = 0
})

$('.icon-reset').on('click', function() {
  window.cancelAnimationFrame(canvasAnimation)
 
  $('.icon-reset').addClass('icoResetMove')
  setTimeout(function() {
    $('.icon-reset').removeClass('icoResetMove')
  }, 1000)
 
  $('.feed-screen').css('display', 'none')
  $('#game').removeClass('game').addClass('layer-hide')
  $('#difficulty').removeClass('layer-hide').addClass('difficulty')
 
  if (window.matchMedia('(max-width: 770px)').matches) {
//  OnResizeCalled()
    $('#bubbles').removeClass('bubblesL2')
    $('#bubbles').removeClass('bubblesL')
    $('#bubbles').addClass('bubblesP')
    flagPortrait = true
  }

  if (window.matchMedia('(min-width: 1020px)').matches) {
  // OnResizeCalled()
    $('#bubbles').removeClass('bubblesL2')
    $('#bubbles').removeClass('bubblesP')
    $('#bubbles').addClass('bubblesL')
  }
})

$('.D-C-Item').click(function() {
  $('#num1').html('')
  $('#num2').html('')
  difficult = $(this).text()
  window.cancelAnimationFrame(canvasAnimation)
  k = 0
  i = 0
  gameSpeed = 0
  currentGameSpeed = 0
  mainCanvas.addEventListener("mousedown", getPosition, false);
  drawCircles();
  initCanvas();
  init()
})

function closeInstructions() {
  $('.marble').removeClass('paused')
  pause = false
  $('#instructionsText').removeClass('layer').addClass('layer layer-hide')
  gameSpeed = currentGameSpeed
}

var infoButton = document.querySelector('#infoButton')
infoButton.addEventListener('click', showInstructions)

function showInstructions() {
  $('.marble').addClass('paused')
  pause = true
  $('#instructionsText').removeClass('layer layer-hide').addClass('layer')
  gameSpeed = 0;
}

function pauseGame() {
  gameSpeed = 0;
}

function unpauseGame() {
  gameSpeed = currentGameSpeed;
}

window.addEventListener("resize", OnResizeCalled, false);

function OnResizeCalled() {
  console.trace('risize')
  var a = document.getElementById("bubbles");

  var scaleX = window.innerWidth / a.width;
  var scaleY = window.innerHeight / a.height;
  //console.log('scaleX: ' + scaleX)
  //console.log('scaleY: ' + scaleY)
  var scaleToFit = Math.min(scaleX, scaleY);
  var scaleToCover = Math.max(scaleX, scaleY);

  a.style.transformOrigin = "0 0"; //scale from top left
  if (window.innerHeight > window.innerWidth) {
    a.style.transform = "scale(" + scaleToFit + ")";
    $('#bubbles').removeClass('bubblesL2')
  } else {
    if (window.matchMedia('(min-width: 1024px)').matches) {
      a.style.transform = "scale(" + 1 + ")";                              
      $('#bubbles').removeClass('bubblesP')
      if (flagPortrait==true)  {
        $('#bubbles').addClass('bubblesL2')
        if (window.matchMedia('(min-width: 1022px)').matches)  {
          if ($('#bubbles').hasClass("bubblesL", "bubblesL2")) {
            $('#bubbles').removeClass("bubblesL2")
          }
        } 
      } else  {
        $('#bubbles').removeClass('bubblesL2')
      }
    }
  }
}