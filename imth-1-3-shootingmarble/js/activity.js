(function () {
  var numbers = []
  var num = 0
  var difficult
  var gameTimer = 0
  var gameTimerTotal = 0
  var settings = [0, 0, 0]
  var i = 0
  var k = 1
  var z = 0
  var tiempo
  var pause = false
  var actual
  var addAnimation
  var gameStatus = false
  var currentGameSpeed = 0;
  
var theThing = document.getElementById('div1')
var one = $('#div')
var two = document.querySelector('#div1')


window.onfocus = function() {
  if(gameStatus){
    pause = false;

    $('.marble').removeClass('paused')
    
  }

};
window.onblur = function() {
  if(gameStatus){
    pause = true;
    gameSpeed = 0;
    $('.marble').addClass('paused')
  }
};

  
  function summonBear () {
    if (!pause) {
      //console.log(i)
      i++
      if (i > gameTimerTotal) {
        clearInterval(tiempo)
        clearInterval(addAnimation)

        $('.bear').css('display', 'none')
        $('.rock').addClass('layer-hide')
        $('.left-slide').css('background', 'none')
        $('.bearWalking').css('left', '1500px').css('visibility', 'visible')
        $('.sling').css('visibility', 'hidden')
        $('.penguin2').css('visibility', 'visible')
        $('.penguin2').css({

          left: $('.penguin2').position().left
        }).animate({
          left: '500%'
        }, 1600, 'linear')

        audioOsoPinguino.play()
        setTimeout(function () {
          audioSorry.play()
          $('#container').css('pointer-events', 'inherit')
          $('.feed-screen').css('display', 'block')
        }, 3500)
      }
    }
  }

  function generateNumbers () {
    switch (difficult) {
      case 'Easy':
        $('.marble').css('background-image', 'url(../images/penguin@2x.png)')
        settings[0] = 4000
        settings[1] = 1000
        settings[2] = 'stylie-easy'
        gameTimer = 51500
        currentGameSpeed = 1
        
        break
      case 'Medium':
        $('.marble').css('background-image', 'url(../images/penguin3x.png)')
        settings[0] = 2900
        settings[1] = 700
        settings[2] = 'stylie-medium'
        gameTimer = 36000
        currentGameSpeed = 3
        break
      case 'Hard':
        $('.marble').css('background-image', 'url(../images/penguin2x.png)')
        settings[0] = 1700
        settings[1] = 400
        settings[2] = 'stylie-hard'
        gameTimer = 22300
        currentGameSpeed = 5
        break
    }

    for (var i = 0; i < 20; i++) {
      numbers.push(parseInt(Math.random() * 20 + 1, 10))
    }
  }

  function getNumber () {
    var index = parseInt(Math.random() * numbers.length, 10)
    num = numbers[index]
    numbers.splice(index, 1)
  }

  function init () {
    gameStatus = true
    k = 1   
    z = 0
    $('.marble').removeClass('stylie-easy stylie-medium stylie-hard')
    $('#num').addClass('layer-hide')
    $('.correctAnswer').addClass('layer-hide')
    $('.marble').show()
    console.log('animation start')
   // moveThing()
    //addAnimation = setInterval(marbleAnimation, 100)
    numbers = []
    generateNumbers()
    gameSpeed = currentGameSpeed
    setNumbers()
    next()
    gameTimerTotal = gameTimer
    i = 0
    $('.rock').removeClass('layer-hide')
      //$('.rock').css('margin-top', '100%')
    $('.left-slide').css('display', 'block')

    $('.penguin3').removeClass('walking').css({
      'visibility': 'hidden',
      'left': '400px'
    })
    $('.penguin2').css('visibility', 'hidden')
    $('.penguin2').removeClass('walking')

    $('.cube').removeClass('walking')
    $('.bear').css('display', 'block')
    $('.bearWalking').css({
      'visibility': 'hidden',
      'left': '0px'
    })
    $('.sling').css('visibility', 'visible')
    $('.penguin2').removeClass('walking').css({
      'visibility': 'hidden',
      'left': '100px'
    })
    $('.slingshot').removeClass('layer-hide').css('left', '')
    $('.marble').removeClass('stylie ' + settings[2])
    $('#game').removeClass('layer-hide').addClass('game')
    $('#difficulty').removeClass('difficulty').addClass('layer-hide')
    tiempo = setInterval(summonBear, 0)
  }

  function next () {
    if (numbers.length != 0) {
      getNumber()
      setTimeout(function () {
        $('.correctAnswer').addClass('layer-hide')
        $('#num').removeClass('layer-hide').html('?')
        getOperandos()
      }, settings[0])
    } else {
      $('.sling').css('visibility', 'hidden')

      if (window.matchMedia('(max-width: 1030px)').matches) {
        $('.penguin3').css({
          left: actual + 450
        })
      } else {
        $('.penguin3').css({
          left: actual + 150
        })
      }
      $('.rock').addClass('layer-hide')

      $('.penguin3').css('visibility', 'visible')
      $('.penguin3').addClass('walking')

      $('.cube').addClass('walking')

      setTimeout(function () {
        $('.penguin3').removeClass('walking').css({
          'visibility': 'hidden',
          'left': '400px'
        })
        $('.slingshot').addClass('layer-hide')

        audioAplausos.play()
        audioWakeUp.play()

        $('#winScreen').removeClass('layer layer-hide').addClass('winScreen')
        $('.bear').css('display', 'none')
        $('.left-slide').css('display', 'none')

      }, 3500)
      clearInterval(tiempo)
      clearInterval(addAnimation)
    }
  }

  function getOperandos () {
    var op1, op2
    do {
      op1 = parseInt(Math.random() * 20, 10)
    } while (op1 >= num)
    op2 = Math.abs(op1 - num)
    $('#num1').html(op1)
    $('#num2').html(op2)
    $('.sign').removeClass('layer-hide')
  }

  function setNumbers () {
    console.log(circles)
    for (var i = 1; i <= numbers.length; i++) {
      $('#div' + i).html(numbers[i - 1]).removeClass('hide').addClass(' marble')
      circles[i-1].answer = numbers[i-1]
    }
  }

  var closeButton = document.querySelector('#closeButton')
  closeButton.addEventListener('click', closeInstructions)

  $('.play').on('click', function () {
    $('#winScreen').removeClass('winScreen').addClass('layer layer-hide')
    $('.feed-screen').css('display', 'none')
    $('#game').removeClass('game').addClass('layer-hide')
    $('#difficulty').removeClass('layer-hide').addClass('difficulty')
    audioSorry.pause()
    audioSorry.pause()
    audioAplausos.pause()
    audioWakeUp.pause()
    audioAplausos.currentTime = 0
    audioWakeUp.currentTime = 0
  })

  $('.icon-reset').on('click', function () {
    clearInterval(tiempo)
    clearInterval(addAnimation)
    $('.feed-screen').css('display', 'none')
    $('#game').removeClass('game').addClass('layer-hide')
    $('#difficulty').removeClass('layer-hide').addClass('difficulty')
  })

  var myFunc = function (e) {
    if (window.matchMedia('(max-width: 1030px)').matches) {
      $('.slingshot').css({
        left: ($(this).position().left) - 125
      })
    } else {
      $('.slingshot').css({
        left: ($(this).position().left) - 120
      })
    }
    actual = ($(this).position().left) - 120

    $('.rock').css({
      top: $(this).offset().margin - top
    }).animate({
      'margin-top': $(this).offset().top - 600
    }, 250, 'linear')
    setTimeout(function () {
      $('.rock').addClass('layer-hide')
    }, 250)

    if ($(this).text() == num) {
      audioCorrect.play()
      audioS1.play()
      if ($(this)[0].id.replace('div', '') != $('.marble.' + settings[2]).last()[0].id.replace('div', '')) {
        gameTimerTotal += settings[1]
        
        for (var j=1,i = parseInt($(this)[0].id.replace('div', ''), 10) + 1; j <= 20 ; j++){
          console.log("se pausa el "+ i)   
          if(j<parseInt($(this)[0].id.replace('div', ''))){
            $('#div' + j ).addClass('paused')
          }else {
            $('#div' + i ).removeClass('stylie-hard')
            $('#div' + i ).addClass('hurry-hard')
              i++
            }
          }  
        /*
        for (var i = 0; i < $(this)[0].id.replace('div', ''); i++)
        console.log()
           $('#div' + (i + 1)).addClass('paused')
          */
           
        }
      $('.marble').off('click')
      $('#num').addClass('layer-hide')
      $('.correctAnswer').removeClass('layer-hide')
      $('.correctAnswer').text($(this).text()).css({
        top: $(this).position().top + 22,
        left: $(this).position().left + 22,
        visibility: 'visible'
      }).animate({
        top: '0.3%',
        left: '54.5%'
      }, 600, 'linear')
      $(this).removeClass('stylie ' + settings[2])
      $(this).hide()
      setTimeout(function () {
        $('.marble').removeClass('paused')
        $('.marble').removeClass('hurry-hard')
        $('.marble').addClass('stylie-hard')
        
        $('.marble').on('click', myFunc)
        if (numbers.length > 0) {
          $('.rock').removeClass('layer-hide')
          $('.rock').css('margin-top', '')
        }
      }, settings[0])
      next()
    } else {
      audioRebote.play()
      setTimeout(function () {
        $('.rock').removeClass('layer-hide')
        $('.rock').css('margin-top', '')
      }, 2200)
    }
  }
  $('.marble').on('click', myFunc)
  function marbleAnimation () {
    if (!pause && k <= 20) {
      z = z + 100
      if ((z % settings[0]) == 0) {
        $('#div' + k).addClass('stylie ' + settings[2])
        k++
      }
    }
  }

  $('.D-C-Item').click(function () {
    $('#num1').html('')
    $('#num2').html('')
    difficult = $(this).text()
    init()
  })

  function closeInstructions () {
    $('.marble').removeClass('paused')
    pause = false
    $('#instructionsText').removeClass('layer').addClass('layer layer-hide')
    gameSpeed = currentGameSpeed
  }

  var infoButton = document.querySelector('#infoButton')
  infoButton.addEventListener('click', showInstructions)

  function showInstructions () {
    $('.marble').addClass('paused')
    pause = true
    $('#instructionsText').removeClass('layer layer-hide').addClass('layer')
    gameSpeed = 0;
  }
  
  function pauseGame () {
    gameSpeed = 0;
  }
  
  function unpauseGame () {
    gameSpeed = currentGameSpeed;
  }

var path =[
  ['-200%', '0'],
  ['-300%', '0'],
  ['-400%', '0'],
  ['-460%', '80%'], 
  ['-510%', '170%'],
  ['-590%', '233%'],
  ['-690%', '255%'],
  ['-790%', '250%'],
  ['-885%', '210%'],
  ['-965%', '160%'],
  ['-1030%','85%'],
  ['-1103%', '10%'],
  ['-1190%', '-40%'],
  ['-1290%', '-55%'],
  ['-1390%', '-35%'],
  ['-1475%', '15%'],
  ['-1550%', '85%'],
  ['-1590%', '180%'],
  ['-1580%', '280%'],
  ['-1540%', '375%'],
  ['-1475%', '455%'],
  ['-1390%', '510%'],
  ['-1295%', '550%'],
  ['-1195%', '570%'],
  ['-1090%', '565%'],
  ['-990%', '555%'],
  ['-890%','525%'],
  ['-795%', '490%'],
  ['-700%', '450%'],
  ['-610%', '410%'],
  ['-510%', '380%'],
  ['-410%', '380%'],
  ['-310%', '405%'],
  ['-230%','472%'],
  ['-175%','560%'],
  ['-175%','662%'],
  ['-210%', '758%'],
  ['-290%', '825%'],
  ['-385%', '860%'],
  ['-490%', '875%'],
  ['-592%', '880%'],
  ['-696%', '880%'],
  ['-796%', '865%'],
  ['-896%', '840%'],
  ['-996%', '815%'],
  ['-1096%', '790%'],
  ['-1196%', '760%'],
  ['-1300%', '750%'], 
  ['-1400%','750%'],
  ['-1500%', '750%'], 
  ['-1600%', '750%']
  ] 
  var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
 window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
  var cancelAnimationFrame = window.cancelAnimationFrame || window.mozCancelAnimationFrame;
  var myReq;

  
    var hi=1
  var sth
  function hector(){
    
    
    sth = setTimeout(function(){
        
        
      if (!pause && k <= 20) {
      z = z + 100
      if ((z % settings[0]) == 0) {
        $('#div' + k).addClass('stylie ' + settings[2])
        k++
      }
    }

        requestAnimationFrame(hector)
    },100)
    
  }
  
  
  
  
  
}())
