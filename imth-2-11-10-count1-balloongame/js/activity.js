(function () {
  var IMAGE_PATH = '../images/'
  var clickedBalloon = false
  var balloonWaveTimer
  var num = 0
  var showBalloons
  var last = 0  
  var $pausePlay = $('.pausePlay')
  var $icoInfo = $('.icoInfo')
  var $closeBtn = $('.closeBtn')
  var $completeMsg = $('#completeMsg')
  var $borderBalloons = $('.borderBallons')
  var $bottomImage = $('.bottomImage')
  var $restartConfirmation = $('.restartConfirmation')
  var $blocker = $('#blocker')
  var $blockerContainerBackground = $('blockerContainerBackground')
  var $restartCont = $('.restartCont')
  var $test = $('.test')
  var $tales = $('.tales')
  var $backgroundImage = $('.backgroundImage')
  var $sunMoon = $('.sunMoon')
  var $question = $('.question')

  var Star = {
    stars: 0,
    $scores: $('#starScores'),
    add: function () {
      this.stars++
      $('.starPoints.emptyStar').first()
        .removeClass('emptyStar')
        .addClass('filledStar tada')
    },
    remove: function(){
      if (this.stars > 0) {
        $('.starPoints.filledStar').last()
          .removeClass('filledStar tada')
          .addClass('emptyStar')
        this.stars--
      }
    }
  }

  var Balloon = {
    clicked: false,
    balloons:[1, 2, 3, 4, 5, 6],
    colors: ['balloonBlue', 'balloonRed', 'balloonGreen', 'balloonOrange', 'balloonViolet', 'balloonYellow'],
    $balloons: $('.balloon'),
    $heads: $('.balloonHead'),
    setColors: function() {
      for (var i = 1; i <= 6; i++) {      
        $('#balloon' + i).addClass(this.colors[parseInt(Math.random() * 6, 10)])
      }
    },
    inflate: function() {
      if (!Game.pause) {
        if (num <= 6) {
          $('#balloon' + this.balloons[num]).addClass('balloonAnimation')
          $('#tail' + this.balloons[num++]).addClass('tales')
        } else {
          num = 0
          clearInterval(showBalloons)
        }
      }
    },
    randomSort: function(){
      var tmp=[]
      this.balloons=[1,2,3,4,5,6]
      for (var i = 0; i<6; i++) {
        tmp.push(this.balloons.splice(parseInt(Math.random() * this.balloons.length, 10), 1)[0]);
      }
      this.balloons=tmp
      last = this.balloons[5]
    }
  }

  Balloon.$balloons.on('click touchstart', function () {
    var thisBalloonColor = $(this).attr('class').split(' ')[2]
      $(this).removeClass('balloonBlue balloonRed balloonGreen balloonOrange balloonViolet balloonYellow')
      $(this).children('.test')
        .children('.balloonHead')
        .html()

      var algo = $(this).children('.test').children('.balloonHead')
      var algo = $(this.id).selector
    
      setTimeout(function(){
        $('#balloon' + algo.substr(algo.length-1)).fadeTo( 500, 0 )
      },400)
      
      $(this).children('.test')
        .children('.tales')
        .addClass('layer-hide')

      if ($(this).text() == Answer.current) {
        boom.play()
        $(this).children('.test')
          .addClass('explosionAnimation ' + thisBalloonColor + 'Explode')
        $('.txtNumbersCont.txtNumbersDefault.notAnswered').first()
          .removeClass('notAnswered')
          .addClass('txtNumberFound animated fadeInRight false')
          .html(Answer.current)
        $('.txtNumbersCont.txtNumbersDefault.notAnswered').first()
          .addClass('txtNumbersNext')
          .html('?')
        
        Star.add()        
        Answer.current += Answer.INTERVAL
        $('.currentAnswer').text(Answer.current)
      } else {
        deflate.play()
        $(this).children('.test')
          .addClass('deflateAnimation ' + thisBalloonColor + 'Deflate')
        Star.remove()
      }
      clickedBalloon = true
      if ($('.txtNumbersCont.txtNumbersDefault.notAnswered').length === 0) {
        setTimeout(function(){
          claps.play()
          ResetIcon.removeClick()
          clearInterval(balloonWaveTimer)

          if (Level.scores[+Level.current - 1] < Star.stars) {
            Level.scores[+Level.current - 1] = Star.stars
          }
          if (Level.next == Level.current) {
            Level.next++
            $('.levels.levelslocked').first()
              .removeClass('levelslocked')
              .addClass('levelsOpen')
          }

          Balloon.$balloons.addClass('paused')

          $blocker.addClass('blockerContainerBackground').removeClass('layer-hide')
          $question.addClass('layer-hide')
          $restartCont.removeClass('layer-hide')

          Level.$modal.removeClass('resetBackground finishBackground')
          Level.$modal.addClass('veryGoodBackground')
          Level.$modal.addClass('showLevel')

          Star.$scores.addClass('layer-hide')
          if (+Level.current === 8) {
            Level.$restart.addClass('layer-hide')
            Level.$message.addClass('layer-hide')
            $completeMsg.removeClass('layer-hide')

            Level.$modal.addClass('finishBackground')
            Level.$modal.removeClass('veryGoodBackground resetBackground')
          } else {
            Level.$restart.addClass('layer-hide')
            $completeMsg.addClass('layer-hide')
            Level.$message.removeClass('layer-hide')
          }

          $('.starsCollected').html(Star.stars)
          $('#nextLevel').html(Level.next)
          $('#answerInterval').html(Answer.INTERVAL)
          $('#nextLevelCounter').html(Answer.current - Answer.INTERVAL)

          $borderBalloons.addClass('levelAnimateIn')
          $bottomImage.addClass('levelAnimateIn')
        },1800)
      }
 
      $(this).children('.test')
        .children('.balloonHead')
        .html('')    
  })

  var Level = {
    next: 1,
    current:1,
    scores: [0, 0, 0, 0, 0, 0, 0, 0],
    $restart: $('#restartLevel'),
    $modal: $('.levelModal'),
    $message: $('#levelMsg'),
    stages: {1:'day', 2:'afternoon', 3:'sunset', 4:'dusk', 5:'night', 6:'night', 7:'night', 8:'night'},
    setStage: function() {   
      $backgroundImage.addClass(this.stages[this.current])
      if (this.current < 3) {
        $sunMoon.addClass('sun')
      } else {
        $sunMoon.addClass('moon')
      }
    }
  }

  var  Game = {
    pause: false,
    Ballon: Object.create(Balloon),
    reset: function () {
      this.Ballon.$balloons.removeClass('balloonAnimation')
    },
    setPause: function () {
      this.pause = true
      this.Ballon.$heads.addClass('layer-hide')
      this.Ballon.$balloons.addClass('paused')
    },
    play: function(){
      this.pause = false
      this.Ballon.$heads.removeClass('layer-hide')
      this.Ballon.$balloons.removeClass('paused')
    },
    setState: function (label,image) {
      $pausePlay.children('a')
        .html(label)
      $pausePlay.children('img')
        .attr('src', IMAGE_PATH+image)
    }
  }

  var Answer = {
    INTERVAL: 1,
    current: 841,
    Balloons: Object.create(Balloon),
    setAnswers: function () {
      var existAnswer = false;

      for (var i = 0; i <= 5; i++) {
        $('#balloon' + (i + 1))
          .find('.balloonHead')
          .html( parseInt(Math.random() * ((this.current+11) - (this.current-2))) + (this.current-2),10)
      }
      this.Balloons.$heads.each(function(index, value){
        if (+value.innerText === this.current) {
          existAnswer = existAnswer || true
          value.innerText = Math.floor(Math.random() * ((this.current+11) - (this.current-2))) + (this.current-2)            
        }
      })

      if (!existAnswer) {
        $('#balloon' + parseInt(Math.random() * 6 + 1, 10))
        .find('.balloonHead')
        .html(this.current)
      }
    }
  }

  var Icon = {
    addClick: function () {
      this.$icon.css('opacity', '1')
      this.$icon.css('pointer-events', 'auto')
    },
    removeClick:function () {
      this.$icon.css('opacity', '0.5')
      this.$icon.css('pointer-events', 'none')
    }
  }

  var InfoIcon = Object.create(Icon)
  var ResetIcon = Object.create(Icon)
  InfoIcon.$icon = $('.icoInfo')
  ResetIcon.$icon = $('.icoReset')

  InfoIcon.$icon.on('click touchstart', function () {
    Game.setPause()
    if ($pausePlay.children('a').text() !== 'Start') {
      Game.setState('Continue','play.png')
    }
    $blocker.removeClass('layer-hide')
    $('#blockerBackground').removeClass('layer-hide')
    $question.removeClass('layer-hide')
    InfoIcon.removeClick()
  }) 

  ResetIcon.$icon.on('click touchstart', function () {
    this.removeClick()
   
    $blocker.addClass('blockerContainerBackground').removeClass('layer-hide')
    $question.addClass('layer-hide')
    $restartCont.removeClass('layer-hide')
    Game.setPause()
    Level.$modal.removeClass('veryGoodBackground finishBackground')
    Level.$modal.addClass('showLevel resetBackground')
    Star.$scores.addClass('layer-hide')
    Level.$restart.removeClass('layer-hide')
    Level.$message.addClass('layer-hide')
    $completeMsg.addClass('layer-hide')
    $borderBalloons.addClass('levelAnimateIn')
    $bottomImage.addClass('levelAnimateIn')
  }.bind(ResetIcon))

  ResetIcon.removeClick()
 
  $(document).ready(function () {
    $pausePlay.click(function () {
      switch ($(this).children('a').text()) {
        case 'Start':
          Game.play()
          init()
          ResetIcon.addClick()
          Game.setState('Pause', 'pause.png')
          break
        case 'Pause':
          Game.setPause()
          Game.setState('Continue', 'play.png')
          break
        case 'Continue':
          Game.play()
          Game.setState('Pause', 'pause.png')
          break
      }
    })
   
    $('.closeBtn').click(function () {
      $question.addClass('layer-hide')
      if($restartCont.hasClass('layer-hide')){
        $blocker.addClass('layer-hide')
        $('#blockerBackground').addClass('layer-hide')
      }
      InfoIcon.addClick()
    })

    $restartConfirmation.click(function () {
      ResetIcon.addClick()
     
      $blocker.removeClass('blockerContainerBackground').addClass('layer-hide')
      $('#blockerBackground').addClass('layer-hide')
      $restartCont.addClass('layer-hide')
      Game.setState('Continue','play.png')
      Level.$modal.removeClass('showLevel')

      $borderBalloons.removeClass('levelAnimateIn')
      $bottomImage.removeClass('levelAnimateIn')
    })
  })
 
  $(document).on('click', '.levelsOpen', function () {
    clearInterval(balloonWaveTimer)
    Game.reset()
    Game.setPause()
    ResetIcon.removeClick()
    Game.setState('Start','play.png')
   
    $restartCont.addClass('layer-hide')
    Star.stars = 0
    Answer.current = (Answer.INTERVAL * (parseInt($(this).text(), 10) - 1) * 20) + 841
 
    Level.current = $(this).children('h1').text()
    $('.levelDisplay').html('Level ' + Level.current)
 
    Balloon.$balloons
      .removeClass('speed1 speed2 speed3 speed4 speed5 speed6 speed7 speed8')
      .addClass('speed' + Level.current)
    $backgroundImage.removeClass('day afternoon sunset dusk night')
    $sunMoon.removeClass('sun moon')
    Level.setStage()
 
    $('.txtNumbersCont.txtNumbersDefault')
      .removeClass('txtNumberFound animated fadeInRight false txtNumbersNext')
      .addClass('notAnswered')
      .html('')
 
    $('.txtNumbersCont.txtNumbersDefault.notAnswered').first()
      .removeClass('notAnswered')
      .addClass('txtNumberFound animated fadeInRight false')
      .html( Answer.INTERVAL * 20 * (Level.current - 1 ) + 840)

    $('.txtNumbersCont.txtNumbersDefault.notAnswered').first()
      .addClass('txtNumbersNext')
      .html('?')

    $('.starPoints.filledStar')
      .removeClass('filledStar tada')
      .addClass('emptyStar')
   
    $blocker.removeClass('blockerContainerBackground')
      .addClass('layer-hide')

    $('#blockerBackground').addClass('layer-hide')
    Level.$modal.removeClass('showLevel')
    $borderBalloons.removeClass('levelAnimateIn')
    $bottomImage.removeClass('levelAnimateIn')
    
    Game.setState('Start', 'play.png')
  })
 
  $(document).on('click', '.bestScore', function () {
    for (var i = 0; i < 8; i++) {
      $('#scoreLevel' + (i + 1)).html(Level.scores[i])
    }
    Level.$restart.addClass('layer-hide')
    $completeMsg.addClass('layer-hide')
    Level.$message.addClass('layer-hide')
    Star.$scores.removeClass('layer-hide')
  })
 
  function balloonWave () {
    if ($('#balloon' + last).position().top < -250) {
      Balloon.$balloons.removeClass('balloonBlue balloonRed balloonGreen balloonOrange balloonViolet balloonYellow')
      $('.tales').removeClass('layer-hide')
      $('.balloon > .test').removeClass('deflateAnimation explosionAnimation balloonBlueExplode balloonRedExplode balloonGreenExplode balloonOrangeExplode balloonVioletExplode balloonYellowExplode balloonBlueDeflate balloonRedDeflate balloonGreenDeflate balloonOrangeDeflate balloonVioletDeflate balloonYellowDeflate')
      Answer.setAnswers()
      if (clickedBalloon) {
        clickedBalloon = false
      } else {
        Answer.setAnswers()
        Star.remove()
      }
      clearInterval(balloonWaveTimer)
      init()
    }
  }
 
  function init () {
    num = 0
    clearInterval(showBalloons)
    Balloon.randomSort()
    Balloon.$balloons.css('opacity', '1')
    $('.tales').removeClass('layer-hide')
    Balloon.$balloons.removeClass('balloonAnimation balloonBlue balloonRed balloonGreen balloonOrange balloonViolet balloonYellow')
    showBalloons = setInterval(Balloon.inflate.bind(Balloon), 550)
    Answer.setAnswers()
    balloonWaveTimer = setInterval(balloonWave, 10)
    Balloon.setColors()
  } 
})()