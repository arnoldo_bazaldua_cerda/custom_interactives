//array de los posibles valores para la operacion
var posibles =['10+10','10+20','10+30','10+40','10+50','10+60','10+70','10+80','20+10','20+20','20+30','20+40','20+50','20+60','20+70','30+10','30+20','30+30','30+40','30+50','30+60','40+10','40+20','40+30','40+40','40+50','50+10','50+20','50+30','50+40','60+10','60+20','60+30','70+10','70+20','80+10']
//funcion para desordenar un arreglo de manera aleatoria
function shuffleArray(array) {
	for (var i = array.length - 1; i > 0; i--) {
		var j = Math.floor(Math.random() * (i + 1));
		var temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
	return array;
}
// el arreglo de las operaciones pasa a ser h
var h = shuffleArray(posibles)
// cards es un arreglo sera el consultado por el widget
cards=[];
for(var i = 0; i<10;i++){
	cards.push({type:'result',value:eval(h[i])})
	cards.push({type:'operation',value:h[i]})

}
console.log(cards)
