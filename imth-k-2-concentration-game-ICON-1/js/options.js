//global Options that will handle the diferent engines in concentration game
/*var options={
	name:'imth-k-3-concentration',
	//engine:'tenEnginePointsReverse'iimth-k-2-concentration-game-ICON-1,
	//this will be half the real cardNumbers eg: if 20 cards the this will be 10
	//make this a pair
	maxCards:10,
	engine:'fiveToTenSubengine',//subengine K-3-concentration-game
	//preset:5,///works with subengine and it will select automatically some value!
	//engine:'tenEngine',//imth1-1
	//engine:'tenEnginePoints',//imth-k-2-concentration-game-ICON-2
	//engine:'addEngine',//imht-1-3
	//@check must be, the value both must be E.G: 10 , so all values will be evaluated as 10 as result,
	//one card it's 4+6 willbe evaluated as 10
	//samevalue parameter will match both values and both must be the same E.G: 2==2, 3==3.
	//check:10,//imth1-1
	//check:'sameValue',//imth1-3 k-2-concentration-icon1
	//feedBack:'operation',
	//imth-k-3-concentration-game will set this number each time a diferent number is selected
	//switchPlayersOnTry:true,//only imth-k-2-concentration-game-ICON-1
	menu:{
		fn:'fiveToTenMenu',
		template:'points'
		//ennunciated:"Select one of the number cards to begin"
	},
	//template:'pointsAndNumbers'//template used to show numbers, value used to check what feedback must be displayed
	template:'points'//
	//in case cards should be shown front instead of back
	//title:'make ten.'

}*/

//99 de k1
interactiveName="imth-k-2-concentration-game-ICON-1";

console.log('+++DEGUG+++ current activity is: '+interactiveName);
var _configTemplates={
	'imth-1-1-concentration-game':{
		engine:'tenEngine',//imth1-1
		maxCards:10,
		check:10,//imth1-1
		title:"Make 10",
		save:false
	},
	'imth-1-3-concentration-game':{
		maxCards:10,
		engine:'addEngine',
		check:'sameValue',
		feedBack:'operation',
		save:false
	},
	'imth-k-2-concentration-game-ICON-1':{
		maxCards:10,
		engine:'addEngine',
		check:'sameValue',
		switchPlayersOnTry:true,
		engine:'tenEnginePointsReverse',
		template:'pointsAndNumbers',
		save:false
	},
	'imth-k-2-concentration-game-ICON-2':{
		maxCards:10,
		engine:'addEngine',
		check:'sameValue',
		engine:'tenEnginePoints',
		template:'pointsAndNumbers',
		save:false
	},
	'imth-k-3-concentration-game-ICON-1':{
		maxCards:10,
		check:'sameValue',
		engine:'fiveToTenSubengine',//subengine K-3-concentration-game
		preset:5,///works with subengine and it will select automatically some value!
		menu:{
			fn:'fiveToTenMenu',
			template:'points'
		},
		template:'points',
		save:false
		//title:'make five.'
	},
	'imth-k-3-concentration-game-ICON-2':{
		maxCards:10,
		check:'sameValue',
		engine:'fiveToTenSubengine',//subengine K-3-concentration-game
		preset:6,///works with subengine and it will select automatically some value!
		menu:{
			fn:'fiveToTenMenu',
			template:'points'
		},
		template:'points',
		save:false
		//title:'make five.'
	},
	'imth-k-3-concentration-game-ICON-3':{
		maxCards:10,
		check:'sameValue',
		engine:'fiveToTenSubengine',//subengine K-3-concentration-game
		preset:7,///works with subengine and it will select automatically some value!
		menu:{
			fn:'fiveToTenMenu',
			template:'points'
		},
		template:'points',
		save:false
		//title:'make five.'
	},
	'imth-k-3-concentration-game-ICON-4':{
		maxCards:10,
		check:'sameValue',
		engine:'fiveToTenSubengine',//subengine K-3-concentration-game
		menu:{
			fn:'fiveToTenMenu',
			template:'points'
		},
		template:'points',
		save:false
		//title:'make five.'

	},
	'1-4-concentration-game':{
		maxCards:10,
		engine:'substractionEngine',
		check:'sameValue',
		save:false
	},
	'imth-k-5-concentration-game':{
		maxCards:10,
		engine:'game_addition_facts',
		check:'sameValue',
		save:false
		// title:'make five.'
	},
	'imth-k-6-concentration-game':{
		maxCards:10,
		engine:'k6_concentration_game',
		check:'sameValue',
		save:false
		// title:'make five.'
	},
    'imth-1-11-concentration-sub':{
		maxCards:10,
		engine:'concentration_sub_1_11',
		check:'sameValue',
		save:false
    },
    'imth-1-11-concentration-add':{
		maxCards:10,
		engine:'concentration_add_1_11',
		check:'sameValue',
		save:false
    }
}
//////////////////////////// events Callbacks ///////////////////////////////////
options=_configTemplates[interactiveName];
options.name=interactiveName;
var fetchedState;
var evt = document.createEvent("Event");
evt.initEvent("myEvent",true,true);
function fetchCallBack(state){

	fetchedState=JSON.parse(state);
	document.dispatchEvent(evt);
}
function finishCallBack(){

	return {};
}
