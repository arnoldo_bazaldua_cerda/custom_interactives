/* validateNrestart.js */
var myScore = myScore || {}
myScore.service = {
  operations: [],
  Score: 0,
  restartTurn: 0,
  index: 0, // index of the operations
  setScore: function (score) {
    if ((myScore.service.Score + score) >= 0) {
      myScore.service.Score += score
    }
  },
  validate: function (draggable) {
    var value = eval(draggable.find('.bubble').html()) // eslint-disable-line
    var score1 = 0
    if (MathOperations.builder.getAnswer() === parseInt(value, 10)) {
      score1++
      myScore.service.restartTurn++
      myScore.service.timeOutMouth('.mouthEating', '.eating')
      draggable.addClass('hide')
      myApp.audioX.setAudio('#audioTrue')
      myApp.audioX.playA()
      setTimeout(function () {
        draggable.removeClass('hide')
      }, 500)
/*  ********************restart turb set to 10*******************************/
/*      if (myScore.service.restartTurn === 10) {
        myScore.service.setScore(score1)
        myScore.service.restart(false)
        myScore.service.displayCongratulationsMessage()
        myScore.service.restartTurn = 0
      } else {
        myScore.service.restart(true)
      }*/
      value = 0
      var box = draggable.closest('.box')
      box.find('.container').addClass('hide')
      $('div').filter('#sign').removeClass('signAppear')
      setTimeout(function () {
        myScore.service.cloudApear()
        $('div').filter('#sign').addClass('signAppear')
      }, 500)
      setTimeout(function () {
/*        myScore.service.operations = MathOperations.builder.getOperations({
          'operations': myApp.values.fruitAppearing
        })*/
        myScore.service.bruteForceAnwer()
      }, 520)
    } else {
      score1--
      myScore.service.timeOutMouth('.mouthEww', '.wrong')
      myApp.audioX.setAudio('#audioFalse')
      myApp.audioX.playA()
      draggable.removeClass('dragDropHidden').addClass('dragDropVisible')
    }
    myScore.service.setScore(score1)
    $('#total').html(myScore.service.Score)
  },
  timeOutMouth: function (replace1, replace2) {
    $('.mouthNormal').hide()
    $('.face.normal').hide()
    $(replace1).show()
    $('.face' + replace2).show()
    setTimeout(function () {
      $(replace1).hide()
      $('.face' + replace2).hide()
      $('.face.normal').show()
      $('.mouthNormal').show()
    }, 500)
  },
  displayCongratulationsMessage: function () {
    var divCongrat = $('div').filter('#congratFeedback')
    divCongrat.removeClass('congratFeedback-hide')
    divCongrat.find('span').filter('.score').html(myScore.service.Score)
    myScore.service.removeMonsterAndFruits()
    divCongrat.click(function () {
      $(this).addClass('congratFeedback-hide')
      setTimeout(function () {
        $('#typeOfGame').removeClass('hide').addClass('signAppear')
      }, 500)
    })
  },
  cloudApear: function () {
    if (myApp.values.randomCloudOrFruitFlag === 1) {
      myApp.values.showAnswer = ((Math.floor((Math.random() * 2) + 1) === 1) ? true : false) // eslint-disable-line
    }
    MathOperations.builder.generateCloudOperation(myApp.values.showAnswer)
  },
  removeMonsterAndFruits: function () {
    myApp.values.pearsonAppAudioFlag = false
    myApp.audioX.setAudio('#audioBack')
    myApp.audioX.stopA()
    $('.boring').hide()
    $('#eyebrowLeft').removeClass('hard')
    $('#eyebrowRight').removeClass('hard')
    $('#eyeLeft').removeClass('bored')
    $('#eyeRight').removeClass('bored')
    $('#head').removeClass('hard')
    $('div').filter('#sign').removeClass('signAppear')
    $('.meter').removeClass('signAppear').addClass('hide')
    $('#progressBar').css('width', 0)
    $('#progressBar').removeClass()
/*    myScore.service.restart(false)*/
    $('div').filter('.MonsterHolder').hide()
    $('div').filter('.go').filter('.score').removeClass('signAppear').addClass('hide')
    $('.box').each(function (index, value) {
      $(this).find('.container').removeClass('hide')
      $(this).removeAttr('style')
      $(this).removeClass().addClass('box')
      $(this).addClass('hide')
      $(this).find('.container').removeClass('hide')
      $(this).find('.fondo').removeClass().addClass('fondo')
      $(this).find('.fruitHolder').removeClass().addClass('fruitHolder')
      $(this).find('.bubble').html()
    })
  },
  playAgainEvent: function () {
    $('div#congratFeedback').addClass('congratFeedback-hide')
    $('div').filter('.go').filter('.score').find('span').filter('#total').html('0')
      // reset the score
    myScore.service.Score = 0
    setTimeout(function () {
      $('div').filter('#startGame').filter('.go').removeClass('hide')
      $('div').filter('.go').filter('.score').removeClass('hide')
      myScore.service.cloudApear()
      $('div').filter('.MonsterHolder').show(function () {
        $('div').filter('#sign').addClass('signAppear')
        $('div').filter('.go').filter('.score').addClass('signAppear')
      })
    }, 500)
  },
  bruteForceAnwer: function () { // force the anwers to apear in the second positions of the array
    var Cloudanswer = MathOperations.builder.CloudOp
    var answer = [Cloudanswer, '-', 0]
    var indexPlus = myScore.service.index + 2  // set the postion of the anwers
    var indexBounds = indexPlus < myApp.values.fruitAppearing ? indexPlus : indexPlus - myApp.values.fruitAppearing
    myScore.service.operations[indexBounds] = answer
    /* console.log('answer is in:' + indexBounds)*/
  },
  IsTheFruitOut: function () {
    var allAnimationsEnd = 0
    $('div').on('animationend', '.box', function (e) {
      allAnimationsEnd++
      if (allAnimationsEnd > myApp.values.fruitAppearing) {
        allAnimationsEnd = 1
      }
      if (allAnimationsEnd === 1) {
        myScore.service.operations = MathOperations.builder.getOperations()
        myScore.service.bruteForceAnwer()
      }
      var fruit = $(this).clone()
      $(this).remove()
      if ($(fruit).find('.img').hasClass('dragDropHidden')) {
        $(fruit).find('.img').removeClass('dragDropHidden').addClass('dragDropVisible')
      }
      myScore.service.index = allAnimationsEnd - 1 >= 0 || allAnimationsEnd - 1 <= 6 ? allAnimationsEnd - 1 : 0 // if out of bounds reset to cero.
     /* console.log('operations[' + myScore.service.index + '] = ' + myScore.service.operations[myScore.service.index])*/
      walkingFood.service.generateFruit(myApp.values.showAnswer, fruit, myScore.service.operations[myScore.service.index])
    })
  }
}
$(document).ready(function () {
  $('div').filter('#sign').addClass('signAppear')
  myScore.service.IsTheFruitOut()
})
