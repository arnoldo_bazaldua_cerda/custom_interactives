/* ************************ WalingFood.js **************************************
 **
 ************************* ********************************** **/
var walkingFood = walkingFood || {}
walkingFood.values = {
  classBoxPos: ['first', 'second', 'third', 'fourth', 'fifth', 'sixth'],
  boxPositionAux: null
}
walkingFood.service = {
/* generates a random number to set the position of the box */
  generateFruit: function (isAnswerShowed, box, operation) {
    var fruitN = []
    var numberFruit = null
    var equation = ''
    var fruitNameN = null
    var fruitName = null
    var equationEval = null
    equation = operation[0] + operation[1] + operation[2]
    equationEval = eval(equation) // eslint-disable-line
    numberFruit = equationEval >= 10 ? 10 : equationEval
    numberFruit = numberFruit === 0 ? 1 : numberFruit
    var index = Math.floor((Math.random() * myApp.values.fruitAppearing) + 1)
    index = walkingFood.service.generateRandomIndex(index)
    var boxPosition = 0
    if (walkingFood.values.boxPositionAux === null) {
      boxPosition = Math.floor((Math.random() * 5))
      walkingFood.values.boxPositionAux = boxPosition
    } else {
      boxPosition = walkingFood.service.verifyBoxPosition()
    }
    var $container = $(box)
    $container
      .not('.noneFruits')
      .removeClass()
      .addClass('box')
      .css('animation-name', 'fruitMove')
      /* .addClass('boxAnimation')*/
      .css('animation-timing-function', 'linear')
      .css('-webkit-animation-timing-function', 'linear')
      .css('animation-name', 'fruitMove')
      .css('animation-delay', '0s')
    $container.addClass(walkingFood.values.classBoxPos[boxPosition])
    fruitNameN = myRandom.service.getRandomClass(0, 11, fruitN)
    fruitName = myApp.values.classNames[fruitNameN]
    var fondo = $container.find('.fondo')
    $(fondo)
      .removeClass()
      .addClass(' fondo')
      .addClass(fruitName)
      .addClass(fruitName + '-' + numberFruit)
      .addClass('fondoShake')
    var fruitHolder = $container.find('.fruitHolder')
    $(fruitHolder).removeClass()
    $(fruitHolder).addClass('fruitHolder').addClass('fruitHolder-' + fruitName).addClass(fruitName + '-' + numberFruit)
    if (!$container.find('.img').hasClass('ui-draggable')) {
      $container.find('.img').addClass('ui-draggable')
    }
    if ($container.find('.container').hasClass('hide')) {
      $container.find('.container').removeClass('hide')
    }
    /* $(box).addClass('withoutDelay')*/
    $container.find('.bubble').html(!isAnswerShowed ? equationEval : equation)
    // add the box to '.dsp' div
    $('.dsp').append(box)
    myDragDrop.service.drag('.ui-draggable')
  },
  generateRandomIndex: function (index) {
    if (myScore.service.temp === index) {
      index = Math.floor((Math.random() * myApp.values.fruitAppearing) + 1)
      return walkingFood.service.generateRandomIndex(index)
    } else {
      myScore.service.temp = index
      return index
    }
  },
  /* generates a random number to set the position of the box */
  generateRandomToAppearAtTheBegin: function (index, auxBox) {
    if (auxBox.length > 0) {
      for (var x in auxBox) {
        if (auxBox[x] === index) {
          index = Math.floor((Math.random() * myApp.values.fruitAppearing) + 1)
          return walkingFood.service.generateRandomToAppearAtTheBegin(index, auxBox)
        }
      }
    }
    return index
  },
  verifyBoxPosition: function () {
    var upper = 5
    var lower = 0
    var limit = 0
    if (walkingFood.values.boxPositionAux >= lower && walkingFood.values.boxPositionAux <= 2) {
      limit = walkingFood.values.boxPositionAux + 3
      walkingFood.values.boxPositionAux = Math.floor(Math.random() * ((upper + 1) - (limit - 1))) + (limit - 1)
    } else if (walkingFood.values.boxPositionAux >= 3 && walkingFood.values.boxPositionAux <= upper) {
      limit = walkingFood.values.boxPositionAux - 3
      walkingFood.values.boxPositionAux = Math.floor(Math.random() * limit) + lower
    }
    return walkingFood.values.boxPositionAux
  }
}
