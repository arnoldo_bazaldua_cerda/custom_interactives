/* ************************ WalkingFood.js **************************************
 **
 ************************* ********************************** **/
var walkingFood = walkingFood || {}
walkingFood.values = {
  classBoxPos: ['first', 'second', 'third', 'fourth', 'fifth', 'sixth'],
  boxPositionAux: null
}
walkingFood.service = {
/* generates a random number to set the position of the box */
  generateFruit: function (isAnswerShowed, box, operation) {
    var fruitN = []
    var numberFruit = null
    var equation = ''
    var fruitNameN = null
    var fruitName = null
    var equationEval = null
    if (operation === undefined) {
      var undefOp = MathOperations.builder.getOperations({ //  eslint-disable-line
        'operations': 1,
        'minNumOfAnwers': 0
      })
      operation = undefOp[0]
    }
    equation = operation[0] + operation[1] + operation[2]
    equationEval = eval(equation) // eslint-disable-line
    numberFruit = equationEval >= 10 ? 10 : equationEval
    var index = Math.floor((Math.random() * myApp.values.fruitAppearing) + 1)
    index = walkingFood.service.generateRandomIndex(index)
    var boxPosition = 0
    if (walkingFood.values.boxPositionAux === null) {
      boxPosition = Math.floor((Math.random() * 5))
      walkingFood.values.boxPositionAux = boxPosition
    } else {
      boxPosition = walkingFood.service.verifyBoxPosition()
    }
        // myTimer.service.setFruitOffset(this, index)
    $(box).removeClass()
      .addClass('box')
      .css('animation-timing-function', 'linear')
      .css('-webkit-animation-timing-function', 'linear')
      .css('animation-name', 'fruitMove')
      .css('animation-delay', '0s')
     /* .removeAttr('style')
      .css('animation-name', 'fruitMove')
      .css('animationPlayState', 'play')*/
    $(box).addClass(walkingFood.values.classBoxPos[boxPosition])
    fruitNameN = myRandom.service.getRandomClass(0, 11, fruitN)
    fruitName = myApp.values.classNames[fruitNameN]
    var fondo = $(box).find('.fondo')
    $(fondo).removeClass()
    $(fondo).addClass(' fondo').addClass(fruitName).addClass(fruitName + '-' + numberFruit)
    var fruitHolder = $(box).find('.fruitHolder')
    $(fruitHolder).removeClass()
    $(fruitHolder).addClass('fruitHolder').addClass('fruitHolder-' + fruitName).addClass(fruitName + '-' + numberFruit)
    if (!$(box).find('.img').hasClass('ui-draggable')) {
      $(box).find('.img').addClass('ui-draggable')
    }
    if ($(box).find('.container').hasClass('hide')) {
      $(box).find('.container').removeClass('hide')
    }
    /* $(box).addClass('withoutDelay')*/
   /* $(box).css('-webkit-transition-delay', '0s')*/
    $(box).find('.bubble').html(!isAnswerShowed ? equationEval : equation)
    // add the box to '.dsp' div
    $('.dsp').append(box)
    myDragDrop.service.drag('.ui-draggable')
  },
  generateRandomIndex: function (index) {
    if (myScore.service.temp === index) {
      index = Math.floor((Math.random() * myApp.values.fruitAppearing) + 1)
      return walkingFood.service.generateRandomIndex(index)
    } else {
      myScore.service.temp = index
      return index
    }
  },
  /* generates a random number to set the position of the box */
  generateRandomToAppearAtTheBegin: function (index, auxBox) {
    if (auxBox.length > 0) {
      for (var x in auxBox) {
        if (auxBox[x] === index) {
          index = Math.floor((Math.random() * myApp.values.fruitAppearing) + 1)
          return walkingFood.service.generateRandomToAppearAtTheBegin(index, auxBox)
        }
      }
    }
    return index
  },
  verifyBoxPosition: function () {
    var upper = 5
    var lower = 0
    var limit = 0
    if (walkingFood.values.boxPositionAux >= lower && walkingFood.values.boxPositionAux <= 2) {
      limit = walkingFood.values.boxPositionAux + 3
      walkingFood.values.boxPositionAux = Math.floor(Math.random() * ((upper + 1) - (limit - 1))) + (limit - 1)
    } else if (walkingFood.values.boxPositionAux >= 3 && walkingFood.values.boxPositionAux <= upper) {
      limit = walkingFood.values.boxPositionAux - 3
      walkingFood.values.boxPositionAux = Math.floor(Math.random() * limit) + lower
    }
    return walkingFood.values.boxPositionAux
  }
}
