/* ************************ timer.js **************************************
 **
 ********************************************************** **/
var myTimer = myTimer || {}
myTimer.values = {
  globalTime: 120,
  fruitSpeed: null
}
myTimer.service = {
  $progressBar: $('#progressBar'),
  setFruitOffset: function (box, index) {
    index = index - 1
    var offset = (myTimer.values.fruitSpeed * index / (myApp.values.fruitAppearing))
    $(box).css('animation-delay', offset + 's')
  },
  startTimer: function () {
    this.$progressBar
      .css('width', 0)
      .css('animation-duration', myTimer.values.globalTime + 's')
      .addClass('timeActive')
    $('.box').css('animation-duration', myTimer.values.fruitSpeed + 's')
  },
  endGame: function () {
    this.$progressBar.on('animationend', function (e) {
      myScore.service.displayCongratulationsMessage()
      $('#progressBar').removeClass('timeActive')
    })
  }
}
