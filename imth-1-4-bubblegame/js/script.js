$(function () {
    reset();
});

$(document).ready(function () {

    $(document).bind(
        'touchmove',
        function (e) {
            e.preventDefault();
        }
    );
    $(".boxContainerEquation").css("visibility", "hidden");
    iModal();
    createInitDialog();
    setImageAvatars(-1);
    
    setVisibleScores(false);
    globalProblems = createProblems();
    globalIndexProblem = 0;
    currentDataToSave = getDataToSave("main");
});

var players = 0,
    playersData = null,
    playerIndex = 0,
    globalProblems = null,
    globalIndexProblem = 0;
var timePlayer1 = 0,
    timePlayer2 = 0,
    pausePlayer1 = false,
    pausePlayer2 = false,
    timer = null;

function setImageAvatars(players) {
    var boxContainerPlayerHeader = $(".boxContainerPlayerHeader > div");
    var boxContainerPlayer = $(".boxContainerPlayer");
    if (players == 1) {
        $(boxContainerPlayerHeader[0]).addClass("delphin");
        if (boxContainerPlayer != undefined && boxContainerPlayer.length == 2) {
            $(boxContainerPlayer[0]).show();
            $(boxContainerPlayer[1]).hide();
        }
    } else if (players == 2) {
        $(boxContainerPlayerHeader[0]).removeClass("delphin").addClass("octopus");
        $(boxContainerPlayerHeader[1]).removeClass("otter").addClass("otter");
        if (boxContainerPlayer != undefined && boxContainerPlayer.length == 2) {
            $(boxContainerPlayer[0]).show();
            $(boxContainerPlayer[1]).show();
        }
    } else {
        $(boxContainerPlayerHeader[0]).removeClass("delphin").removeClass("octopus");
        $(boxContainerPlayerHeader[1]).removeClass("otter");
        if (boxContainerPlayer != undefined && boxContainerPlayer.length == 2) {
            $(boxContainerPlayer[0]).hide();
            $(boxContainerPlayer[1]).hide();
        }
    }
}

function getTimer(index) {
    var tmr = 0;
    if (index == 0)
        tmr = parseInt(timePlayer1);
    else if (index == 1)
        tmr = parseInt(timePlayer2);
    return tmr;
}

function pauseTimer(index) {
    if (index == 0)
        pausePlayer1 = true;
    else if (index == 1)
        pausePlayer2 = true;
}

function readyTimer(index) {
    if (index == 0)
        pausePlayer1 = false;
    else if (index == 1)
        pausePlayer2 = false;
}

function stopTimer() {
    if (timer != null) {
        clearInterval(timer);
        timer = null;
    }
}

function resetTimer() {
    stopTimer();
    timePlayer1 = 0;
    timePlayer2 = 0;
}

function startTimer() {
    stopTimer();
    resetTimer();
    if (timer == null) {
        timer = setInterval(function () {
            if (!pausePlayer1)
                timePlayer1++;
            if (!pausePlayer2)
                timePlayer2++;
        }, 1000);
    }
}

function createInitDialog() {

    $(".player1InitGame_iModal, .player2InitGame_iModal").on("click", function () {
        players = parseInt($(this).attr("id").replace("buttom", ""));
        $(".selectPlayers").css("display", "none");
        $(".enterNick").css("display", "none");
        if (players == 1)
            playersData = [{}];
        else if (players == 2)
            playersData = [{}, {}];
        //playerIndex = 0;
        $('.icoReset').removeClass('icoResetMove');
        initGame();
    });
}

function initGame() {
    setImageAvatars(players);
    playersData[0].name = "xxx";
    playersData[0].responses = [];
    playersData[0].score = 0;

    playerIndex = 0;
    if (players == 2) {
        playerIndex = 1;
        playersData[playerIndex].name = "xxx";
        playersData[playerIndex].responses = [];
        playersData[playerIndex].score = 0;
    }

    initScoreBoxes();
    $(".boxContainerPlayerScore > div> h3").html(0);
}

function setScoreValue(index) {

    var playerScore = $(".boxContainerPlayerScore > div > h3");
    $(playerScore[index]).html(parseInt($(playerScore[index]).html()) + 1);

}

function getScoreValue(index) {
    var playerScore = $(".boxContainerPlayerScore > div > h3");
    return parseInt($(playerScore[index]).html());
}

function initScoreBoxes() {
    //startTimer();
    
    var playerHeader = $(".boxContainerPlayerHeader > h3");
    $(".boxPlayerContainer1").removeClass("enable disable")
    $(".boxPlayerContainer2").removeClass("enable disable")
    console.log($('.boxPlayerContainer2').attr('class'))
    if (players == 1) {
        $($(".boxContainerPlayer")[1]).removeClass("enable").removeClass("disable").addClass("disable");
        $(playerHeader[0]).html(playersData[0].name);
        playerIndex = 3
        console.log(playerIndex)
    } else if (players == 2) {
      if (Math.round(Math.random()) == 0) {
        $(".boxPlayerContainer2").children(".boxContainerPlayerScore").addClass("disabled");	 
        $(".boxPlayerContainer1").children(".boxContainerPlayerScore").removeClass("disabled");
        $(".octopus").removeClass("disabled");
        $(".otter").addClass("disabled");
        //$($(".boxContainerPlayer")[0]).removeClass("enable").removeClass("disable").addClass("enable");
        setTimeout(function () {
          $($(".boxContainerPlayer")[1]).removeClass("enable").removeClass("disable").addClass("disable");
        }, 1);
        
		$($(".boxContainerPlayer")[0]).removeClass("enable").removeClass("disable").addClass("enable");
        $(playerHeader[1]).html(playersData[1].name);
        $(playerHeader[0]).html(playersData[0].name);
        playerIndex = 0
        //playersData[0].responses.push('i')
        //playersData[0].responses.push('i')
        
        //setScoreValue(0, 0);
      } else {
        $(".boxPlayerContainer2").children(".boxContainerPlayerScore").removeClass("disabled");	 
        $(".boxPlayerContainer1").children(".boxContainerPlayerScore").addClass("disabled");
        $(".octopus").addClass("disabled");
        $(".otter").removeClass("disabled");
        //$($(".boxContainerPlayer")[0]).removeClass("enable").removeClass("disable").addClass("enable");
        setTimeout(function () {
          $($(".boxContainerPlayer")[0]).removeClass("enable").removeClass("disable").addClass("disable");
        }, 1);
        
		$($(".boxContainerPlayer")[1]).removeClass("enable").removeClass("disable").addClass("enable");
        $(playerHeader[0]).html(playersData[0].name);
        $(playerHeader[1]).html(playersData[1].name);
        playerIndex = 1
        //setScoreValue(1, 0);
      }
      
    }
    //setScoreValue(0, 0);
    //setScoreValue(1, 0);
    $(".boxContainerBubbles").show();
    addBubbles();
}

function addBubbles() {
    $(".boxContainerEquation").css("visibility", "");
    $(".boxContainerBubbles").empty();
    $(".boxContainerEquation").fadeIn();

    for (var i = 0; i < 4; i++) {
        var item = "<div id='bubble" + i + "' class='bubbleContainer'><div class='bubble'><div class='bubble_element bubble_floating_" + i + "'></div><h3>-1</h3><div class='bubble_element " + " correctBackground bubble_floating_" + i + " ' ></div></div></div>";
        $(".boxContainerBubbles").append(item);
    }
    var currentProblem = this.globalProblems[this.globalIndexProblem];
    fillEquation(currentProblem.minuend + "-" + currentProblem.subtractend);

    $('.bubbleContainer').on("click", clickBubble);
    var bubbleValues = $('.bubbleContainer').find(".bubble").find("h3");
  //console.log(bubbleValues);
    for (var i = 0; i < bubbleValues.length; i++) {
        $(bubbleValues[i]).html(currentProblem.responses[i]);
        $('#bubble' + i).css({
            top: (i * 107),
            left: (Math.floor((Math.random() * 5) + 1) * 117) - 120,
            position: 'absolute'
        });
    }
}

function jump(isCorrect) {
    var timeAnimation = (isCorrect) ? 4000 : 4500;
    var timeAudio = (isCorrect) ? 2000 : 1000;
    if (players == 1) {
        setTimeout(function () {
            if (isCorrect) {

                setScoreValue(0, 1)
                try {
                    correct_audio4.src = ''
                } catch (e) {}
                try {
                    delete correct_audio4;
                } catch (e) {}
                try {
                    correct_audio4 = new Audio('../audio/effects/burbuja.mp3');
                } catch (e) {}
                try {
                    correct_audio4.play()
                } catch (e) {}

            } else {

                try {
                    incorrect_audio5.src = ''
                } catch (e) {}
                try {
                    delete incorrect_audio5;
                } catch (e) {}
                try {
                    incorrect_audio5 = new Audio('../audio/effects/burbuja_repele.mp3');
                } catch (e) {}
                try {
                    incorrect_audio5.play()
                } catch (e) {}
            }
        }, timeAudio);

        setTimeout(function () {
            $($(".boxPlayerContainer1 > .boxContainerPlayerHeader > div").addClass("popPlayer").removeClass("boxPlayerContainer1 boxPlayerContainer2").get(0)).on('webkitAnimationEnd oAnimationEnd msAnimationEnd animationend', endPopPlayerAnimationFunction);
        }, (timeAnimation));
    } else if (players == 2) {

        if (playerIndex == 0) {
            setTimeout(function () {
                if (isCorrect) {

                    setScoreValue(0, 1)

                    try {
                        correct_audio4.src = ''
                    } catch (e) {}
                    try {
                        delete correct_audio4;
                    } catch (e) {}
                    try {
                        correct_audio4 = new Audio('../audio/effects/burbuja.mp3');
                    } catch (e) {}
                    try {
                        correct_audio4.play()
                    } catch (e) {}
                } else {

                    try {
                        incorrect_audio5.src = ''
                    } catch (e) {}
                    try {
                        delete incorrect_audio5;
                    } catch (e) {}
                    try {
                        incorrect_audio5 = new Audio('../audio/effects/burbuja_repele.mp3');
                    } catch (e) {}
                    try {
                        incorrect_audio5.play()
                    } catch (e) {}
                }
            }, timeAudio);

            setTimeout(function () {
                $($(".boxPlayerContainer2 > .boxContainerPlayerHeader > div").addClass("popPlayer").removeClass("boxPlayerContainer2").get(0)).on('webkitAnimationEnd oAnimationEnd msAnimationEnd animationend', endPopPlayerAnimationFunction);
            }, timeAnimation);

        } else if (playerIndex == 1) {
            setTimeout(function () {
                if (isCorrect) {
                    setScoreValue(1, 1)
                    try {
                        correct_audio4.src = ''
                    } catch (e) {}
                    try {
                        delete correct_audio4;
                    } catch (e) {}
                    try {
                        correct_audio4 = new Audio('../audio/effects/burbuja.mp3');
                    } catch (e) {}
                    try {
                        correct_audio4.play()
                    } catch (e) {}
                } else {
                    try {
                        incorrect_audio5.src = ''
                    } catch (e) {}
                    try {
                        delete incorrect_audio5;
                    } catch (e) {}
                    try {
                        incorrect_audio5 = new Audio('../audio/effects/burbuja_repele.mp3');
                    } catch (e) {}
                    try {
                        incorrect_audio5.play()
                    } catch (e) {}
                }
            }, timeAudio);

            setTimeout(function () {
                $($(".boxPlayerContainer1 > .boxContainerPlayerHeader > div").addClass("popPlayer").removeClass("boxPlayerContainer1").get(0)).on('webkitAnimationEnd oAnimationEnd msAnimationEnd animationend', endPopPlayerAnimationFunction);
            }, timeAnimation);

        }
    }
}

var clickBubble = function (e) {
    $('.bubbleContainer').off("click", clickBubble);

        var ref = $(this);
        var correctResponse = globalProblems[globalIndexProblem].response;
        var clickedResponse = parseInt(ref.find(".bubble").find("h3").html())
        var isCorrectResponse = (correctResponse === clickedResponse) ? true : false;
        jump(isCorrectResponse);
        if (isCorrectResponse)
            animationCorrectClick(ref, e);
        else
            animationIncorrectClick(ref, e, correctResponse);
        var millisSetTimeout = (isCorrectResponse) ? 100 : 500;
        setTimeout(function () {
            var clickedResponse = parseInt(ref.find(".bubble").find("h3").html())
            var id = ref.attr("id").replace("bubble", "")
              setTimeout(function () {
                
                if (players == 1) {
                  
                    readyTimer(0);
                    readyTimer(1);
                    
                    if (globalIndexProblem < 9) {
                        globalIndexProblem++;
                        addBubbles();
                      console.log(playerIndex)
                        turnEnableUIPlayer(3);
                    } else {
                        showScores();
                    }
                } else if (players == 2) {
                    var responses = playersData[playerIndex].responses;
                    responses.push(clickedResponse);


                    var responses1 = playersData[0].responses;
                    var responses2 = playersData[1].responses;
                    if (playerIndex == 1) {
                      console.log("responses1.length > responses2.length")
                        pauseTimer(0);
                        readyTimer(1);
                      playerIndex = 0
                    } else {
                      console.log("responses2.length > responses1.length")
                      console.log(responses2.length)
                      console.log(responses1.length)
                        pauseTimer(1);
                        readyTimer(0);
                        playerIndex = 0;
                        
                      playerIndex = 1
                    }
                  readyTimer(playerIndex);
                  turnEnableUIPlayer(playerIndex);
                  if (globalIndexProblem < 19) {
                    turnEnableUIPlayer(playerIndex);
                    //console.log(globalIndexProblem);
                    globalIndexProblem++;
                    addBubbles();
                  } else
                    //turnEnableUIPlayer(playerIndex);
                    showScores();
                }
            }, 3400);            
        }, millisSetTimeout);

}

function animationIncorrectClick(ref, e, correct) {
    var bubbleIncorrect = $('.bubbleContainer').filter(function () {
        if ($(this).css('visibility') == "visible")
            return $(this);
    }).find(".bubble").find("h3").filter(function (index) {
        var valueBuble = parseInt($(this).html());
        if (valueBuble !== parseInt(correct))
            return this;
    }).parent();

    var bubbleCorrect = $('.bubbleContainer').filter(function () {
        if ($(this).css('visibility') == "visible")
            return $(this);
    }).find(".bubble").find("h3").filter(function (index) {
        var valueBuble = parseInt($(this).html());
        if (valueBuble === parseInt(correct))
            return this;
    }).parent().children()[0];
    bubbleCorrect = $(bubbleCorrect).addClass("correct");
    $(bubbleCorrect.get(0)).on('webkitAnimationEnd oAnimationEnd msAnimationEnd animationend', endanimationIncorrectClickFunction);

    setTimeout(function () {
        $(bubbleIncorrect.addClass("floatAway").get(0)).on('webkitAnimationEnd oAnimationEnd msAnimationEnd animationend', endanimationFloatAwayFunction);
    }, 800);

}

function endanimationFloatAwayFunction(e) {
    $('.bubbleContainer').filter(function () {
        if ($(this).css('visibility') == "visible")
            return $(this);
    }).on("click", clickBubble);

    var floatAway = $(".floatAway");
    $(floatAway.get(0)).off('webkitAnimationEnd oAnimationEnd msAnimationEnd animationend', endanimationFloatAwayFunction);
    floatAway.removeClass("floatAway");
}

function endanimationIncorrectClickFunction(e) {
    $($(e.target).removeClass('correct').get(0)).off('webkitAnimationEnd oAnimationEnd msAnimationEnd animationend', endanimationIncorrectClickFunction);
}

function animationCorrectClick(ref, e) {
    ref.find(".bubble").addClass("correct-Elemnt")
    $(".bubble").each(function (index, elemnt) {
        if ($(elemnt).hasClass("correct-Elemnt")) {
            $(elemnt).find(".correctBackground").css({
                "opacity": "0.6"
            })
        } else {
            $(elemnt).fadeOut()
        }

    })
    setTimeout(function () {
        $(".bubble_element").fadeOut()
    }, 1700)
    setTimeout(function () {
        $(".bubble").find("h3").fadeOut()
    }, 3000)

}



function endPopPlayerAnimationFunction(e) {
    $($(e.target).removeClass('popPlayer').get(0)).off('webkitAnimationEnd oAnimationEnd msAnimationEnd animationend', endPopPlayerAnimationFunction);
}

function turnEnableUIPlayer(index) {
  $(".otter").removeClass("disabled");
  $(".octopus").removeClass("disabled");
  
    if (index == 0) {
        playerIndex = index;

        $(".octopus").removeClass("disabled");
        $(".otter").addClass("disabled");
        $(".boxPlayerContainer1").children(".boxContainerPlayerScore").removeClass("disabled");
        $(".boxPlayerContainer2").children(".boxContainerPlayerScore").addClass("disabled");

        $(".boxPlayerContainer1").removeClass("disable").addClass("enable");
        $(".boxPlayerContainer2").removeClass("enable").addClass("disable");
        currentDataToSave = getDataToSave("playing");
    } else if (index == 1) {
        playerIndex = index;

        $(".otter").removeClass("disabled");
        $(".octopus").addClass("disabled");
        $(".boxPlayerContainer2").children(".boxContainerPlayerScore").removeClass("disabled");
        $(".boxPlayerContainer1").children(".boxContainerPlayerScore").addClass("disabled");


        $(".boxPlayerContainer2").removeClass("disable").addClass("enable");
        $(".boxPlayerContainer1").removeClass("enable").addClass("disable");
        currentDataToSave = getDataToSave("playing");
    } else {

        $(".octopus").removeClass("disabled");
        $(".otter").removeClass("disabled");
        $(".boxPlayerContainer1").children(".boxContainerPlayerScore").removeClass("disabled");
        $(".boxPlayerContainer2").children(".boxContainerPlayerScore").removeClass("disabled");

        //$(".boxPlayerContainer1").removeClass("enable").addClass("disable");
        $(".boxPlayerContainer2").removeClass("enable").addClass("disable");
    }
    calculateScore();

    return false;
}

var currentDataToSave = null; 
var lockGetDataToSave = false;

function getDataToSave(screenState) {
    var data = {};
    if (!lockGetDataToSave) {
        data.scorePlayer1 = parseInt($(".boxPlayerContainer1 > .boxContainerPlayerScore > div > h3").html());
        data.scorePlayer2 = parseInt($(".boxPlayerContainer2 > .boxContainerPlayerScore > div > h3").html());
        data.globalIndexProblem = globalIndexProblem;
        data.globalProblems = globalProblems;
        data.pausePlayer1 = pausePlayer1;
        data.pausePlayer2 = pausePlayer2;
        data.playersData = playersData;
        data.playerIndex = playerIndex;
        data.timePlayer1 = timePlayer1;
        data.timePlayer2 = timePlayer2;
        data.screenState = screenState;
        data.players = players;
    }
    return data;
}

function resetDataToSave() {
    stopTimer();
    resetTimer();

    lockGetDataToSave = true;
    currentDataToSave = null;

    players = 0;
    playersData = null;
    playerIndex = 0;
    globalProblems = null;
    globalIndexProblem = 0;
    timePlayer1 = 0;
    timePlayer2 = 0;
    pausePlayer1 = false;
    pausePlayer2 = false;
    timer = null;

    lockGetDataToSave = false;
    $(".imagesBackgroundMain").css("display", "");
    $(".imagesBackgroundScores").css("display", "none");

    $(".boxContainerEquation").css("visibility", "hidden");
    //createInitDialog();
    setVisibleScores(false);
    globalProblems = createProblems();
    globalIndexProblem = 0;

    setImageAvatars(-1);
    $(".boxContainerBubbles").empty();

  $(".otter").removeClass("disabled");
  $(".octopus").removeClass("disabled");
    currentDataToSave = getDataToSave("main");
}

function showScores() {
    stopTimer();
    currentDataToSave = getDataToSave("score");

    var scorePlayer1 = getScoreValue(0);
    var scorePlayer2 = getScoreValue(1);

    var tmrPlayer1 = getTimer(0);
    var tmrPlayer2 = getTimer(1);
    var minTimer = 0;
    if (players == 1) {
        minTimer = tmrPlayer1;
    } else if (players == 2) {
        if (tmrPlayer1 < tmrPlayer2)
            minTimer = tmrPlayer1;
        else if (tmrPlayer2 < tmrPlayer1)
            minTimer = tmrPlayer2;
        else
            minTimer = tmrPlayer1;
    }
    minTimer = parseInt(minTimer);
    if (minTimer > 60) {
        var minutes = Math.floor(minTimer / 60);
        var seconds = minTimer - minutes * 60;
        minTimer = minutes + ":" + seconds + " min";
    } else {
        minTimer = "00:" + minTimer + " sec";
    }

    createScore();
    setScoreTime(minTimer);

    setVisibleMain(false);
    setVisibleScores(true);

    if (players == 1)
        setFinalScore(0, scorePlayer1);
    else if (players == 2) {
        setFinalScore(0, scorePlayer1);
        setFinalScore(1, scorePlayer2);
    }
    $(".boxContainerScoreTime").hide()
}

function calculateTrueResponses(userResponses, player) {
    var totalTrue = 0;
    var count = userResponses.responses
    var user = userResponses.player
    for (var i = 0; i < count.length; i++) {
        if (parseInt(player) === 2) {
            if (parseInt(this.globalProblems[i + 1].response) == parseInt(count[i]))
                totalTrue++;
        } else {
            if (parseInt(this.globalProblems[i].response) == parseInt(count[i]))
                totalTrue++;
        }
    }
    return parseInt(totalTrue);
}

function calculateScore() {
    return false;
    if (players == 1) {
        var responsesPlayer1 = playersData[0];
        if (responsesPlayer1.responses.length > 0)
            responsesPlayer1 = calculateTrueResponses(responsesPlayer1, 1);
        else
            responsesPlayer1 = 0;
        setScoreValue(0, responsesPlayer1);
    } else if (players == 2) {
        var responsesPlayer1 = playersData[0];
        var responsesPlayer2 = playersData[1];
        if (responsesPlayer1.responses.length > 0) {
            responsesPlayer1 = calculateTrueResponses(responsesPlayer1, 1);
        } else {
            responsesPlayer1 = 0;
        }
        if (responsesPlayer2.responses.length > 0) {
            responsesPlayer2 = calculateTrueResponses(responsesPlayer2, 2);
        } else {
            responsesPlayer2 = 0;
        }
        setScoreValue(0, responsesPlayer1);
        setScoreValue(1, responsesPlayer2);
    }

}

function createMinuend() {
    return getRandomInt(1, 10);
}

function createSubtractend() {
    return getRandomInt(0, 10);
}

function createProblems() {

    var problems=[{minuend:18,response:9,responses:[8,7,9,3],subtractend:9},{minuend:17,response:8,responses:[8,7,6,2],subtractend:9},{minuend:14,response:7,responses:[11,5,7,8],subtractend:7},{minuend:12,response:3,responses:[3,8,6,4],subtractend:9},{minuend:15,response:9,responses:[7,9,10,6],subtractend:6},{minuend:17,response:9,responses:[7,9,10,6],subtractend:8},{minuend:16,response:7,responses:[7,8,2,6],subtractend:9},{minuend:16,response:8,responses:[7,8,2,9],subtractend:8},{minuend:16,response:9,responses:[5,8,10,9],subtractend:7},{minuend:15,response:6,responses:[5,6,10,7],subtractend:9},{minuend:15,response:7,responses:[5,6,3,7],subtractend:8},{minuend:15,response:8,responses:[8,6,12,7],subtractend:7},{minuend:14,response:5,responses:[5,6,4,1],subtractend:9},{minuend:14,response:6,responses:[5,6,4,0],subtractend:8},{minuend:14,response:8,responses:[8,6,5,11],subtractend:6},{minuend:14,response:9,responses:[8,7,9,1],subtractend:5},{minuend:13,response:4,responses:[4,3,2,5],subtractend:9},{minuend:13,response:5,responses:[4,6,9,5],subtractend:8},{minuend:13,response:6,responses:[4,6,7,5],subtractend:7},{minuend:13,response:7,responses:[1,6,7,5],subtractend:6},{minuend:13,response:8,responses:[13,6,7,8],subtractend:5},{minuend:13,response:9,responses:[12,9,7,8],subtractend:4},{minuend:12,response:3,responses:[3,1,4,5],subtractend:9},{minuend:12,response:4,responses:[3,2,4,8],subtractend:8},{minuend:12,response:5,responses:[2,6,5,8],subtractend:7},{minuend:12,response:6,responses:[3,6,5,7],subtractend:6},{minuend:12,response:7,responses:[10,6,8,7],subtractend:5},{minuend:12,response:8,responses:[12,9,8,7],subtractend:4},{minuend:12,response:9,responses:[2,9,8,7],subtractend:3},{minuend:11,response:2,responses:[4,2,1,0],subtractend:9},{minuend:11,response:3,responses:[3,2,4,7],subtractend:8},{minuend:11,response:4,responses:[3,5,4,9],subtractend:7},{minuend:11,response:5,responses:[3,5,4,9],subtractend:6},{minuend:11,response:6,responses:[6,5,7,11],subtractend:5},{minuend:11,response:7,responses:[6,9,7,4],subtractend:4},{minuend:11,response:8,responses:[8,9,7,11],subtractend:3},{minuend:11,response:9,responses:[2,9,7,11],subtractend:2}];

    var total = (players === 1) ? 10 : 20;

    var antMinuend = 0, antSubtractend = 0

    var equation = [];
    console.log(problems[0])
    problems = shuffleArray(problems);
    console.log(problems[0])
    var i = 0;
    while (true) {

      equation.push(problems[i])
      if (equation.length == total){
          break;
      }
      i++;
    }

    return equation;
}


function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

function parseEquation(equation) {
    var numbers = [];
    var operator = null;
    if (equation.indexOf("-") != -1) {
        numbers = equation.split("-");
        operator = "-";
    }
    else if (equation.indexOf("+") != -1) {
        numbers = equation.split("+");
        operator = "+";
    }
    return [numbers[0], operator, numbers[1]];
}

function fillEquation(equation) {
    console.log("fillEquation")
    equation = parseEquation(equation);
    $($(".containerEquation > .number")[0]).html(equation[0]);
    $($(".containerEquation > .number")[1]).html(equation[2]);
    $(".containerEquation > .operator").html(equation[1]);
}
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function setVisibleMain(visible) {
    if (visible) {
        $(".boxContainerEquation, .boxContainerBubbles, .boxContainerPlayer, .imagesBackgroundMain").css("display", "");
    } else {
        $(".boxContainerEquation, .boxContainerBubbles, .boxContainerPlayer, .imagesBackgroundMain").css("display", "none");
    }
}

function setVisibleScores(visible) {
    if (visible) {
        $(".imagesBackgroundScores, .boxContainerScore").css("display", "");
        //$(".boxContainerScoreTitle").html((players === 1) ? "Score" : "Scores");
    } else {
        $(".imagesBackgroundScores, .boxContainerScore").css("display", "none");
    }
}

function createScore() {
    $(".boxContainerScorePlayers").empty().css("display", "");
    var templatePlayer = "<div><div class='boxContainerScoreHeader1'>[playerName1]</div></div><div class='boxContainerScoreValue'><div class='imageScore'></div><h3>0</h3></div></div>";
    $(".boxContainerScorePlayers").empty();
    if (players == 1) {
        var tmp = "";
        tmp = templatePlayer.replace("[playerName1]", "<div class='delphin'></div>");
        $(".boxContainerScorePlayers").append(tmp);
    } else if (players == 2) {
        var tmp = "";
        tmp = templatePlayer.replace("[playerName1]", "<div class='octopus'></div>");
        tmp += "<div class='boxContainerScoreSpacer'></div>";
        tmp += templatePlayer.replace("[playerName1]", "<div class='otter'></div>");
        $(".boxContainerScorePlayers").append(tmp);
    }

    $(".playAgain").on("click", function () {
         resetClick();
    $(".selectPlayers").css("display", "block");
    });
}


function setFinalScore(index, value) {
    $($(".boxContainerScoreValue > h3")[index]).html(value);
}


function setScoreTime(value) {
    $(".boxContainerScoreTime > h3").html(value);
}


function resetClick(){
    reset();
    resetDataToSave();
}


function reset(){
    var resetTO=setTimeout(function(){
        clearInterval(resetTO)
    },100)
} 
