// avity.js
$(function () {
  var listElements = $('#numbers .numbers-element')

  var Monsters = {
    $monsters: $('.monsters'),
    $monster: $('.monsters').find('.monster'),
    getSlide: function () {
      return this.$monsters.data('slide')
    },
    changeSlide: function (number) {
      this.$monsters.data('slide', number)
    },
    setMonsterSlide: function (next, before) {
      Game.$nextMonster = $(Monsters.$monsters[next])
      Game.$beforeMonster = $(Monsters.$monsters[before])
      Game.$falledMonsters = Game.$nextMonster.find('.monster-laying')
    },
    throwMonster: function ($monster) {
      $monster.addClass('monster-shake')
      setTimeout(function () {
        $monster.addClass('monster-down')
      }, 200)
      setTimeout(function () {
        $monster.addClass('monster-laying')
        $monster.removeClass('monsters-hide monster-down')
      }, 200)
    }
  }

  Monsters.$monster.on('click touchstart', function () {
    var $this = $(this)
    if (!$this.hasClass('monster-laying')) {
      Monsters.throwMonster($this)
      Game.setResetButton(false)
    }
  })

  var Equations = {
    $equationsList: $('.static-equation'),
    $resultValue: $('#valueThree'),
    currentValue: function () {
      return this.$resultValue.find('.numbers-element.ui-draggable.ui-draggable-handle')[0]
    },
    setEquations: function (next, before) {
      this.$nextEquation = $(this.$equationsList[next])
      this.$beforeEquation = $(this.$equationsList[before])
      this.resultTmp = this.$nextEquation.find('.numbers__resultNumber').text()
    }
  }

  var Game = {
    $resetButton: $('#cleanButton'),
    $nextButton: $('.arrow-next'),
    $prevButton: $('.arrow-prev'),
    clearValues: function (value) {
      /**
      * This function will check if there is a value in the droppable area, the clear button will be activated.
      * @function
      * @name clearValues
      */
      var three = value || 0
      var value_three = +three
      if (value_three !== '') {
        this.setResetButton(false)
      } else {
        Game.setResetButton(true)
      }
    },
    showNextSlide: function () {
      Equations.$nextEquation.removeClass('static-equation-hide')
      Equations.$beforeEquation.addClass('static-equation-hide')
      this.$nextMonster.removeClass('monsters-hide')
      this.$beforeMonster.addClass('monsters-hide')
    },
    changeStateResteButton: function () {
      if (Equations.resultTmp !== '' || this.$falledMonsters.length > 0) {
        this.setResetButton(false)
      } else {
        this.setResetButton(true)
      }
    },
    setResetButton: function (active) {
      if (active) {
        this.$resetButton.addClass('icon-reset-inactive')
      } else {
        this.$resetButton.removeClass('icon-reset-inactive icon-reset-scroll')
      }
    }
  }

  Game.$resetButton.on('click', function (e) {
    var slide = Monsters.getSlide()
    if (!$(this).hasClass('icon-reset-inactive')) {
      $($('.monsters')[slide - 1]).find('.monster').removeClass('monster-shake monster-laying')
      $(this).addClass('icon-reset-inactive icon-reset-scroll')
      $(Equations.$equationsList[slide - 1]).find('.numbers__resultNumber').text('')
    }
  })

  /*
    Comienza juan.c.gamboa
  */
  Game.$nextButton.on('click', function () {
    var $slide = Monsters.getSlide()

    if ($slide < 6) {
      Equations.setEquations($slide, $slide - 1)
      Monsters.setMonsterSlide($slide, $slide - 1)
      Game.changeStateResteButton()
      Monsters.changeSlide($slide + 1)
      Game.$prevButton.removeClass('arrow-hide')
      Game.showNextSlide()

      if ($slide === 5) {
        Game.$nextButton.addClass('arrow-hide')
        Game.$prevButton.removeClass('arrow-hide')
      }

      $('.slide.slide-active').removeClass('slide-active').addClass('slide-visited').next().addClass('slide-active')
    }
  })

  Game.$prevButton.on('click', function (e) {
    var $slide = Monsters.getSlide()

    if ($slide > 1) {
      var nextSlide = $('.monsters').data('slide') - 1
      Equations.setEquations($slide - 2, $slide - 1)
      Monsters.setMonsterSlide(nextSlide - 1, nextSlide)
      Game.changeStateResteButton()
      Monsters.changeSlide(nextSlide)
      Monsters.$monster.removeClass('monster-shake')

      Game.showNextSlide()

      if (Monsters.getSlide() === 1) {
        Game.$prevButton.addClass('arrow-hide')
      }
      if (Monsters.getSlide() <= 6) {
        Game.$nextButton.removeClass('arrow-hide')
      }

      $('.slide.slide-active').removeClass('slide-active').addClass('slide-visited').prev().addClass('slide-active')
    }
  })
  /*
  Termina juan.c.gamboa
  */

  var Instructions = {
    instructions: document.getElementById('instructions'),
    $instructionsText: $('#instructionsText'),
    $closeButton: $('#closeButton'),
    $infoButton: $('#infoButton'),
    $audioButton: $('#audioButton'),
    init: function () {
      this.$audioButton.click(this.play.bind(this))
      this.$infoButton.click(this.show.bind(this))
      this.$closeButton.click(this.close.bind(this))
      this.play()
    },
    play: function () {
      //this.instructions.currentTime = 0
      this.instructions.load()
      this.instructions.play()
    },
    stop: function () {
      this.instructions.pause()
      this.instructions.currentTime = 0
    },
    close: function () {
      this.$instructionsText.toggleClass('layer-hide')
      this.stop()
      this.$infoButton.removeClass('icon-information-inactive')
    },
    show: function () {
      this.play()
      this.$instructionsText.toggleClass('layer-hide')
      this.$infoButton.addClass('icon-information-inactive')
    }
  }

/*
** NUMBERS
*/

/**
 * Drag and Drop function for the numbers.
 * The dropped value is also draggable and it validates if
 * there is a dropped value to delete from the results area.
 * @function
 * @name dragNum
 * @param
 */

  var $dragNum = $('#numbers .numbers-element')

  $dragNum.on('touchstart', function (e) {
    $($dragNum).draggable({
      helper: 'clone',
      revert: 'invalid'
    }, false)
  })

  $('#results .numbers-element').droppable({
    drop: function (event, ui) {
      var droppable = $(this)
      var innerDraggable = ui.draggable[0].innerText
      var draggable = $(ui.draggable).clone().addClass('numbers-element-dragged')
      $(droppable).innerText = innerDraggable
      $(droppable).html(draggable)

      Game.setResetButton(false)
      Game.clearValues(ui.draggable[0].innerText)
      $(draggable).draggable({
        helper: 'clone'
      })
      event.preventDefault()
    },
    out: function (event, ui) {
      var draggable = ui.draggable.clone()
      $(draggable).fadeOut(1000)
      if (draggable[0].isEqualNode(Equations.currentValue(draggable))) {
        $(Equations.currentValue()).remove()
        if ($('.monster-laying').length === 0) {
          Game.$resetButton.addClass('icon-reset-inactive')
        }
      }
      event.preventDefault()
    }
  })

/**
 * This function will trigger the touch event so that the list can be draggable
 * @function
 * @name initList
*/
  function init () {
    Instructions.init()
    try {
      listElements.trigger('touchstart')
    } catch (e) {}
  }
  init()
}())
