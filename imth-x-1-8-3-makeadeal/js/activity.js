/**
 * @author theo.spencer
 */

$(document).ready(function(){
	buildGame();
	$("#instructions").append("<p>"+activitySettings.instructions+"</p>").show();
	instructions();
	choiceTracker();
	$("#playAgain").click(function(){playAgain();});
	numKeypad = new vKeyPad(".numText", "#container", {isBlurOnWindowClick:false,hiddenKeys:["sub","sup","x","y","hyphen","multi"],colCount:5, isFrac:true, maximumLength:"maxWidth", autoClear:false, clearZero:false,additionalKeys:[{"id":"sup", "innerHTML":"%",  "value":"%", "isSpecialKey":false, "rowIndex":3, "colSpan":0}]});
	numKeypad.onTextChanged = function(){
		$('.btReset,.icoReset').css({'opacity':1});
		$(".icoReset").css({'opacity':'1','cursor':'default'});
		$(".icoReset").bind('click',resetenable);
	};
});

function overlay() {
	$(".overlay").show();
}

$("#info").click(function(){
	overlay();
	instructions();
});  

$("#reset").click(function(){
	overlay();
	resetBtn();
});  


$("button[value='Cancel']").click(function(){
	$(".overlay").hide();
	$("#resetInfo").hide();
});

$("button[value='Ok']").click(function(){
	$("#stage").empty();
	$(".overlay").hide();
	$("#resetInfo").hide();
	$(".ltYellow").html("0");
	$("#message span").html("Make your first selection. Choose a door to find the car.");
	buildGame();
	choiceTracker();
});

function closeBtn(){
	$(".close").click(function() {
		$(this).closest(".overlay").hide();
		$("#instructions").hide();
		$("#resetInfo").hide();
	});
}

var instructions = function(){
	overlay();
	$("#instructions").show();
	closeBtn();
};

var resetBtn = function(){
	overlay();
	$("#resetInfo").show();
	closeBtn();
};
	
myMax = activitySettings.doors;
var getRandom = function(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

var getRandomExclude = function(min2,max2,exclude) {
   myNum = getRandom(min2,max2);
   if (myNum !== exclude){
   		return myNum;
   } else {
   		return getRandomExclude(min2,max2,exclude);
   }
};

var firstChoice, secondChoice, correctChoice;

var buildGame = function(){
	$("#playAgain").hide();
	answerSet = getRandom(0,2);
	display = activitySettings.options[answerSet];
	for (i=0; i<myMax; i++)
	{
		j = i+1;
		if (display[i] == 'winner'){
			myClass = "winnerImg";
		} else {
			myClass = "loserImg";
		}
		$("#stage").append("<div class='panel'><span class='door'></span><span class='img "+myClass+"'></span><span class='shadowImg'></span><p>Door "+j+"</p></div>");
	}
	correctChoice = display.indexOf("winner")+1;
};

var choiceTracker = function () {
var choice = 0;
var animatedDoor;

	$("#stage .panel").click(function()
	{
  		myIndex = $(this).index(".panel");
  		
  		if (choice == 0)
  		{
			firstChoice = myIndex + 1;
			$("#message span").html("Stay or Switch? Make your final selection to find the car.");
			$(this).children(".shadowImg").addClass("doorGlow");
			$("#reset").css({"background-image":"url('../images/btResetOn.png')"});
			if (firstChoice == correctChoice) {
				styleDoorIndex = getRandomExclude(0,2,myIndex);
				$(".door").eq(styleDoorIndex).addClass("animateDoor");
				animatedDoor = styleDoorIndex;
			} else {
				arr = [0,1,2];
				myWin = display.indexOf("winner");
				arr = jQuery.grep(arr, function( n, i ) {
  				return ( n !== myWin && i !== myIndex );
				});
				$(".door").eq(arr).addClass("animateDoor");
				animatedDoor = arr.toString();				
			};
			choice++;
		} else if(choice == 1){
			if (myIndex == animatedDoor){
				//Do nothing
			} else {
			secondChoice = myIndex + 1;
			$(this).children(".door").addClass("animateDoor");
			$("#playAgain").show();
			scoreGame(firstChoice,secondChoice);
			choice++;
			}
		} else {
			//Do Nothing
		}
		
	});

};

function playAgain(){
	$("#stage").empty();
	buildGame();
	choiceTracker();
}

var sameChoice = 0, switchedChoice = 0, switchedWon = 0, switchedLost = 0, stayedWon = 0, stayedLost = 0;	

function scoreGame(x,y){

	if (x == y && y == correctChoice) {
		sameChoice++;
		stayedWon++;
		$("#stayed").empty().append(sameChoice);
		$("#stayedWon").empty().append(stayedWon);
		$("#message span").html("You Won!");
	} else if ( x !== y && y == correctChoice){
		switchedChoice++;
		switchedWon++;
		$("#switched").empty().append(switchedChoice);
		$("#switchedWon").empty().append(switchedWon);
		$("#message span").html("You Won!");
	} else if( x == y && y !== correctChoice){
		sameChoice++;
		stayedLost++;
		$("#stayed").empty().append(sameChoice);
		$("#stayedLost").empty().append(stayedLost);
		$("#message span").html("That's the second goat. You lost!");
	} else {
		switchedChoice++;
		switchedLost++;
		$("#switched").empty().append(switchedChoice);
		$("#switchedLost").empty().append(switchedLost);
		$("#message span").html("That's the second goat. You lost!");
	}
	
}
