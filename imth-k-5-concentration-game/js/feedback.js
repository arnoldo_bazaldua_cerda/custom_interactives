//a way to generate feedback automatically, event the audios
var preset=options.preset;
var _image="images/base/instructions-sound.png";
var templates={
	'imth-1-1-concentration-game':{
		_content:"<p>Tap <b>1 Player</b> if you are playing by yourself. Tap <b>2 Players</b> if you are playing with a partner.</p><p>The yellow and red cards show numbers.</p><p>Find a yellow card and a red card with numbers that make ten.</p><p>In a 2-player game, if a match is made, the player who made the match keeps playing. If the cards don’t match, it becomes the other player’s turn.</p><p>The player who makes the most matches wins the game.</p>",
	},
	'imth-k-6-concentration-game':{
		_content:"Tap <b>1 Player</b> if you are playing by yourself.<br>Tap <b>2 Players</b> if you are playing with a partner.<br>The yellow cards show subtractions. The red cards show results.<br>You must match a subtraction card with a result card.<br>In a 2-player game, if a match is made, the player who made the match keeps playing. If the cards don’t match, it becomes the other player’s turn.<br>The player who makes the most matches wins the game.",
	},
    'imth-1-11-concentration-add':{
        _content:'<p>Tap <b>1 Player</b> if you are playing by yourself. Tap <b>2 Players</b> if you are playing with a partner.</p><p>The yellow cards show additions. The red cards show sums.</p><p>You must match an addition card with a sum card.</p><p>In a 2-player game, if a match is made, the player who made the match keeps playing. If the cards do not match, it becomes the other player’s turn.</p><p>The player who makes the most matches wins the game.</p>'
    },
    'imth-1-11-concentration-sub':{
        _content:'<p>Tap <b>1 Player</b> if you are playing by yourself. Tap <b>2 Players</b> if you are playing with a partner.</p><p>The yellow cards show subtractions. The red cards show differences.</p><p>You must match a subtraction card with a difference card.</p><p>In a 2-player game, if a match is made, the player who made the match keeps playing. If the cards do not match, it becomes the other player’s turn.</p><p>The player who makes the most matches wins the game.</p>'
    },
	'imth-1-3-concentration-game':{
		_content:"<p></p><p>Tap <b>1 Player</b> if you are playing by yourself. Tap <b>2 Players</b> if you are playing with a partner.</p><p>The yellow cards show additions. The red cards show sums. You must match an addition card with a sum card.</p>In a 2-player game, if a match is made, the player who made the match keeps playing. If the cards don’t match, it becomes the other player’s turn. The player who makes the most matches wins the game.",
	},
	'imth-k-2-concentration-game-ICON-1':{
		_content:"<p>Tap <b>1 Player</b> if you are playing by yourself. Tap <b>2 Players</b> if you are playing with a partner.</p><p>The yellow cards are number cards. The red cards are dot cards. Match each yellow number card  to a red dot card.</p><p>In a 2-player game, take turns. The player who makes the most matches wins the game.</p>",
	},
	'imth-k-2-concentration-game-ICON-2':{
		_content:"<p>Tap <b>1 Player</b> if you are playing by yourself. Tap <b>2 Players</b> if you are playing with a partner.</p><p>The yellow cards are number cards. The red cards are dot cards. Match each yellow number card to a red dot card.</p><p>In a 2-player game, if a match is made, the player who made the match keeps playing. If the cards don’t match, it becomes the other player’s turn. The player who makes the most matches wins the game.</p>"

	},
	'1-4-concentration-game':{
		_content:"<p>Tap <span style='font-weight:bold;'>1 Player</span> if you are playing by yourself. Tap <span style='font-weight:bold;'>2 Players</span> if you are playing with a partner.</p><p> The yellow cards show subtractions. The red cards show differences.You must match a subtraction card with a difference card.</p><p>In a 2-player game, if a match is made, the player who made the match keeps playing. If the cards don’t match, it becomes the other player’s turn.  The player who makes the most matches wins the game.</p>"
	},
	'imth-k-5-concentration-game':{
		_content:"<p>Tap <b>1 Player</b> if you are playing by yourself. Tap <b>2 Players</b> if you are playing with a partner.</p><p>The yellow cards show additions. The red cards show sums.</p><p>You must match an addition card with a sum card.</p><p>In a 2-player game, if a match is made, the player who made the match keeps playing. If the cards do not match, it becomes the other player’s turn.</p><p>The player who makes the most matches wins the game.</p>",
	},
}
if(!preset){
	// console.log(options.name);
	iModalObj=templates[options.name];

	iModalObj.mp3='../audio/'+options.name+".mp3",
	iModalObj._image=_image
}
else{
	var infoAndSounds={
		'5':{
			audio:'imth-k-3-concentration-game-ICON-1',
			word:'five'
		},
		'6':{
			audio:'imth-k-3-concentration-game-ICON-2',
			word:'five'
		},
		'7':{
			audio:'imth-k-3-concentration-game-ICON-3',
			word:'five'
		},
	}

	iModalObj = {
		_content:"<p>Tap <b>1 Player</b> if you are playing by yourself. Tap <b>2 Players</b> if you are playing with a partner.</p><p>The yellow and red cards are dot cards. You must find a yellow dot card and a red dot card that make "+preset+".</p><p>In a 2-player game, if a match is made, the player who made the match keeps playing. If the cards don’t match, it becomes the other player’s turn.</p>The player who makes the most matches wins the game.",
		_image: "images/base/instructions-sound.png",
		mp3:"../audio/"+infoAndSounds[String(preset)].audio+".mp3"
	}


}

var audiosrc = iModalObj.mp3;
var ioAudio = new Audio(audiosrc);


$(document).ready(function(){
	iModalObj = {
	    _content: iModalObj._content,
	    _image: ""
	}

	_iModalTemplate ="<div class='_back _backClose'></div><div class='_iModalBack'><div class='Center_iModal'><div class=' AbsoluteCenter_iModal'><div class='_iModalHeader'><div id='btn-io-info' class='icoInfo'></div><div class='icoClose'>×</div></div><div class='_iModalBody'></div><div id='btn-io-speaker'></div></div></div></div>";

	_iModalBody ="<p>"+iModalObj._content+"</p>";
	feedBackButtons()
	iModal();
});


function iModal(){
    $('body').append(_iModalTemplate);
    $_back = $("._back");

    toggleClassButton($_back,'_backClose')
    $iModal = $('._iModalBack');
    $iModal.find('._iModalBody').append(_iModalBody)
    $iModal.css({"display":"block"});

	var width = screen.width<1024?1024:screen.width;
	$('.mainContainer').css({'margin-left': parseInt((width - 1024)/2)+'px'})
	$('._iModalBack').css({'margin-left': parseInt((width - 835)/2)+'px'})

    try{ioAudio.play();}catch(e){}

    $('.icoClose').on('mousedown touchstart',function(e){
        e.preventDefault();
        $iModal.remove();
        $("._back").remove();
        $iModal.css({"display":"block"});
        try{ioAudio.src = '';}catch(e){}
        try{delete ioAudio;}catch(e){}
        try{ioAudio = new Audio(audiosrc);}catch(e){}
    });



    $('#btn-io-info, #btn-io-speaker').on('mousedown touchstart',function(e){
        e.preventDefault();
        try{ioAudio.src = '';}catch(e){}
        try{delete ioAudio;}catch(e){}
        try{ioAudio = new Audio(audiosrc);}catch(e){}
        try{ioAudio.play();}catch(e){}
    });
}

function feedBackButtons(){

	$(".icoReset").on('click',function (event) {
		event.preventDefault();
		event.stopPropagation();
		var current=$(this);
		current.removeClass('icoResetMove');
		var to=setTimeout(function(){
			clearInterval(to);
			current.addClass('icoResetMove');
		},50)

	});

}

function toggleClassButton(element,className){
    var currentButton=element;
    if(!currentButton.hasClass(className)){
        currentButton.addClass(className);
    }else{
        currentButton.removeClass(className);
    }
    return element;
}
