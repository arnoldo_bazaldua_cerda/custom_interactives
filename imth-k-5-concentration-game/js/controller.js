var addGameControllers={};
var startInteract = true;
addGameControllers.mainController=function($scope,engine,$rootScope,$http,$timeout){

	// $scope.__debugging=true;
	//resetState($scope)
	$scope.gameDone=false;
	$scope.cardMatchCorrrect=false;
	$scope.bodyShow={'display':'block'};
	$scope.hidebit = "";
	$scope.paux=false;
	//$scope.engine=engine.options.engine;
	$scope.engineOptions=engine.options;
	$scope.verifyReset = true;
	$scope.resetState=function(){
		$scope.$apply(function($scope){
			$scope.start=false;
			$scope.players=[];
			$scope.cards=null;
			$scope.turn=0;
			startInteract = true;
			$scope.tries=0;
			$scope.gameDone=false;
			$scope.paux=false;
			$scope.resultPoll = [];
			$scope.current = undefined;
			engine.busy=0;
			$scope.verifyReset = false;

			$scope.menu=null;
			$scope.globalSave={};
			if(engine.options.menu && !engine.options.preset){
				$scope.menuItem=null;
				$scope.start=true;
				$scope.menu=engine.getMenu();
			}

		})
		//hacky awful thing
		try{
			$rootScope.$broadcast('reset')
			if(actualModal){

				actualModal=0;
			}
		}catch(e){
			console.log(e);
		}
	}
	document.addEventListener("myEvent",function(){
		if(options.save){
			if(fetchedState==null){return false;}

			if(fetchedState.players.length==0){
				return false;
			}
			$scope.fetchedState=fetchedState;
			$scope.players=$scope.fetchedState.players;
			$scope.cards=$scope.fetchedState.cards;
			$scope.turn=$scope.fetchedState.turn;
			$scope.gameDone=$scope.fetchedState.gameDone;
			engine.options.maxCards=$scope.cards.length/2;
			$scope.menu=null;

			if(!engine.options.preset){
				if($scope.fetchedState.menuItem){
					$scope.menuItem=$scope.fetchedState.menuItem;
					engine.options.check=$scope.menuItem.value;
					actualModal=1;
				}
			}
			$scope.startGame();
		}

	},false);

	$http.get('../images/background.jpg').success(function(){
		$rootScope.$broadcast('completeBackground');
	})
	$scope.getPoints=function(number){
		return new Array(number);
	}

	$scope.start=false;
	$scope.gridWidth=300;
	$scope.resultPoll=[];
	$scope.$on('countClick',function(data,value){
		console.log("countClick")
		// setTimeout(function(){
		// 	console.log("remove")
		// 	$(event.target.children[1]).css("display","block")
		// },5	º)

		$scope.countClicks(value);
	})
	//create players:
	//if there is menu!
	if(engine.options.menu){
		$scope.start=true;
		$scope.menu=engine.getMenu();
	}

	$scope.players=[];
	$scope.startGameWith=function(nPlayers){
		console.log("startGame")
		// if($scope.verifyReset){}

		//if(!$scope.fetchedState){
		$scope.tries=0;
		//console.log('startGameWith')
		for (var i=0; i<nPlayers;i++ ){
			var p=new engine.player();
			p.data.nombre="Player "+(i+1);
			p.data.id=i;
			$scope.players.push(p);
		}
		//console.log($scope.menuItem)
		$scope.cards=engine.startEngine($scope.menuItem);
		//	}
		$scope.startGame();
	}
	$scope.$on('START_GAME',function(evt,value){
		$scope.$apply(function(){
			value=JSON.parse(value);
			$scope.gridWidth=value.gridWidth+'px';
			$scope.menuItem=value;
			$scope.menu=false;
			$scope.start=false;
		})
		if($scope.fetchedState){
			engine.options.check=engine.options.preset;
			$scope.startGame();
		}
		//hacky little thing :/ i feel bad about this, but, sometimes, you have to!
		try{
			actualModal
			actualModal=1;
			iModal();
		}catch(e){
			console.log(e)
		}
	})

	$scope.startGame=function(){
		$scope.start=true;
		$scope.verifyReset = true;
		$scope.turn=$scope.turn==undefined?0:$scope.turn;
	}
	$scope.disBody=function(boo){

		//$scope.$apply(function(){
		//	console.log("desabilita")
		//	$scope.disableApp=boo?{'pointer-events':'none'}:''
		//});

	}

	saveCallBack=function(){
		if(options.save){
			$scope.globalSave={};
			var saveobj={};

			if($scope.cards){
				saveobj=_.clone($scope.cards);
				saveobj.filter(function(ele){
					//delete ele['$$hashKey'];
					return ele;
				})

			}

			$scope.globalSave.cards=saveobj;
			$scope.globalSave.players=$scope.players;
			$scope.globalSave.turn=$scope.turn;
			$scope.globalSave.gameDone=$scope.gameDone;
			if(!engine.options.preset){
				$scope.globalSave.menuItem=$scope.menuItem;
			}
			var message = {
				type : 'state',
				data : JSON.stringify($scope.globalSave)
			};
			//return whatever you need.
			return message;
		}else{
			var message = {
				type : 'state',
				data : JSON.stringify('')
			};
			return message;
		}

	}

	$scope.countClicks=function(value){

		$scope.resultPoll.push(value);
		if($scope.resultPoll.length>=2){

			$scope.disBody(true);
			$timeout(function(){

				if( $scope.verifyReset ){
					$scope.disBody();
					var operation=0;
					var result=0;
					var resultN;
					var nResult;
					for (var i in $scope.resultPoll){

						var card=$scope.resultPoll[i];
						// debugger
						if(card.type=='operation'){
							operation++;
							//*make this tring a numeric value
							//this interactive need to be checked in a diferent way
							//var nResult;
							switch($scope.engineOptions.engine){
								default:
								nResult=card.value;
								break;
								case 'sameValue':
								nResult=eval(card.value);
								break;
							}

						}else{
							resultN=card.value;
							result++;
						}
					}
					var alreadyChange=false;
					var r=false;


					if(engine.options.check=='sameValue'){

						//check if typeof is number to avoid innecesary replace
						nResult= typeof nResult=='string'?nResult.replace("—","-"):nResult;
						resultN= typeof resultN=='string'?resultN.replace("—","-"):resultN;
						//continue as it was
						res=!(eval(nResult)==eval(resultN));




					}else{
						res=(nResult+resultN)!=engine.options.check;

					}

					$timeout.cancel($scope.correctTimeout);
					if(res){
						$scope.cardMatchCorrrect=false;
						$rootScope.$broadcast('reset')
						$scope.$apply(function(){
							$scope.players[$scope.turn].data.score.wrong+=1;
							$scope.players[$scope.turn].data.score.tries+=1;
						})

					}else{
						//correct
						$scope.cardHolderblur=true;
						$scope.cardMatchCorrrect=true;
						$scope.disBody(true)
						$scope.cardMatched=_.clone($scope.resultPoll);
						$scope.correctTimeout=$timeout(function(){
							$scope.disBody();
							$scope.$apply(function(){
								$scope.cardMatchCorrrect=$scope.cardHolderblur=false;
							})
						},3000);
						$rootScope.$broadcast('correct',$scope.turn)
						$rootScope.$broadcast('playCorrect');
						$scope.$apply(function(){
							$scope.players[$scope.turn].data.score.correct+=1
							$scope.players[$scope.turn].data.score.tries+=1;
						})
						var res={};
						$scope.resultPoll.filter(function(el){
							res[el.type]=el.value;
						})
						$scope.$apply(function(){
							$scope.players[$scope.turn].data.list.push(res);
						})
						for (var i in $scope.resultPoll){
							$scope.resultPoll[i].visible=false;
						}
						r=true;
					}
					$scope.resultPoll=[];
					//if switch to player's turn must be in any number of tries
					//$scope.tries++;
					//$scope.tries==4
					//console.log($scope.engineOptions.switchPlayersOnTry);

					if(!r || $scope.engineOptions.switchPlayersOnTry){
						//$scope.tries=0;
						$scope.getNextTurn();
					}
					var total=0;
					for(var i in $scope.players){
						//total+=$scope.players[i].list.length;
						total+=$scope.players[i].data.list.length;
					}
					for (var i in $scope.players){
						var obj=$scope.players[i].data;
						total+=obj.score.correct;
					}
					total=total/2;
					if(total==engine.returnOp()){
						//$scope.whoWin();
						$scope.$apply(function(){
							$scope.correctTimeout=$timeout(function(){
								$scope.gameDone=true;
								$scope.players.length==1?$rootScope.$broadcast('playMatches'):null;

							},3500)
						})
					}
				}
			},1500)


		}
		$scope.whoWin=function(){

			$scope.gameDone=true;
			/*var greater=0;
			for(var i in $scope.players){
			var obj=$scope.players[i].data;
			//greater>=obj.score.correct?greater:obj.score.correct;
			if(greater<obj.score.correct){
			greater=obj.score.correct
			$scope.winner=obj;
		}
	}
	$scope.$apply(function(){
	$scope.turn=$scope.winner.id;
})*/
}

}
$scope.getNextTurn=function(){
	$scope.$apply(function(){
		$scope.turn>=$scope.players.length-1? $scope.turn =0:$scope.turn+=1;
	})



}
}

concentrationGameAdd.controller(addGameControllers,['$scope','engine'])
