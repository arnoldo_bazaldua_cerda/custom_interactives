//a way to generate feedback automatically, event the audios
console.log('ATTENTION! THIS FEEDBACK MUST ONLY BE IN mth-k-3-concentration-game-ICON-4!')
iModalObj = {
_content:['<p>Choose your number.</p>',"<p>Tap <b>1 Player</b> if you are playing by yourself. Tap <b>2 Players</b> if you are playing with a partner.</p><p>The yellow and red cards are dot cards. Find a yellow dot card and a red dot card that make your number.</p><p>In a 2-player game, if a match is made, the player who made the match keeps playing. If the cards don’t match, it becomes the other player’s turn.</p><p>The player who makes the most matches wins the game.</p>"],
	 _image: "images/base/instructions-sound.png",
	 mp3:["audio/select.mp3","audio/imth-k-3-concentration-game-ICON-4.mp3"]
}






var _iModalTemplate ="<div class='_back _backClose'></div><div class='_iModalBack'><div class='Center_iModal'><div class=' AbsoluteCenter_iModal' style='width: 418px;height: 158px;'><div class='_iModalHeader'><div class='icoInfo'></div><div class='icoClose'>x</div></div><div class='_iModalBody'></div></div></div></div>";

var _iModalBody
var actualModal=0;


var $iModal;
var audiosrc = iModalObj.mp3[actualModal];
var ioAudio = new Audio(audiosrc);
//console.log(iModalObj.mp3[actualModal])

$(document).ready(function(){
	feedBackButtons();
	iModal();
});

function iModal(){

	$('body').append(_iModalTemplate);
	if(actualModal === 1){
		audiosrc = iModalObj.mp3[actualModal];
		try{ioAudio = new Audio(audiosrc);}catch(e){}
		$(".AbsoluteCenter_iModal").removeAttr("style");
	}
	$_back = $("._back");
	try{ioAudio.play();}catch(e){}

	var n=setTimeout(function(){
		toggleClassButton($_back,'_backClose')
		$iModal = $('._iModalBack');

		if(iModalObj._image.length > 0){
			_iModalBody ="<p>"+iModalObj._content[actualModal]+"</p><img src='"+iModalObj._image+"'/>";
		}else{
			_iModalBody ="<p>"+iModalObj._content[actualModal]+"</p>";
		}
		$iModal.find('._iModalBody').append(_iModalBody)
		$iModal.css({"display":"block"});

		$('.icoClose').on('click',function(){
			try{ioAudio.src = '';}catch(e){}
			try{delete ioAudio;}catch(e){}
			try{ioAudio = new Audio(audiosrc);}catch(e){}
			$('body').removeClass('hack');
			$iModal.remove();
			$iModal.css({"display":"block"})
			$_back.remove()

		});
		// close modal clicking anywhere

		$('._iModalBody img').on('click',function(){
			try{ioAudio.src = '';}catch(e){}
			try{delete ioAudio;}catch(e){}
			try{ioAudio = new Audio(audiosrc);}catch(e){}
			try{ioAudio.play();}catch(e){}
		});

	},200);
}

function feedBackButtons(){

	$(".icoInfo").on('click',function (event) {
		event.preventDefault();
		event.stopPropagation();
		iModal();
	})

	$(".icoReset").on('click',function (event) {
		event.preventDefault();
		event.stopPropagation();
		var current=$(this);
		actualModal = 0;
		audiosrc = iModalObj.mp3[actualModal];
		try{ioAudio = new Audio(audiosrc);}catch(e){}
		current.removeClass('icoResetMove');
		var to=setTimeout(function(){
			clearInterval(to);
			current.addClass('icoResetMove');
		},50)
	});

}

function resetIcoFeed(){
	$('.icoFeed').removeClass('icoFeedMove').off('click');
	$('.score').hide();
}

function iconFeedFunction(){
	$(".icoFeed").off('click');
	$(".icoFeed").on('click',function(event){
		event.preventDefault();
		event.stopPropagation();
		checkAnswers();
		$('.score').toggle("showAnim");
	})
}

function completeDrag(){
	console.log('completeDrag');
	if ($('.drag.dropped').length == $('.drop').length) {
		iconFeedFunction();
		$(".icoFeed").addClass('icoFeedMove');
	};
}

function checkAnswers(){
	var correct = 0,
	incorrect = $('.drop').length;

	$('.drop').each(function(index,value){
		var dropid = $(value).attr('id'),
		dragid = $('.'+dropid).attr('id');

		if (dropid.replace(/[^\d.]/g,'') == dragid.replace(/[^\d.]/g,'')) {
			correct += 1;
		};

	});

	$('#contCorrect').text(correct).append('<img src="images/base/IconCorrect.png">');
	$('#contIncorrect').text(incorrect-correct).append('<img src="images/base/IconIncorrect.png">');
}

function toggleClassButton(element,className){
	var currentButton=element;
	if(!currentButton.hasClass(className)){
		currentButton.addClass(className);
	}else{
		currentButton.removeClass(className);
	}
	return element;
}
