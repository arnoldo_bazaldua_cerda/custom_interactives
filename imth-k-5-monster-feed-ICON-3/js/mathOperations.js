 /* ****************************mathOperation.js*****************************
  ****************************************************************************/
/**
** You can set the following properties:
** - operationType -> type of operation (addition & subtraction)
**                 -> If you want only addition operations or subtraction operations
**                 -> you have to modify the randomOp property and set it to false
**   Can be set as: 1 (addition) or 2 (substraction)
**   Default Value: 1
** - upperLimit ->
**   Can be set as: whatever whole number
**   Default Value: 10
** - lowerLimit ->
**   Can be set as: whatever whole number
**   Default Value: 0
** - operations -> totals number of operations
**   Can be set as: whatever natural numbers
**   Default Value: 1
** - randomOp -> allows you to get addition or subtraction equation
**   Can be set as: true (generates addition or subtraction equation) or false (generate only equations as operationType was defined)
**   Default Value: true
** - showAnswer -> boolean value to show the answer instead of the operation
**   Can be set as: true (shows the answer) or false (shows the operation)
**   Default Value: false
**/

var MathOperations = {}

MathOperations.builder = {
  plus: 1,
  minus: 2,
  answer: 0,
  operation: '',
  equation: '',
  allOperations: [],
  auxSettings: null,
  generateMathOperations: function (options) {
    /* Setting options for the function */
    var settings = this.setValues(options)
    this.auxSettings = settings
    // verify the upperLimit and lowerLimit
    if (settings.upperLimit > settings.lowerLimit) {
      this.op = this.generateOperation(settings)
      this.allOperations = this.op
      var opSize = this.op.length
      var stringOperation = ''
      for (var i = 0; i < opSize; i++) {
        stringOperation = this.op[i][0] + this.op[i][1] + this.op[i][2]
        this.equation = stringOperation
        this.answer = eval(stringOperation)
      }
      if (settings.showAnswer) {
        // if showAnswer is 'true'
        // set the stringOperation var to answer value
        stringOperation = this.answer
      }
      this.operation = stringOperation + ' = ' + this.answer
      // setting the operation in html
      $('div').filter('#sign').find('id').find('h2').html(stringOperation)
    }/* else {
      console.log('Please, set the correct values to upperLimit and lowerLimit.')
    }*/
  },
  generateOperation: function (settings) {
    var upperLimit = settings.upperLimit           // set the upper limit
    var lowerLimit = settings.lowerLimit           // set the lower limit
    var operationType = settings.operationType     // set the operatio type
    var operationChar = ''
    var randomOp = settings.randomOp                   // set if random numbers is requested
    var operations = settings.operations
    var randomNO = settings.randomNO
    var operationArray = []
    var num1 = null    // 1st number will be generated by random numbers between 1 && 10
    var num2 = null    // 2nd will be generated based on num1
    var operationArrayAux = [] // array is used as an auxiliar to save the operation generated
    if (randomNO) {
      operations = this.generateRandomNumberOfOperations()
    }
    for (var i = 0; i < operations; i++) {
      operationArrayAux = []
      num1 = Math.floor((Math.random() * upperLimit) + lowerLimit)
      if (randomOp) {
        operationType = this.getRandomOperation()
      }
      if (operationType === this.plus) {
        operationChar = ' + '
        if (num1 === upperLimit) {
          num2 = 0
        } else {
          if (num1 === 0) {
            num2 = Math.floor((Math.random() * (upperLimit - num1)) + 1)
          } else {
            num2 = Math.floor((Math.random() * (upperLimit - num1)) + lowerLimit)
          }
        }
      } else if (operationType === this.minus) {
        operationChar = ' - '
        num2 = Math.floor((Math.random() * num1) + lowerLimit)
      }
      operationArrayAux.push(num1)
      operationArrayAux.push(operationChar)
      operationArrayAux.push(num2)
      operationArray.push(operationArrayAux)
    }
    return operationArray
  },
  setValues: function (options) {
    return $.extend({
      operationType: 1,
      upperLimit: 10,
      lowerLimit: 0,
      operations: 1,
      randomOp: true,
      showAnswer: false,
      randomNO: false
    }, options)
  },
  getAnswer: function () {
    return this.answer
  },
  getOperation: function () {
    return this.operation
  },
  getOperations: function () {
    return this.allOperations
  },
  getRandomOperation: function () {
    var operationRandom = Math.floor(Math.random() * 10) + 1
    if ((operationRandom % 2) === 0) {
      return 1
    } else {
      return 2
    }
  },
  generateRandomNumberOfOperations: function () {
    return Math.floor(Math.random() * (11 - 4)) + 4
  }
}

MathOperations.builder.generateMathOperations({
  showAnswer: true,
  randomNO: true,
  randomOp: false,
  upperLimit: 5
})
