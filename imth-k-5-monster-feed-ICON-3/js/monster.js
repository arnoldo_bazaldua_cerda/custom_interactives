/* Monster.js ( main ) */
$(document).on('ready',
  buildGame)

var myApp = myApp || {}
var classNames = ['banana', 'carrots', 'cheese', 'cherries', 'grapes', 'kiwis', 'meats', 'milks', 'oranges', 'strawberries', 'tires', 'tomatoes', 'watermelons']
myApp.values = {
  pearsonAppAudioFlag: false
}

myApp.audioX = {  /** firset set the auido ID same as in the html an then use play,pause, stop*/
  $audio: $('#audio')[0], // default
  setAudio: function (audioName) {
    this.$audio = $(audioName)[0]
  },
  playA: function () {
    this.$audio.load()
    this.$audio.play()
  },
  stopA: function () {
    this.$audio.pause()
    this.$audio.currentTime = 0
  },
  pauseA: function () {
    this.$audio.load()
    this.$audio.pause()
  },
  getTimeA: function () {
    return this.$audio.currentTime
  }
}

myApp.modal = {
  show: function () {
    $('.box').css('animation-play-state', 'paused')
    myApp.audioX.setAudio('#audioBack')
    myApp.audioX.pauseA()
    $('#_black').css('display', 'block')
    myApp.audioX.setAudio('#audio')
    myApp.audioX.playA()
    $('._iModalBody img').click(function () {
      $('#audio').trigger('pause')
      myApp.audioX.playA()
    })
  },
  hide: function () {
    $('.box').css('animation-play-state', 'running')
    $('#_black').css('display', 'none')
    myApp.audioX.setAudio('#audioBack')
    if ((myApp.audioX.getTimeA() === 0) && (myApp.values.pearsonAppAudioFlag)) { /** time of audio is <> 0 in pearson app so added a flag */
      myApp.audioX.playA()
    }
    myApp.audioX.setAudio('#audio')
    myApp.audioX.stopA()
  },
  resultOnCloud: true
}

function buildGame () {
  myApp.modal.show()
    /* close PopUp */
  $('#icoClose').click(function () {
    myApp.modal.hide()
    $('#cloudUno').addClass('cloudMoveLeft')
  })
  $('#startGame').click(function () {
    myApp.values.pearsonAppAudioFlag = true
    $(this).addClass('hide')
    startGame(myApp.modal.resultOnCloud)
    myApp.audioX.setAudio('#audioBack')
    myApp.audioX.playA()
  })
  $('.contentFeedback .btInfo').click(function () {
    myApp.modal.show()
  })
  $('.contentFeedback .icoReset').click(function () {
    myScore.service.restartTurn = 0
    myScore.service.removeMonsterAndFruits()
    myScore.service.playAgainEvent()
    myScore.service.restart(false)
    $('div').filter('#congratFeedback').addClass('congratFeedback-hide')
    $('.icoReset').addClass('icoResetMove')
    setTimeout(function () {
      $('.icoReset').removeClass('icoResetMove')
    }, 400)
  })
  myDragDrop.service.drag('.ui-draggable')
  myDragDrop.service.drop('.monstercito')
}

function startGame (resultOnCloud) {
  var container = $('.box')
  var fruitsN = []
  var fruitN = []
  var numberFruit = null
  var fruitNameN = null
  var fruitName = null
  var operations = []
  var equation = null
  if (resultOnCloud) {
    operations = MathOperations.builder.getOperations()
  }
  var box = $.makeArray(container)
  var opLength = operations.length
  var auxBox = []
  for (var j = 0; j < opLength; j++) {
    var index = Math.floor((Math.random() * 11) + 1)
    auxBox[j] = generateRandomIndex(index, auxBox)
  }
  for (var i = 0; i < opLength; i++) {
    equation = operations[i][0] + operations[i][1] + operations[i][2]
    numberFruit = eval(equation) // eslint-disable-line
    fruitNameN = myRandom.service.getRandomClass(0, 11, fruitN)
    fruitName = classNames[fruitNameN]
    $(box[i]).removeClass('hide')

    $(box[i]).find('.fondo')
      .addClass(fruitName)
      .addClass(fruitName + '-' + numberFruit)
      .addClass('fondoShake')
    $(box[i]).find('.fruitHolder').addClass('fruitHolder-' + fruitName).addClass(fruitName + '-' + numberFruit)
    $(box[i]).find('.bubble').html(!resultOnCloud ? numberFruit : equation)
    $(box[i]).addClass('move' + auxBox[i])
    if (i >= 9) {
      fruitsN.length = 0
    }
    fruitsN.push(numberFruit)
    fruitN.push(fruitNameN)
  }
}
/* generates a random number to set the position of the box */
function generateRandomIndex (index, auxBox) {
  if (auxBox.length > 0) {
    for (var x in auxBox) {
      if (auxBox[x] === index) {
        index = Math.floor((Math.random() * 11) + 1)
        return generateRandomIndex(index, auxBox)
      }
    }
  }
  return index
}
