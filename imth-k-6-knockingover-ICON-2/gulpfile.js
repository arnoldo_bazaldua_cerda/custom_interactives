'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var server = require('gulp-server-livereload');
var standard = require('gulp-standard');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');

gulp.task('sass', function () {
  gulp.src('./sass/styles.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css')) ;
}); 

gulp.task('webserver', function() {
  gulp.src('.')
    .pipe(server({
      livereload: true,
      directoryListing: true,
      open: true
    }));
});

gulp.task('standard', function () {
  return gulp.src(['./js/activity.js'])
    .pipe(standard({
       "globals": [ "$"]
    }))
    .pipe(standard.reporter('default', {
      breakOnError: true
    }))
});

gulp.task('js:uglify',function(){
  return gulp.src(['./js/jquery-2.1.4.min.js','./js/jquery-ui.min.js','./js/jquery.ui.touch-punch.min.js','js/activity.js'])
  .pipe(concat('concat.js'))
  .pipe(rename('uglify.js'))
  .pipe(uglify())
  .pipe(gulp.dest('js'))
}); 

gulp.task('watch', function () {
  gulp.watch('./sass/**/*.scss', ['sass']);
  gulp.watch('./js/activity.js', ['standard']);
});

gulp.task('default', ['sass', 'watch'], function(){})

gulp.task('build', ['js:uglify','sass'])