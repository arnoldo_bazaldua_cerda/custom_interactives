// activity.js
$(function () {
  var $numbers = $('#numbers')
  var listElements = $('#numbers').find('.numbers-element')
  var audioBowl = document.getElementById('bowls')
  var ding = document.getElementById('ding')
  var objOp = { min: '', sus: '', res: '' }
  var limite
  var num, num2, left2
  var fallen
  var x2Op
  var maxMonster2Op
  var numeroRandom2Op
  var maxMonster2

  var Instructions = {
    audio: document.getElementById('instructions'),
    $instructionsText: $('#instructionsText'),
    $closeButton: $('#closeButton'),
    $infoButton: $('#infoButton'),
    $audioButton: $('#audioButton'),
    init: function () {
      this.$audioButton.click(this.play.bind(this))
      this.$infoButton.click(this.show.bind(this))
      this.$closeButton.click(this.close.bind(this))
      this.play()
    },
    play: function () {
      this.audio.currentTime = 0
      this.audio.play()
    },
    stop: function () {
      this.audio.pause()
      this.audio.currentTime = 0
    },
    close: function () {
      this.$instructionsText.toggleClass('layer-hide')
      this.stop()
      this.$infoButton.removeClass('icon-information-inactive')
    },
    show: function () {
      this.audio.load()
      this.audio.play()
      this.$instructionsText.toggleClass('layer-hide')
      this.$infoButton.addClass('icon-information-inactive')
    }
  }

  var Monsters = {
    $monsters: $('.monsters'),
    $listMonster: $('.monster'),
    getLyingMonsters: function () {
      return this.visibles().filter('div[class^="monster monster-laying-"]')
    },
    getStandingMonsters: function () {
      return this.visibles().not('div[class^="monster monster-laying-"]')
    },
    hitMonsters: function (maxMonster, monstrillos2, numeroRandom, callback) {
      var visibles = Monsters.visibles()
      var x = 0
      var iterador = 0
      var iteradorInterno
      var left
      var media
      
      if (numeroRandom === 0) {
        while (iterador < num) {
          iterador++
        }
        iterador = Math.abs(iterador)
      } else if (numeroRandom < iterador) {
        iteradorInterno = Math.abs(numeroRandom)
        while (iteradorInterno < num) {
          iterador++
          iteradorInterno++
        }
        iterador = Math.abs(iterador)
      } else {
        iteradorInterno = Math.abs(numeroRandom)
        while (iteradorInterno < num) {
          iterador++
          iteradorInterno++
        }
        iterador = -Math.abs(iterador)
      }

      if (numeroRandom > 0) {
        x += numeroRandom
        maxMonster += iterador
      } else {
        x += iterador
        maxMonster += numeroRandom
      }

      media = x + maxMonster

      if (Game.oportunity === 1) {
        x2Op = x
        maxMonster2Op = maxMonster
      } else {
        x = x2Op
        maxMonster2 = maxMonster2Op
      }

      $('.monster').removeClass('monster-highlight')
      //left = $(visibles[Math.floor(media / 2)]).position().left - 140
      if (fallen === 0) {
        if (media % 2 === 0){
          left = $(visibles[Math.floor(0)]).position().left - 100
        } else {
          left = $(visibles[Math.floor(visibles.length - 1)]).position().left + 100
        }
      }
      if (fallen === 1) {
        left = $(visibles[Math.floor(media / 2)]).position().left - 40
      }
      if (fallen === 2) {
        left = $(visibles[Math.floor(media / 2)]).position().left - 120
      }
      if (fallen === 3) {
        left = $(visibles[Math.floor(media / 2)]).position().left - 100
      }
      if (fallen === 4) {
        left = $(visibles[Math.floor(media / 2)]).position().left - 90
      }
      if (fallen === 5) {
        left = $(visibles[Math.floor(media / 2)]).position().left - 30
      }
      if (fallen === 7) {
        left = $(visibles[Math.floor(media / 2)]).position().left
      }
      if (fallen === visibles.length) {
        left = $(visibles[Math.floor(visibles.length / 2)]).position().left
      }      

      if (Game.oportunity <= 1) {
        left2 = left
      } else {
        left = left2
      }

      setTimeout(function () {
        if (fallen === visibles.length) {
          for (var i = 0; i < visibles.length; i++) {
            if (i < visibles.length / 2) {
              $(monstrillos2[i]).addClass('monster-laying-left')
            } else {
              $(monstrillos2[i]).addClass('monster-laying-right')
            }
          }
        } else {
          for (; x < maxMonster; x++) {
            if (x < maxMonster / 2) {
              $(monstrillos2[x]).addClass('monster-laying-left')
            } else {
              $(monstrillos2[x]).addClass('monster-laying-right')
            }
          }
        }
        if (Monsters.getLyingMonsters().length > 0) {
          audioBowl.currentTime = 0;
          audioBowl.play()
        }
        callback()
      }, 1200)

      $('.ball')
        .addClass('ball-animation')
        .css({
          left: left
        }, 1200)
        .on('webkitAnimationEnd mozAnimationEnd animationend', function () {
          $('#ball').removeAttr('style')
          .removeClass('ball-animation')
        })

      $('.ball-shadow')
          .addClass('ball-shadow-animation')
          .css({
            left: left
          }, 1200)
          .on('webkitAnimationEnd mozAnimationEnd animationend', function () {
            $('#ballShadow').removeAttr('style')
            .removeClass('ball-shadow-animation')
          })
    },
    newMonsters: function () {
      // $('.monsters')
      //   .data('slide', Math.floor((Math.random() * 8) + 1))
      //   .attr('data-slide', Math.floor((Math.random() * 8) + 1))
      // Show all hiddne monsters
      this.$listMonster.each(function (index, elem) {
        $(elem).removeClass('monster-hide')
      })

      // ramdomly hidde some monster
      var number = Math.floor((Math.random() * 10) + 1)
      for (var i = 0; i < number; i++) {
        var monster = $(this.$listMonster[(Math.floor(Math.random() * number) + 1)])
        monster.addClass('monster-hide')
      }
      // Saca un data slide diferente cada que se refresca la pagina.
      // Pone el primer numero en el lugar
      limite = Monsters.visibles().length + 1
      Equation.$valueOne.html(limite - 1)
      Game.oportunity = 1
    },
    visibles: function () {
      return this.$listMonster.filter(':visible')
    }
  }

  var Game = {
    flag: false,
    oportunity: 1,
    arrayRandomBackGround: [],
    $cleanButton: $('#cleanButton'),
    $goButton: $('#goButton'),
    clean: function () {
      if (this.$cleanButton.hasClass('icon-reset-active')) {
        Equation.$valueTwo.html('')
        Equation.$resultValue.html('')
        Equation.$checkButton.removeClass('check-correct check-incorrect')
        Equation.$checkButton.addClass('check-button-disabled')
        this.$cleanButton.removeClass('icon-reset-active')
        this.$cleanButton.addClass('icon-reset-inactive icon-reset-scroll')
        Monsters.$listMonster.removeClass('monster-laying-left monster-laying-right')
        Monsters.newMonsters()
        this.$goButton.removeClass('go-button-correct go-button-incorrect')
          .css('visibility', 'visible')
        Equation.$valueTwo.removeClass('numbers-wrong')
        Equation.$resultValue.removeClass('numbers-wrong')
        Equation.$valueTwo.addClass('numbers-empty ui-droppable')
        Equation.$resultValue.addClass('numbers-empty ui-droppable')
        this.oportunity = 1
      }
    },
    go: function () {
      this.flag = true
      // Genera nuero aleatorio
      num = Math.floor((Math.random() * limite))

      if (Game.oportunity === 1) {
        num2 = num
      } else {
        num = num2
      }

      // Agrega monstrillos al arreglo
      var monstrillos2 = Object.keys(Monsters.visibles()).map(function (k) {
        return Monsters.visibles()[k]
      })

      var maxMonster = Monsters.visibles().length
      fallen = maxMonster - num
      Equation.result = num
      var numeroRandom = Math.floor(Math.random() * num - 1) // + 1

      numeroRandom *= Math.floor(Math.random() * 2) === 1 ? 1 : -1

      if (Game.oportunity === 1) {
        numeroRandom2Op = numeroRandom
      } else {
        numeroRandom = numeroRandom2Op
      }
      // Escenarios para tumbar monitos
      Monsters.hitMonsters(maxMonster, monstrillos2, numeroRandom, function () {
        $('#numeroFinal').html(num)
        // Termina monstrillos al arreglo
        Game.$goButton.css('visibility', 'hidden')
        Game.$cleanButton.removeClass('icon-reset-inactive icon-reset-scroll')
        Game.$cleanButton.addClass('icon-reset-active')
        Equation.calculation(true)
      })
    },
    highlightAnswer: function () {
      setTimeout(function () {
        Equation.$valueTwo.html(fallen)
          .addClass('numbers-wrong')
          .removeClass('numbers-empty ui-droppable')
      }, 1000)

      var acumulador = $.makeArray(Monsters.getLyingMonsters()).length * 1000
      Monsters.getLyingMonsters().each(function (index, value) {
        setTimeout(function () {
          $(value).addClass('monster-highlight')
          Game.highlightSound()
          acumulador = index
          if (Monsters.getStandingMonsters().length === 0) {
            if (Monsters.getLyingMonsters().length - 1 === index) {
              Game.$goButton.addClass('go-button-correct')
                .css('visibility', 'visible')
            }
          }
        }, (index + 1) * 1000)
      })
      // wait some time in between
      setTimeout(function () {
        Equation.$resultValue.html(Equation.result)
          .addClass('numbers-wrong')
          .removeClass('numbers-empty ui-droppable')
      }, (acumulador) + 1000)

      Monsters.getStandingMonsters().each(function (index, value) {
        setTimeout(function () {
          $('#' + $(value).attr('id')).addClass('monster-highlight')
          Game.highlightSound()
          if (Monsters.getStandingMonsters().length - 1 === index) {
            Game.$goButton.addClass('go-button-correct')
              .css('visibility', 'visible')
          }
        }, acumulador + 1000 + ((index + 1) * 1000))
      })
    },
    highlightSound: function () {
      ding.load()
      ding.currentTime = 0
      ding.play()
    },
    shuffle: function () {
      var currentIndex = this.arrayRandomBackGround.length // array.length
      var temporaryValue
      var randomIndex
        // While there remain elements to shuffle...
      while (currentIndex !== 0) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex)
        currentIndex -= 1
        // And swap it with the current element.
        temporaryValue = this.arrayRandomBackGround[currentIndex]
        this.arrayRandomBackGround[currentIndex] = this.arrayRandomBackGround[randomIndex]
        this.arrayRandomBackGround[randomIndex] = temporaryValue
      }
    }
  }

  var Equation = {
    $checkButton: $('#checkBtn'),
    $valueOne: $('#valueOne'),
    $valueTwo: $('#valueTwo'),
    $resultValue: $('#valueThree'),
    calculation: function (validate) {
      if(validate && Game.flag){
        objOp.min = +this.$valueOne.text() // The plus "+" symbol transform the strng value to numeric value
        if(this.$valueTwo.text() ==="" || this.$resultValue.text() ==="" ){
          return
        }
        objOp.sus = +this.$valueTwo.text()        
        objOp.res = +this.$resultValue.text()
        this.$checkButton.removeClass('check-button-disabled')
      } else {
        this.$checkButton.addClass('check-button-disabled')
      }
      $(this).removeClass('numbers-empty')
    },
    currentValue: function (elem) {
      return elem.find('.numbers-element.ui-draggable.ui-draggable-handle')[0]
    },
    validateResult: function () {
      if (Game.$goButton.hasClass('go-button-correct')) {
        Game.clean()
        this.$valueTwo.addClass('numbers-empty ui-droppable').removeClass('numbers-wrong')
        this.$resultValue.addClass('numbers-empty ui-droppable').removeClass('numbers-wrong')
      } else if (Game.$goButton.hasClass('go-button-incorrect')) {
        this.$valueTwo.html('')
        this.$resultValue.html('')
        this.$checkButton.removeClass('check-correct check-incorrect').addClass('check-button-disabled')
        Monsters.$listMonster.removeClass('monster-laying-left monster-laying-right')
        Game.$goButton.removeClass('go-button-incorrect')
        Game.$goButton.css('visibility', 'hidden')
      }
      Game.go()
    }
  }

  /*
  ** NUMBERS
  */
  // Drag and Drop with jQuery
  var $dragNum = listElements  

  $numbers.on('touchstart', '.numbers-element', function (e) {
    $($dragNum).draggable({
      helper: 'clone',
      revert: 'invalid'
    }, false)
  })

  Equation.$valueTwo.droppable({
    drop: function (event, ui) {
      var draggable = ui.draggable.clone()
      draggable.addClass('numbers-element-dragged')
      draggable.draggable({
        helper: 'clone'
      })
      Equation.$valueTwo.html(draggable)
      Equation.calculation(true)
      event.preventDefault()
    },
    out: function (event, ui) {
      var draggable = ui.draggable.clone()
      $(draggable).fadeOut(1000)
      if (draggable[0].isEqualNode(Equation.currentValue(Equation.$valueTwo))) {
        $(Equation.currentValue(Equation.$valueTwo)).remove()
      }
      event.preventDefault()
      
      Equation.calculation(false)
    }
  })

  Equation.$resultValue.droppable({
    drop: function (event, ui) {
      var draggable = ui.draggable.clone()
      draggable.addClass('numbers-element-dragged')
      draggable.draggable({
        helper: 'clone'
      })
      Equation.$resultValue.html(draggable)
      Equation.calculation(true)
      event.preventDefault()
    },
    out: function (event, ui) {
      var draggable = ui.draggable.clone()
      $(draggable).fadeOut(1000)
      if (draggable[0].isEqualNode(Equation.currentValue(Equation.$resultValue))) {
        $(Equation.currentValue(Equation.$resultValue)).remove()
      }
      event.preventDefault()
      Equation.$checkButton.addClass('check-button-disabled')
      Equation.calculation(false)
    }
  })

  Equation.$checkButton.click(function () {
    if ($(this).hasClass('check-button-disabled') || $(this).hasClass('check-incorrect') || $(this).hasClass('check-correct')) {
      return false
    }

    if (num === objOp.res && (objOp.min - objOp.sus) === objOp.res) {
      Equation.$checkButton.addClass('check-correct')
      Game.$goButton.css('visibility', 'visible')
          .css('margin-left', '135px')
          .removeClass('go-button-incorrect')
          .addClass('go-button-correct')
    } else {
      if (Game.oportunity < 2) {
        Equation.$checkButton.addClass('check-incorrect')
        Game.$goButton.css('visibility', 'visible')
          .css('margin-left', '135px')
          .removeClass('go-button-correct')
          .addClass('go-button-incorrect')

        Game.oportunity++
      } else if (Game.oportunity >= 2) {
        Equation.$checkButton.addClass('check-incorrect')
        Game.$goButton.css('margin-left', '135px')
          .removeClass('go-button-incorrect')
          .addClass('go-button-correct')

        Game.highlightAnswer()
        Game.oportunity = 1
      }
    }
  })

  function init () {
    Monsters.visibles().each(function (i) {
      Game.arrayRandomBackGround.push(i + 1)
      $('#mon' + (i + 1)).css('background-image', 'url(../images/char' + Game.arrayRandomBackGround[i] + '.png)')
    })
    Instructions.init()
    Game.shuffle()

    try {
      listElements.trigger('touchstart')
    } catch (e) {}

    Game.$goButton.click(Equation.validateResult.bind(Equation))
    Game.$cleanButton.click(Game.clean.bind(Game))

    Monsters.newMonsters()
  }

  init()
}())
