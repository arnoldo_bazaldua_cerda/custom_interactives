/* Monster.js ( main ) */
$(document).on('ready',
  buildGame)

var myApp = myApp || {}
myApp.values = {
  classNames: ['banana', 'carrots', 'cheese', 'cherries', 'grapes', 'kiwis', 'meats', 'milks', 'oranges', 'strawberries', 'tires', 'tomatoes', 'watermelons'],
  showAnswer: null,
  randomCloudOrFruitFlag: 0,
  fruitAppearing: 6,
  pearsonAppAudioFlag: false
}
myApp.AnimationsIE11 = {
  isIE: function () {
    return (false || !!document.documentMode)
  },
  removeAnim: function () { /** for IE11 some animations need to be removed because it add a delay to the animations */
    if (this.isIE()) {
      $('#cloudOne').addClass('noAnimation')
      $('#cloudTwo').addClass('noAnimation').css('margin-left', '250px')
      $('#cloudThree').addClass('noAnimation').css('margin-left', '750px')
    }
  }
}
myApp.audioX = {  /** firset set the auido ID same as in the html an then use play,pause, stop*/
  $audio: $('#audio')[0], // default
  setAudio: function (audioName) {
    this.$audio = $(audioName)[0]
  },
  playA: function () {
    this.$audio.load()
    this.$audio.play()
  },
  stopA: function () {
    this.$audio.pause()
    this.$audio.currentTime = 0
  },
  pauseA: function () {
    this.$audio.load()
    this.$audio.pause()
  },
  getTimeA: function () {
    return this.$audio.currentTime
  }
}
myApp.modal = {
  show: function () {
    $('.box')
      .removeClass('animationPlayStatePlay')
      .addClass('animationPlayStatePause')
    $('#progressBar')
      .removeClass('animationPlayStatePlay')
      .addClass('animationPlayStatePause')
    myApp.audioX.setAudio('#audioBack')
    myApp.audioX.pauseA()
    $('#_black').css('display', 'block')
    myApp.audioX.setAudio('#audio')
    myApp.audioX.playA()
    $('._iModalBody img').click(function () {
      myApp.audioX.playA()
    })
  },
  hide: function () {
    $('.box')
      .removeClass('animationPlayStatePause')
      .addClass('animationPlayStatePlay')
    $('#progressBar')
      .removeClass('animationPlayStatePause')
      .addClass('animationPlayStatePlay')
    $('#_black').css('display', 'none')
    myApp.audioX.setAudio('#audioBack')
    if ((myApp.audioX.getTimeA() === 0) && (myApp.values.pearsonAppAudioFlag)) { /** time of audio is <> 0 in pearson app so added a flag */
      myApp.audioX.playA()
    }
    myApp.audioX.setAudio('#audio')
    myApp.audioX.stopA()
  }
}
myApp.fisrtSelection = {
  ShowStartScreen: function () {
    $('#typeOfGame').removeClass('signAppear').addClass('hide')
    $('.meter').removeClass('hide').addClass('signAppear')
    myScore.service.playAgainEvent()
  },
  clickableButtons: function () {
    $('#numbers').click(function () {
      myApp.values.randomCloudOrFruitFlag = 0
      myApp.values.showAnswer = true
      myApp.fisrtSelection.ShowStartScreen()
      // OperationShowOnCloud: false
    })
    $('#expressions').click(function () {
      myApp.values.randomCloudOrFruitFlag = 0
      myApp.values.showAnswer = false
      myApp.fisrtSelection.ShowStartScreen()
      // OperationShowOnCloud: True
    })
    $('#someOfEach').click(function () {
      myApp.values.randomCloudOrFruitFlag = 1
      myApp.fisrtSelection.ShowStartScreen()
      // OperationShowOnCloud: True AND False
    })
  }
}
function buildGame () {
  myApp.AnimationsIE11.removeAnim()
  myApp.modal.show()
  $('#startGame').addClass('hide')
  $('div').filter('.MonsterHolder').hide()
  $('.go.score').addClass('hide')
    /* close PopUp */
  $('#icoClose').click(function () {
    myApp.modal.hide()
    $('#cloudUno').addClass('cloudMoveLeft')
  })
  myApp.fisrtSelection.clickableButtons()
  $('#startGame').click(function () {
    myApp.values.pearsonAppAudioFlag = true
    $(this).addClass('hide')
    startGame()
    myApp.audioX.setAudio('#audioBack')
    myApp.audioX.playA()
  })
  $('.contentFeedback .btInfo').click(function () {
    myApp.modal.show()
  })
  $('.contentFeedback .icoReset').click(function () {
    myScore.service.restartTurn = 0
    myScore.service.removeMonsterAndFruits()
    $('div#congratFeedback').addClass('congratFeedback-hide')
    $('#startGame').addClass('hide')
    $('.icoReset').addClass('icoResetMove')

    setTimeout(function () {
      $('#typeOfGame').removeClass('hide').addClass('signAppear')
      $('.icoReset').removeClass('icoResetMove')
    }, 400)
  })
  myDragDrop.service.drag('.ui-draggable')
  myDragDrop.service.drop('.monstercito')
}

function startGame () {
  myTimer.service.startTimer()
  var container = $('.box')
  var fruitsN = []
  var fruitN = []
  var operations = MathOperations.builder.getOperations({
    'operations': 6
  })
  var numberFruit = null
  var equation = null
  var fruitNameN = null
  var fruitName = null
  var auxBox = []
  var equationEval = null
  var containerLength = container.length
  for (var j = 0; j < containerLength; j++) {
    var index = Math.floor((Math.random() * 6) + 1)
    auxBox[j] = walkingFood.service.generateRandomToAppearAtTheBegin(index, auxBox)
  }
  container.each(function (index, value) {
    equation = operations[index][0] + operations[index][1] + operations[index][2]
    equationEval = eval(equation) // eslint-disable-line
    numberFruit = (equationEval >= 10 ? 10 : equationEval)
    fruitNameN = myRandom.service.getRandomClass(0, 11, fruitN)
    fruitName = myApp.values.classNames[fruitNameN]
    $(this)
      .removeClass('hide')
      .find('.fondo')
      .addClass(fruitName)
      .addClass(fruitName + '-' + numberFruit)
      .addClass('fondoShake')
    $(this)
      .find('.fruitHolder')
      .addClass('fruitHolder-' + fruitName)
      .addClass(fruitName + '-' + numberFruit)
    $(this)
      .find('.bubble')
      .html(!myApp.values.showAnswer ? equationEval : equation)
    $(this)
      .addClass(walkingFood.values.classBoxPos[index])
      .css('animation-name', 'fruitMove')
    myTimer.service.setFruitOffset(this, auxBox[index])
    if (index >= 9) {
      fruitsN.length = 0
    }
    fruitsN.push(numberFruit)
    fruitN.push(fruitNameN)
  })
  myTimer.service.endGame()
}
