/* DragDrop.js using  Jquery-UI */
var myDragDrop = myDragDrop || {}
myDragDrop.values = {}
myDragDrop.service = {
  drag: function (draggableSelector) {
    $(draggableSelector).draggable({
      helper: function () {
        var clone = $(this).clone()
        clone
          .addClass('bigger')
          .parent()
          .removeAttr()
          .removeClass()
        clone
            .find('.fondo')
            .removeClass('fondoShake')
        return clone
      },
      /* hides the real element when dragging */
      start: function (event, ui) {
        $(this).removeClass('dragDropVisible').addClass('dragDropHidden')
        /* invisible method is sset in the JqPluggins */
      },
      appendTo: 'section',
      containment: '#mainHolder',
      stop: function (event, ui) {
        $(this).removeClass('dragDropHidden').addClass('dragDropVisible')
      }
    })
  },
  drop: function (DroppableSelector) {
    $(DroppableSelector).droppable({
      drop: function (event, ui) {
        myScore.service.validate(ui.draggable)
      }
    })
  }
}

/* rndom.js */
var myRandom = myRandom || {}
myRandom.values = {}
myRandom.service = {
  getGameLevel: function () {
    var level = 3
    return level
  },
  getRandomClass: function ($min, $max, $not) {
    if ($not.length > 0) {
      do {
        var x = Math.floor(Math.random() * (($max + 1) - $min)) + $min
        var z = $not.indexOf(x)
      } while (z > -1)
    } else {
      x = Math.floor(Math.random() * (($max + 1) - $min)) + $min
    }
    return x
  }
}

/* validateNrestart.js */
var myScore = myScore || {}
myScore.service = {
  operations: [],
  Score: 0,
  restartTurn: 0,
  index: 0, // index of the operations
  setScore: function (score) {
    if ((myScore.service.Score + score) >= 0) {
      myScore.service.Score += score
    }
  },
  validate: function (draggable) {
    var value = eval(draggable.find('.bubble').html()) // eslint-disable-line
    var score1 = 0
    if (MathOperations.builder.getAnswer() === parseInt(value, 10)) {
      score1++
      myScore.service.restartTurn++
      myScore.service.timeOutMouth('.bocaEating', '.eating')
      draggable.addClass('hide')
      myApp.audioX.setAudio('#audioTrue')
      myApp.audioX.playA()
      setTimeout(function () {
        draggable.removeClass('hide')
      }, 500)
/*  ********************restart turb set to 10*******************************/
/*      if (myScore.service.restartTurn === 10) {
        myScore.service.setScore(score1)
        myScore.service.restart(false)
        myScore.service.displayCongratulationsMessage()
        myScore.service.restartTurn = 0
      } else {
        myScore.service.restart(true)
      }*/
      value = 0
      var box = draggable.closest('.box')
      box.find('.container').addClass('hide')
      $('div').filter('#sign').removeClass('signAppear')
      setTimeout(function () {
        myScore.service.cloudApear()
        $('div').filter('#sign').addClass('signAppear')
      }, 500)
      setTimeout(function () {
        myScore.service.operations = MathOperations.builder.getOperations({
          'operations': 6
        })
        myScore.service.bruteForceAnwer()
      }, 520)
    } else {
      score1--
      myScore.service.timeOutMouth('.bocaEww', '.wrong')
      myApp.audioX.setAudio('#audioFalse')
      myApp.audioX.playA()
      draggable.removeClass('dragDropHidden').addClass('dragDropVisible')
    }
    myScore.service.setScore(score1)
    $('#total').html(myScore.service.Score)
  },
  timeOutMouth: function (replace1, replace2) {
    $('.bocaNormal').hide()
    $('.face.normal').hide()
    $(replace1).show()
    $('.face' + replace2).show()
    setTimeout(function () {
      $(replace1).hide()
      $('.face' + replace2).hide()
      $('.face.normal').show()
      $('.bocaNormal').show()
    }, 500)
  },
  displayCongratulationsMessage: function () {
    var divCongrat = $('div').filter('#congratFeedback')
    divCongrat.removeClass('congratFeedback-hide')
    divCongrat.find('span').filter('.score').html(myScore.service.Score)
    myScore.service.removeMonsterAndFruits()
    $('div#congratFeedback').click(function () {
      $(this).addClass('congratFeedback-hide')
      setTimeout(function () {
        $('#typeOfGame').removeClass('hide').addClass('signAppear')
      }, 500)
    })
  },
  cloudApear: function () {
    if (myApp.values.randomCloudOrFruitFlag === 1) {
      myApp.values.showAnswer = ((Math.floor((Math.random() * 2) + 1) === 1) ? true : false) // eslint-disable-line
    }
    MathOperations.builder.generateCloudOperation(myApp.values.showAnswer)
  },
  removeMonsterAndFruits: function () {
    myApp.values.pearsonAppAudioFlag = false
    myApp.audioX.setAudio('#audioBack')
    myApp.audioX.stopA()
    $('div').filter('#sign').removeClass('signAppear')
    $('.meter').removeClass('signAppear').addClass('hide')
    $('#progressBar').css('width', 0)
    $('#progressBar').removeClass()
/*    myScore.service.restart(false)*/
    $('div').filter('.MonsterHolder').hide()
    $('div').filter('.go').filter('.score').removeClass('signAppear').addClass('hide')
    $('.box').each(function (index, value) {
      $(this).removeAttr('style')
      $(this).removeClass().addClass('box')
      $(this).addClass('hide')
      $(this).find('.container').removeClass('hide')
      $(this).find('.fondo').removeClass().addClass('fondo')
      $(this).find('.fruitHolder').removeClass().addClass('fruitHolder')
      $(this).find('.bubble').html()
    })
  },
  playAgainEvent: function () {
    $('div#congratFeedback').addClass('congratFeedback-hide')
    $('div').filter('.go').filter('.score').find('span').filter('#total').html('0')
      // reset the score
    myScore.service.Score = 0
    setTimeout(function () {
      $('div').filter('#startGame').filter('.go').removeClass('hide')
      $('div').filter('.go').filter('.score').removeClass('hide')
      myScore.service.cloudApear()
      $('div').filter('.MonsterHolder').show(function () {
        $('div').filter('#sign').addClass('signAppear')
        $('div').filter('.go').filter('.score').addClass('signAppear')
      })
    }, 500)
  },
  bruteForceAnwer: function (box) { // force the anwers to apear in the second positions of the array
    var answer = MathOperations.builder.getOperations({ //  eslint-disable-line
      'operations': 1,
      'minNumOfAnwers': 1
    })
    var indexPlus = myScore.service.index + 2  // set the postion of the anwers
    var indexBounds = indexPlus < myApp.values.fruitAppearing ? indexPlus : 0
    myScore.service.operations[indexBounds] = answer[0]
    /* console.log('answer is in:' + indexBounds)*/
  },
  IsTheFruitOut: function () {
    var allAnimationsEnd = 0
    $('div').on('animationend', '.box', function (e) {
      allAnimationsEnd++
      if (allAnimationsEnd > 6) {
        allAnimationsEnd = 1
      }
      if (allAnimationsEnd === 1) {
        myScore.service.operations = MathOperations.builder.getOperations({
          'operations': 6
        })
        myScore.service.bruteForceAnwer()
      }
      if (allAnimationsEnd === 3) {
      /*  operations[2] = MathOperations.builder.getOperations({
          'operations': 1,
          'minNumOfAnwers': 1
        })*/
      }
      var fruit = $(this).clone()
      $(this).remove()
    /*  $(this).css('animation-delay', '')*/
      if ($(fruit).find('.img').hasClass('dragDropHidden')) {
        $(fruit).find('.img').removeClass('dragDropHidden').addClass('dragDropVisible')
      }
      myScore.service.index = allAnimationsEnd - 1 >= 0 || allAnimationsEnd - 1 <= 6 ? allAnimationsEnd - 1 : 0 // if out of bounds reset to cero.
      // console.log('operations[' + myScore.service.index + '] = ' + myScore.service.operations[myScore.service.index])
      walkingFood.service.generateFruit(myApp.values.showAnswer, fruit, myScore.service.operations[myScore.service.index])
    })
  }
}
$(document).ready(function () {
  $('div').filter('#sign').addClass('signAppear')
  myScore.service.IsTheFruitOut()
})

/* ************************ timer.js **************************************
 **
 ********************************************************** **/
var myTimer = myTimer || {}
myTimer.values = {
  globalTime: 60,
  fruitSpeed: 10
}
myTimer.service = {
  setFruitOffset: function (box, index) {
    index = index - 1
    var offset = (myTimer.values.fruitSpeed * index / (myApp.values.fruitAppearing))
    $(box)
       .not('.noneFruits')
      .css('animation-delay', offset + 's')
  },
  startTimer: function () {
    $('#progressBar').css('width', 0)
    $('#progressBar').addClass('timeActive')
    $('#progressBar').css('animation-duration', myTimer.values.globalTime + 's')
    $('.box')
      .not('.noneFruits')
      .css('animation-duration', myTimer.values.fruitSpeed + 's')
  },
  endGame: function () {
    $('#progressBar').on('animationend', function (e) {
      myScore.service.displayCongratulationsMessage()
      $('#progressBar').removeClass('timeActive')
    })
  }
}


 /* ****************************mathOperation.js*****************************
  ****************************************************************************/
/**
** You can set the following properties:
** - operationType -> type of operation (addition & subtraction)
**                 -> If you want only addition operations or subtraction operations
**                 -> you have to modify the randomOp property and set it to false
**   Can be set as: 1 (addition) or 2 (substraction)
**   Default Value: 1
** - upperLimit ->
**   Can be set as: whatever whole number
**   Default Value: 10
** - lowerLimit ->
**   Can be set as: whatever whole number
**   Default Value: 0
** - operations -> totals number of operations
**   Can be set as: whatever natural numbers
**   Default Value: 1
** - randomOp -> allows you to get addition or subtraction equation
**   Can be set as: true (generates addition or subtraction equation) or false (generate only equations as operationType was defined)
**   Default Value: true
** - showAnswer -> boolean value to show the answer instead of the operation
**   Can be set as: true (shows the answer) or false (shows the operation)
**   Default Value: false
**/

var MathOperations = MathOperations || {}
MathOperations.values = {}

MathOperations.builder = {
  plus: 1,
  minus: 2,
  answer: 0,
  operation: '',
  equation: '',
  allOperations: [],
  auxSettings: null,
  CloudOp: 0,
  generateMathOperations: function (options) {
    /* Setting options for the function */
    var settings = this.setValues(options)
    var countIfNotAnswer = 0
    this.auxSettings = settings
    var positionOfAnwers = []
    var numOfAnwers = settings.minNumOfAnwers
    // verify the upperLimit and lowerLimit
    if (settings.upperLimit > settings.lowerLimit) {
      this.op = this.generateOperation(settings)
      this.allOperations = this.op
      var opSize = this.op.length
      var stringOperation = ''
      for (var i = 0; i < opSize; i++) {
        stringOperation = this.op[i][0] + this.op[i][1] + this.op[i][2]
        this.equation = stringOperation
        this.answer = eval(stringOperation)
        if (this.answer === this.CloudOp) {
          countIfNotAnswer++
          positionOfAnwers.push(i);
        }
      }
      var answersInArray = 0
      var conditionalString = ''
      while(countIfNotAnswer < numOfAnwers){ // minimum time you want the anwers to appear in the array
        var tempRndom =  0
        var TempAnswer = []
        var number1 = 0
        var number2 = 0
        number1 = Math.floor((Math.random() * this.CloudOp) + 1)
        if (settings.operationType === this.plus) {
          number2 = this.CloudOp - number1
          TempAnswer.push(number1)
          TempAnswer.push('+')
          TempAnswer.push(number2)
      } else {
        number2 = this.CloudOp + number1
        TempAnswer.push(number1)
        TempAnswer.push('-')
        TempAnswer.push(number2)
      }
      do { // will validate that new position of the new anwers generated is not repeated 
        tempRndom =  Math.floor(Math.random() * opSize)
        positionOfAnwers.forEach(function(value, index){
          if (positionOfAnwers.length >= 1){
            if(index === 0){
             conditionalString = '(' + tempRndom + '===' + positionOfAnwers[index] + ')' 
            }else {
             conditionalString = conditionalString + ' || (' + tempRndom + '===' + positionOfAnwers[index] + ')' 
            }
          } else {
            conditionalString = false
          }
        })
        } while (eval(conditionalString))
    
        positionOfAnwers.push(tempRndom)
        this.op[tempRndom] = TempAnswer
        countIfNotAnswer = countIfNotAnswer + ++answersInArray
      }
      return this.op
    } 
  },
  generateCloudOperation: function (showSolution) {
    var Cloudoperation = this.generateOperation(this.setValues({'operations': 1}))
    var StrOp =  Cloudoperation[0][0] + Cloudoperation[0][1] + Cloudoperation[0][2]
    this.CloudOp = eval(StrOp)
    if (showSolution) {
      $('div').filter('#sign').find('id').find('h2').html(this.CloudOp)
    } else {
      $('div').filter('#sign').find('id').find('h2').html(StrOp)
    }
  },

  generateOperation: function (settings) {
    var upperLimit = settings.upperLimit           // set the upper limit
    var lowerLimit = settings.lowerLimit           // set the lower limit
    var operationType = settings.operationType     // set the operatio type
    var operationChar = ''
    var randomOp = settings.randomOp                   // set if random numbers is requested
    var operations = settings.operations
    var randomNO = settings.randomNO
    var operationArray = []
    var num1 = null    // 1st number will be generated by random numbers between 1 && 10
    var num2 = null    // 2nd will be generated based on num1
    var operationArrayAux = [] // array is used as an auxiliar to save the operation generated
    if (randomNO) {
      operations = this.generateRandomNumberOfOperations()
    }
    for (var i = 0; i < operations; i++) {
      operationArrayAux = []
      num1 = Math.floor((Math.random() * upperLimit) + lowerLimit)
      if (randomOp) {
        operationType = this.getRandomOperation()
      }
      if (operationType === this.plus) {
        operationChar = ' + '
        if (num1 === upperLimit) {
          num2 = 0
        } else {
          if (num1 === 0) {
            num2 = Math.floor((Math.random() * (upperLimit - num1)) + 1)
          } else {
            num2 = Math.floor((Math.random() * (upperLimit - num1)) + lowerLimit)
          }
        }
      } else if (operationType === this.minus) {
        operationChar = ' - '
        num2 = Math.floor((Math.random() * num1) + lowerLimit)
      }
      operationArrayAux.push(num1)
      operationArrayAux.push(operationChar)
      operationArrayAux.push(num2)
      operationArray.push(operationArrayAux)
    }
    return operationArray
  },
  setValues: function (options) {
    return $.extend({
      'operationType': 1,
      'upperLimit': 11,
      'lowerLimit': 1,
      'operations': 6,
      'minNumOfAnwers': 1,
      'randomOp': false,
      'randomNO': false
    }, options)
  },
  getAnswer: function () {
    return this.CloudOp
  },
  getOperation: function () {
    return this.operation
  },
  getOperations: function (options) {
    options = MathOperations.builder.setValues(options)
    return MathOperations.builder.generateMathOperations(options)
  },
  getRandomOperation: function () {
    var operationRandom = Math.floor(Math.random() * 10) + 1
    if ((operationRandom % 2) === 0) {
      return 1
    } else {
      return 2
    }
  },
  generateRandomNumberOfOperations: function () {
    return Math.floor(Math.random() * (11 - 4)) + 4
  }
}


/* ************************ WalingFood.js **************************************
 **
 ************************* ********************************** **/
var walkingFood = walkingFood || {}
walkingFood.values = {
  classBoxPos: ['first', 'second', 'third', 'fourth', 'fifth', 'sixth'],
  boxPositionAux: null
}
walkingFood.service = {
/* generates a random number to set the position of the box */
  generateFruit: function (isAnswerShowed, box, operation) {
    var fruitN = []
    var numberFruit = null
    var equation = ''
    var fruitNameN = null
    var fruitName = null
    var equationEval = null
    if (operation === undefined) {
      var undefOp = MathOperations.builder.getOperations({ //  eslint-disable-line
        'operations': 1,
        'minNumOfAnwers': 0
      })
      operation = undefOp[0]
    }
    equation = operation[0] + operation[1] + operation[2]
    equationEval = eval(equation) // eslint-disable-line
    numberFruit = equationEval >= 10 ? 10 : equationEval

    var index = Math.floor((Math.random() * 3) + 1)
    index = walkingFood.service.generateRandomIndex(index)
    var boxPosition = 0
    if (walkingFood.values.boxPositionAux === null) {
      boxPosition = Math.floor((Math.random() * 5))
      walkingFood.values.boxPositionAux = boxPosition
    } else {
      boxPosition = walkingFood.service.verifyBoxPosition()
    }
    var $container = $(box)
    $container
      .not('.noneFruits')
      .removeClass()
      .addClass('box')
      .css('animation-name', 'fruitMove')
      /* .addClass('boxAnimation')*/
      .css('animation-timing-function', 'linear')
      .css('-webkit-animation-timing-function', 'linear')
      .css('animation-name', 'fruitMove')
      .css('animation-delay', '0s')
      .addClass(walkingFood.values.classBoxPos[boxPosition])
    fruitNameN = myRandom.service.getRandomClass(0, 11, fruitN)
    fruitName = myApp.values.classNames[fruitNameN]
    var fondo = $container.find('.fondo')
    $(fondo)
      .removeClass()
      .addClass(' fondo')
      .addClass(fruitName)
      .addClass(fruitName + '-' + numberFruit)
      .addClass('fondoShake')
    var fruitHolder = $(box).find('.fruitHolder')
    $(fruitHolder).removeClass()
    $(fruitHolder).addClass('fruitHolder').addClass('fruitHolder-' + fruitName).addClass(fruitName + '-' + numberFruit)
    if (!$(box).find('.img').hasClass('ui-draggable')) {
      $(box).find('.img').addClass('ui-draggable')
    }
    if ($(box).find('.container').hasClass('hide')) {
      $(box).find('.container').removeClass('hide')
    }
    $(box).find('.bubble').html(!isAnswerShowed ? equationEval : equation)
    // add the box to '.dsp' div
    $('.dsp').append(box)
    myDragDrop.service.drag('.ui-draggable')
  },
  generateRandomIndex: function (index) {
    if (myScore.service.temp === index) {
      index = Math.floor((Math.random() * 6) + 1)
      return walkingFood.service.generateRandomIndex(index)
    } else {
      myScore.service.temp = index
      return index
    }
  },
  /* generates a random number to set the position of the box */
  generateRandomToAppearAtTheBegin: function (index, auxBox) {
    if (auxBox.length > 0) {
      for (var x in auxBox) {
        if (auxBox[x] === index) {
          index = Math.floor((Math.random() * 6) + 1)
          return walkingFood.service.generateRandomToAppearAtTheBegin(index, auxBox)
        }
      }
    }
    return index
  },
  verifyBoxPosition: function () {
    var upper = 5
    var lower = 0
    var limit = 0
    if (walkingFood.values.boxPositionAux >= lower && walkingFood.values.boxPositionAux <= 2) {
      limit = walkingFood.values.boxPositionAux + 3
      walkingFood.values.boxPositionAux = Math.floor(Math.random() * ((upper + 1) - (limit - 1))) + (limit - 1)
    } else if (walkingFood.values.boxPositionAux >= 3 && walkingFood.values.boxPositionAux <= upper) {
      limit = walkingFood.values.boxPositionAux - 3
      walkingFood.values.boxPositionAux = Math.floor(Math.random() * limit) + lower
    }
    return walkingFood.values.boxPositionAux
  }
}

/* Monster.js ( main ) */
$(document).on('ready',
  buildGame)

var myApp = myApp || {}
myApp.values = {
  classNames: ['banana', 'carrots', 'cheese', 'cherries', 'grapes', 'kiwis', 'meats', 'milks', 'oranges', 'strawberries', 'tires', 'tomatoes', 'watermelons'],
  showAnswer: null,
  randomCloudOrFruitFlag: 0,
  fruitAppearing: 6,
  pearsonAppAudioFlag: false
}
myApp.AnimationsIE11 = {
  isIE: function () {
    return (false || !!document.documentMode)
  },
  removeAnim: function () { /** for IE11 some animations need to be removed because it add a delay to the animations */
    if (this.isIE()) {
      $('#cloudOne').addClass('noAnimation')
      $('#cloudTwo').addClass('noAnimation').css('margin-left', '250px')
      $('#cloudThree').addClass('noAnimation').css('margin-left', '750px')
    }
  }
}
myApp.audioX = {  /** firset set the auido ID same as in the html an then use play,pause, stop*/
  $audio: $('#audio')[0], // default
  setAudio: function (audioName) {
    this.$audio = $(audioName)[0]
  },
  playA: function () {
    this.$audio.load()
    this.$audio.play()
  },
  stopA: function () {
    this.$audio.pause()
    this.$audio.currentTime = 0
  },
  pauseA: function () {
    this.$audio.load()
    this.$audio.pause()
  },
  getTimeA: function () {
    return this.$audio.currentTime
  }
}
myApp.modal = {
  show: function () {
    $('.box')
      .removeClass('animationPlayStatePlay')
      .addClass('animationPlayStatePause')
    $('#progressBar')
      .removeClass('animationPlayStatePlay')
      .addClass('animationPlayStatePause')
    myApp.audioX.setAudio('#audioBack')
    myApp.audioX.pauseA()
    $('#_black').css('display', 'block')
    myApp.audioX.setAudio('#audio')
    myApp.audioX.playA()
    $('._iModalBody img').click(function () {
      myApp.audioX.playA()
    })
  },
  hide: function () {
    $('.box')
      .removeClass('animationPlayStatePause')
      .addClass('animationPlayStatePlay')
    $('#progressBar')
      .removeClass('animationPlayStatePause')
      .addClass('animationPlayStatePlay')
    $('#_black').css('display', 'none')
    myApp.audioX.setAudio('#audioBack')
    if ((myApp.audioX.getTimeA() === 0) && (myApp.values.pearsonAppAudioFlag)) { /** time of audio is <> 0 in pearson app so added a flag */
      myApp.audioX.playA()
    }
    myApp.audioX.setAudio('#audio')
    myApp.audioX.stopA()
  }
}
myApp.fisrtSelection = {
  ShowStartScreen: function () {
    $('#typeOfGame').removeClass('signAppear').addClass('hide')
    $('.meter').removeClass('hide').addClass('signAppear')
    myScore.service.playAgainEvent()
  },
  clickableButtons: function () {
    $('#numbers').click(function () {
      myApp.values.randomCloudOrFruitFlag = 0
      myApp.values.showAnswer = true
      myApp.fisrtSelection.ShowStartScreen()
      // OperationShowOnCloud: false
    })
    $('#expressions').click(function () {
      myApp.values.randomCloudOrFruitFlag = 0
      myApp.values.showAnswer = false
      myApp.fisrtSelection.ShowStartScreen()
      // OperationShowOnCloud: True
    })
    $('#someOfEach').click(function () {
      myApp.values.randomCloudOrFruitFlag = 1
      myApp.fisrtSelection.ShowStartScreen()
      // OperationShowOnCloud: True AND False
    })
  }
}
function buildGame () {
  myApp.AnimationsIE11.removeAnim()
  myApp.modal.show()
  $('#startGame').addClass('hide')
  $('div').filter('.MonsterHolder').hide()
  $('.go.score').addClass('hide')
    /* close PopUp */
  $('#icoClose').click(function () {
    myApp.modal.hide()
    $('#cloudUno').addClass('cloudMoveLeft')
  })
  myApp.fisrtSelection.clickableButtons()
  $('#startGame').click(function () {
    myApp.values.pearsonAppAudioFlag = true
    $(this).addClass('hide')
    startGame()
    myApp.audioX.setAudio('#audioBack')
    myApp.audioX.playA()
  })
  $('.contentFeedback .btInfo').click(function () {
    myApp.modal.show()
  })
  $('.contentFeedback .icoReset').click(function () {
    myScore.service.restartTurn = 0
    myScore.service.removeMonsterAndFruits()
    $('div#congratFeedback').addClass('congratFeedback-hide')
    $('#startGame').addClass('hide')
    $('.icoReset').addClass('icoResetMove')

    setTimeout(function () {
      $('#typeOfGame').removeClass('hide').addClass('signAppear')
      $('.icoReset').removeClass('icoResetMove')
    }, 400)
  })
  myDragDrop.service.drag('.ui-draggable')
  myDragDrop.service.drop('.monstercito')
}

function startGame () {
  myTimer.service.startTimer()
  var container = $('.box')
  var fruitsN = []
  var fruitN = []
  var operations = MathOperations.builder.getOperations({
    'operations': 6
  })
  var numberFruit = null
  var equation = null
  var fruitNameN = null
  var fruitName = null
  var auxBox = []
  var equationEval = null
  var containerLength = container.length
  for (var j = 0; j < containerLength; j++) {
    var index = Math.floor((Math.random() * 6) + 1)
    auxBox[j] = walkingFood.service.generateRandomToAppearAtTheBegin(index, auxBox)
  }
  container.each(function (index, value) {
    equation = operations[index][0] + operations[index][1] + operations[index][2]
    equationEval = eval(equation) // eslint-disable-line
    numberFruit = (equationEval >= 10 ? 10 : equationEval)
    fruitNameN = myRandom.service.getRandomClass(0, 11, fruitN)
    fruitName = myApp.values.classNames[fruitNameN]
    $(this)
      .removeClass('hide')
      .find('.fondo')
      .addClass(fruitName)
      .addClass(fruitName + '-' + numberFruit)
      .addClass('fondoShake')
    $(this)
      .find('.fruitHolder')
      .addClass('fruitHolder-' + fruitName)
      .addClass(fruitName + '-' + numberFruit)
    $(this)
      .find('.bubble')
      .html(!myApp.values.showAnswer ? equationEval : equation)
    $(this)
      .addClass(walkingFood.values.classBoxPos[index])
      .css('animation-name', 'fruitMove')
    myTimer.service.setFruitOffset(this, auxBox[index])
    if (index >= 9) {
      fruitsN.length = 0
    }
    fruitsN.push(numberFruit)
    fruitN.push(fruitNameN)
  })
  myTimer.service.endGame()
}
