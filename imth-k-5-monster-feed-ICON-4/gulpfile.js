'use strict';
 
 /*dependencies*/
var gulp = require('gulp');
var sass = require('gulp-sass');
var eslint = require('gulp-eslint');
var server = require('gulp-server-livereload');
var concat = require('gulp-concat'); 
var uglify = require('gulp-uglify');
var standard = require('gulp-standard');

 /*tasks configurations*/
gulp.task('compressJS', function() {
    gulp.src(['./js/dragDrop.js',
      './js/rndom.js',
      './js/validateNrestart.js',
      './js/timer.js',
      './js/mathOperations.js',
      './js/walkingFood.js',
      './js/monster.js'])
    .pipe(concat('allScripts.js'))
/*    .pipe(uglify())*/
    .pipe(gulp.dest('./js'));
});

/*ESlint and watch*/
gulp.task('lint', function () {
  return gulp.src('./js/monster.js')
  .pipe(eslint())
  .pipe(eslint.format())
  .pipe(eslint.failAfterError());
});

gulp.task('ugly',function() {
  gulp.src('./js/allScripts.js')
  .pipe(uglify())
   .pipe(gulp.dest('./js'));   
  gulp.src(['../scripts/jquery-1.10.2.min.js',
      '../scripts/jquery-ui-1-11-4.min.js',
      './js/allScripts.js',
      '../scripts/jquery.ui.touch-punch.min.js'
      ])
  .pipe(concat('app.min.js'))
  .pipe(gulp.dest('./js'));
});

/*server task*/
gulp.task('webserver', function() {
  gulp.src('.')
    .pipe(server({
      livereload: true,
      directoryListing: true,
      open: true
    }));
});


gulp.task('standard', function () {
  return gulp.src(['./js/jqPlugins.js',
      './js/dragDrop.js',
      './js/rndom.js',
      './js/validateNrestart.js',
      './js/timer.js',
      './js/walkingFood.js',
      './js/monster.js'])
    .pipe(standard({
     "globals": [ "$","MathOperations","myScore","myDragDrop","myRandom","myTimer","startGame","myApp","walkingFood"]
    }))
    .pipe(standard.reporter('default', {
      breakOnError: true
    }))
});


gulp.task('watch', function () {

  gulp.watch(['./js/dragDrop.js',
      './js/rndom.js',
      './js/validateNrestart.js',
      './js/timer.js',
      './js/walkingFood.js',
      './js/monster.js'],
      ['standard']);
gulp.watch('./js/*.js',['compressJS']);
});

gulp.task('build', ['ugly'])
/*default, running all tasks at once*/
gulp.task('default',['compressJS', 'lint', 'watch']);