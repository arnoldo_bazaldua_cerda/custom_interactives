
function iModal() {
    $("._back").css({"display": "block"});
    $("._back").addClass("_backTransition")
    $('._iModalBack').css({"display": "block"});

    try {
        audinst.src = ''
    } catch (e) {}
    try {
        delete audinst;
    } catch (e) {}
    try {
      //instructions
        audinst = new Audio('../audio/instructions/bubblegame4.mp3');
    } catch (e) {}
    try {
        audinst.play()
    } catch (e) {}

    $('.icoClose').on('click', function () {
        try {
            audinst.src = ''
        } catch (e) {}
        try {
            delete audinst;
        } catch (e) {}
        try {
            audinst = new Audio('../audio/instructions/bubblegame4.mp3');
        } catch (e) {}

         //$("._back").css({"display": "none"});
      $("._back").removeClass("_backTransition")
      $('._iModalBack').css({"display": "none"});
    });

    $('.soundButton').on('click', function () {
        try {
            audinst.src = ''
        } catch (e) {}
        try {
            delete audinst;
        } catch (e) {}
        try {
            audinst = new Audio('../audio/instructions/bubblegame4.mp3');
        } catch (e) {}
        try {
            audinst.play()
        } catch (e) {}
    });
}

function iconreset() {
  $('.icoReset').addClass('icoResetMove');
  $(".octopus").removeClass("disabled");
  $(".otter").removeClass("disabled");
  $(".boxPlayerContainer1").children(".boxContainerPlayerScore").removeClass("disabled");
  $(".boxPlayerContainer2").children(".boxContainerPlayerScore").removeClass("disabled");
  $(".boxPlayerContainer1").removeClass("enable").addClass("disable");
  $(".boxPlayerContainer2").removeClass("enable").addClass("disable");
  resetDataToSave();
  reset();
  $(".selectPlayers").css("display", "block");
}

$(".icoReset").on("click", iconreset);

$(".icoInfo").on("click", iModal);

function toggleClassButton(element,className) {
	var currentButton=element;
	if(!currentButton.hasClass(className)){
		currentButton.addClass(className);
	}else{
		currentButton.removeClass(className);
	}
	return element;
}  
