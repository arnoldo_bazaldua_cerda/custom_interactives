/* ************************ timer.js **************************************
 **
 ********************************************************** **/
var myTimer = myTimer || {}
myTimer.values = {
  globalTime: 120,
  fruitSpeed: null
}
myTimer.service = {
  setFruitOffset: function (box, index) {
    var offset = 0
    index = index - 1
    offset = (myTimer.values.fruitSpeed * index / (myApp.values.fruitAppearing))
    $(box).css('animation-delay', offset + 's')
  },
  startTimer: function () {
    $('#progressBar').css('width', 0)
    $('#progressBar').addClass('timeActive')
    $('#progressBar').css('animation-duration', myTimer.values.globalTime + 's')
    $('.box').css('animation-duration', myTimer.values.fruitSpeed + 's')
  },
  endGame: function () {
    $('#progressBar').on('animationend', function (e) {
      myScore.service.displayCongratulationsMessage()
      $('#progressBar').removeClass('timeActive')
    })
  }
}
