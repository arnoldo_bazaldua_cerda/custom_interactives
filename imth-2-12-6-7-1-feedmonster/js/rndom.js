/* rndom.js */
var myRandom = myRandom || {}
/* myRandom={
  values = {},
  service= {

}
  }*/
myRandom.values = {}
myRandom.service = {
  getGameLevel: function () {
    var level = 3
    return level
  },
  getRandomClass: function ($min, $max, $not) {
    if ($not.length > 0) {
      do {
        var x = Math.floor(Math.random() * (($max + 1) - $min)) + $min
        var z = $not.indexOf(x)
      } while (z > -1)
    } else {
      x = Math.floor(Math.random() * (($max + 1) - $min)) + $min
    }
    return x
  }
}
