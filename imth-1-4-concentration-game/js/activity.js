/**
 * @author theo.spencer
 */

$(document).ready(function(){
	instructions();
	playerSelect();
});

/******* Audio Controls *********/

var myAudio = $('#audInstructions')[0];
var successChime = $('#audCorrectMatch')[0];

function playAudio(){
	myAudio.play();
}

function stopAudio(){
	myAudio.pause();
}

function resetAudio() {
	myAudio.currentTime = 0.0;
}

function correctChime(){
	successChime.play();
}

$(".audioBtn").click(function(){
	stopAudio();
	resetAudio();
	playAudio();
});

$(".info").click(function(){
	instructions();
	resetAudio();
	playAudio();
});

$(".reset").click(function(){
	gameReset();
	$("#gameTable").empty();
	$(".player1, .player2").hide();
	playerSelect();
});

/******* Audio Controls End *********/

/******* Button Controls *********/

function closeBtn(){
	$(".icoClose").click(function() {
		$("#overlay").hide();
		$("#instructions").hide();
		stopAudio();
		resetAudio();
		$("#instructions .content").empty();
	});
}

var playerSelect = function(){
	$(".playerSelect").show();
	$(".playerSelect").addClass("appear");
	$(".playerSelect").click(function(){
		playerNum = $(this).attr("rel");
		switch (playerNum) {
			case '1':
				$(".player1").show();
				break;
			case '2':
				$(".player2").show();
				break;
		}
		$(".matches").html(matches);
		$(".tries").html(tries);
		$(".playerSelect").hide();
		tableBuilder(4,5);
		$(".playerSelect").unbind("click");
	});
};

var instructions = function(){
	$("#instructions .content").append(activitySettings.instructions);
	overlay(0);
	$("#instructions").show();
	playAudio();
	closeBtn();
};

$("#playAgain").click(function(){
	gameReset();
	playerSelect();
});

/******* Button Controls End *********/

function overlay(color) {
	$("#overlay").removeClass().show();
	switch (color) {
	case 0:
		$("#overlay").addClass("overlayBlack");
		break;
	case 1:
		$("#overlay").addClass("whiteOverlay");
		setTimeout(function(){
			$("#overlay").removeClass("whiteOverlay");	
		}, 4000)
		break;
	case 2:
		$("#overlay").removeClass();
		break;
	}
}

/***** Build Table *****/
/*Builds table to hold cards for game*/

function tableBuilder (row,col){
	colsTxt = "";
	for (j=0;j<col;j++){
		colsTxt = colsTxt+"<td><div class='card'><div class='front'><div class='panel'></div></div><div class='back'><div class='panel'></div></div></div></td>";
	}
	for (i=0; i<row; i++){
		$("#gameTable").append("<tr>"+colsTxt+"</tr>");
	} 
	setPanels(20);
    setNumbers();
	setDots();
}

var getRandom = function(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

/*Randomly assigns yellow and pink panels for game*/

function setPanels(panels){
	twentySet = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19];
	for (i=0; i<panels; i++)
	{
		mySetNum = twentySet.length-1;
		myNum = getRandom(0,mySetNum);
		setCard = twentySet[myNum];
		twentySet.splice(myNum,1);
		if (i%2 == 0){
			$(".card").eq(setCard).addClass('pink');
		} else {
			$(".card").eq(setCard).addClass('yellow');
		}
		
	}
};

function setNumbers() {
	tenSet = [0,1,2,3,4,5,6,7,8,9];
	for (i=1;i<=10; i++){
	mySetNum = tenSet.length-1;
	myNum = getRandom(0,mySetNum);
	setCard = tenSet[myNum];
	tenSet.splice(myNum,1);
	$(".yellow").eq(setCard).find(".back .panel").html(i);
	$(".yellow").eq(setCard).attr('rel',i);
}
};

function setDots() {
	tenSet = [0,1,2,3,4,5,6,7,8,9];
	for (i=1;i<=10; i++){
	mySetNum = tenSet.length-1;
	myNum = getRandom(0,mySetNum);
	setCard = tenSet[myNum];
	tenSet.splice(myNum,1);
	dots = "";
	for (j=1;j<=i;j++){
		dots=dots+"<span class='dot'></span>";
	}
	$(".pink").eq(setCard).find(".back .panel").html(function(){
		$(this).append(dots);
	});
	$(".pink").eq(setCard).attr('rel',i);
	console.log("Set card "+setCard+" Card number "+i);
	}
	
};

var ySelected = false;
var pSelected = false;
var matched = false;
var player = 1;
var matches = 0;
var tries = 0;
var matches2 = 0;
var tries2 = 0;
var totalMatches = 0;
var pChoice, yChoice, pPanel, yPanel, yelCard, pinkCard, playerNum;

$("#stage").on("click", ".card", function(){
	if ($(this).closest("#stage .card").hasClass("yellow") && ySelected == false && $(this).hasClass("match") == false) {
		$(this).closest("#stage .card").addClass("flip");
		ySelected = true;
		yChoice = $(this).attr("rel");
		yPanel = $(this).index(".card");
	} else if ($(this).closest("#stage .card").hasClass("pink") && pSelected == false && $(this).hasClass("match") == false) {
		$(this).closest("#stage .card").addClass("flip");
		pSelected = true;
		pChoice = $(this).attr("rel");
		pPanel = $(this).index(".card");
	} else {
		
	}
	checkChoice();
});

function checkChoice(){
	setTimeout(function(){
		if(ySelected == true && pSelected == true)
		{
			if(yChoice == pChoice) {
				$("body").find(".card").eq(yPanel).removeClass("flip").addClass("match");
				$("body").find(".card").eq(pPanel).removeClass("flip").addClass("match");
				yelCard = $(".card .back").eq(yPanel).html();
				pinkCard = $(".card .back").eq(pPanel).html();
				console.log(yelCard);
				if (totalMatches < 9) {
					overlay(1);
					$("#showCard").css("z-index","150");
					$("#pinkMatch").append(pinkCard).addClass('card-match');
					$("#yellowMatch").append(yelCard).addClass('card-match');
					correctChime();
					matched = true;
					$("#showCard .card").removeClass("match");		
				} else {
					//$("#pinkMatch").append(pinkCard).addClass('card-match');
					//$("#yellowMatch").append(yelCard).addClass('card-match');
				}
			} else if (yChoice != pChoice){
				$("body").find(".card").eq(yPanel).removeClass("flip");
				$("body").find(".card").eq(pPanel).removeClass("flip");
				matched = false;
			} else {
				
			}
		scoreUpdate(playerNum);
		ySelected = false;
		pSelected = false;
		} else {
			
		}
	}, 3000);
}

function scoreUpdate (players) {
	switch (players) {
		case '1':
			if (matched == true) {
				matches = matches +1
				$(".matches").html(matches);
				tries = tries +1
				$(".tries").html(tries);
				$(".player-results ul").eq(0).append("<li><div class='yellow'>"+yelCard+"</div><div class='pink'>"+pinkCard+"</div></li>")	
			} else {
				tries = tries +1
				$(".tries").html(tries);
			}
			playerSwitch();
			break;
		case '2':
			if (matched == true) {
				if ($(".player2").index(".turn") == 0){
					matches = matches+1
					$(".player2.turn .matches").html(matches);
					tries = tries +1
					$(".player2.turn .tries").html(tries);
					$(".player-results ul").eq(0).append("<li><div class='yellow'>"+yelCard+"</div><div class='pink'>"+pinkCard+"</div></li>")
				} else {
					matches2 = matches2+1
					$(".player2.turn .matches").html(matches2);
					tries2 = tries2 +1
					$(".player2.turn .tries").html(tries2);
					$(".player-results ul").eq(1).append("<li><div class='yellow'>"+yelCard+"</div><div class='pink'>"+pinkCard+"</div></li>")
				}					
			} else {
				if($(".player2").index(".turn") == 0){
					tries = tries +1
					$(".player2.turn .tries").html(tries);
				} else {
					tries2 = tries2 +1
					$(".player2.turn .tries").html(tries2);
				}
			}	
			playerSwitch();		
			break;
	}
}

function playerSwitch() {
	totalMatches = matches + matches2;
	if (totalMatches < 10) {
		$(".player2").eq(0).toggleClass("turn");
		$(".player2").eq(1).toggleClass("turn");
		$("#overlay").addClass("white-overlay");
		setTimeout(function(){
			$("#overlay").removeClass("white-overlay").hide();
			$("#pinkMatch, #yellowMatch").empty();
			$("#showCard div").removeClass("card-match");
			$("#showCard").css("z-index","1");
		}, 3000);
	} else {
		$(".player1, .player2").hide();
		$(".results").show();
		if (playerNum == 2){
			$(".player-results").show();
		} else {
			$(".player-results").eq(0).show();
		}
		gameFinal();
	}
	
}

function gameFinal() {
	$("#gameTable").empty();
	$(".results .card").removeClass('match');
}

function gameReset() {
	$(".tries, .matches, .matches2, .recap ul").empty();
	$(".recap ul li").remove();
	$("#pinkMatch, #yellowMatch").empty();
	ySelected = false;
	pSelected = false;
	matched = false;
	player = 1;
	matches = 0;
	tries = 0;
	matches2 = 0;
	tries2 = 0;
	totalMatches = 0;
	$(".results").hide();
	$(".player2").removeClass("turn");
	$(".player2").eq(0).addClass("turn");
}
