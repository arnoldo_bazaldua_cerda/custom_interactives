'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var server = require('gulp-server-livereload');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
 
gulp.task('sass', function () {
  gulp.src('./sass/styles.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./stylesheet')) ;
});
 

gulp.task('watch', function () {
  gulp.watch('./sass/**/*.scss', ['sass']);
})

gulp.task('webserver', function() {
  gulp.src('.')
    .pipe(server({
      livereload: true,
      directoryListing: true,
      open: true
    }));
});

gulp.task('js:uglify',function(){
  return gulp.src(['./js/jquery-2.1.4.min.js', 'js/jquery-ui-1.11.4.min.js','js/jquery.ui.touch-punch.min.js','js/activity.js'])
  .pipe(concat('concat.js'))
  .pipe(rename('activity.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('js'))
}); 


gulp.task('default',['sass','watch','webserver'],function(){})