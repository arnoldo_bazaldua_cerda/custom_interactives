// activity.js
$(function () {
  var yellows, purples, value_one, value_two, value_three, one, two, three

  // audio
  var audio = document.getElementById('audio')
  var beaker = document.getElementById('rattle')

  // button controlls
  var closeButton = document.querySelector('#closeButton')
  closeButton.addEventListener('click', closeInstructions)

  var goButton = document.querySelector('#goButton')
  goButton.addEventListener('click', go)

  var infoButton = document.querySelector('#infoButton')
  infoButton.addEventListener('click', showInstructions)

  var audioButton = document.querySelector('#audioButton')
  audioButton.addEventListener('click', audioInstructions)

  var validateButton = document.querySelector('#checkBtn')
  validateButton.addEventListener('click', validateValues)

  var cleanButton = document.querySelector('#cleanButton')
  cleanButton.addEventListener('click', clean)
  
window.onresize = function () {
    if (window.innerHeight > window.innerWidth) {
      adjustOverFlow()
  } else {
  }
}

function adjustOverFlow(){
    for (var ball = 1; ball <= 20; ball++) {
      if($('#ball'+ball).hasClass('ball-purple' ) ||
         $('#ball'+ball).hasClass('ball-yellow' ) &&
         parseFloat($('#ball'+ball).css('left'))>624)
        $('#ball'+ball).css('left','624px')
        console.log($('#ball'+ball).css('left'))
    }
}
  
  // Drag and Drop with jQuery
  var $dragOperator = $('#numbers li.operators')
  var $dragNum = $('#numbers li.numbers-element')
  var $dragBalls = $('#balls-thrown span.ui-widget-content')

  
  function go () {
    $dragBalls.css({
      'top': '',
      'left': ''
    })
    validate()
    reset()
    $('#numbersArea').removeClass('numbers-area-disabled')
    $('#goButton').addClass('layer-hide')
    $($dragNum).removeClass('ball ball1 ball2 ball3 ball4 ball5 ball6 ball7 ball8 ball9 ball10 layer layer-hide')
    beaker.load()
    beaker.play()
    $('#armBeaker').addClass('arm-shake')
    setTimeout(showCoins, 1300)
    $('.numbers').css('pointer-events', 'all')
    $('#results').css('pointer-events', 'all')
  }

  function audioInstructions () {
    stopInstructions()
    audio.load()
    audio.play()
  }
  
  function stopInstructions () {
    audio.pause()
    audio.currentTime = 0
  }

  function closeInstructions () {
    $('#instructionsText').toggleClass('layer layer-hide')
    stopInstructions()
  }

  function showInstructions () {
    audioInstructions()
    $('#instructionsText').toggleClass('layer layer-hide')
  }

  function howMany () {
    yellows = 0
    purples = 0
    do {
      yellows = parseInt(Math.random() * 5 + 1, 10)
      purples = parseInt(Math.random() * 5 + 1, 10)
    } while ((yellows + purples) > 10)

    var balls = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    for (var a = 0; a < yellows; a++) {
      var b = Math.ceil(Math.random() * balls.length - 1)
      $('#ball' + balls[b]).addClass('ball' + balls[b] + ' ball-yellow ')
      balls.splice(b, 1)
    }

    for (var c = 0; c < purples; c++) {
      var d = Math.ceil(Math.random() * balls.length - 1)
      $('#ball' + balls[d]).addClass('ball' + balls[d] + ' ball-purple ')
      balls.splice(d, 1)
    }

    for (var e = 0; e < balls.length; e++) {
      $('#ball' + balls[e]).removeClass('ball' + balls[e]).addClass('layer layer-hide')
    }
  }

  function showCoins () {
    for (var i = 1; i <= 10; i++) {
      $('#ball' + i).toggleClass('ball ball' + i)
    }
    howMany()
    $('#armBeaker').removeClass('arm-shake')
  }

  /*
   ** NUMBERS
   */

  $dragNum.on('touchstart', function (e) {
    $($dragNum).draggable({
      helper: 'clone'
    }, false)
  })

  $('#results li.numbers-element').droppable({
    drop: function (event, ui) {
      var draggable = ui.draggable[0].innerHTML
      this.innerHTML = draggable
      calculation()
      clearValues()
      event.preventDefault()
    }
  })  
  $('#results li.operators').droppable({
    drop: function (event, ui) {
      var draggable = ui.draggable[0].innerHTML
      this.innerHTML = draggable
      calculation()
      clearValues()
      event.preventDefault()
    }
  })

  $(function () {
    $dragBalls.draggable({
      containment: '.boardBalls'
    })
    $dragBalls.css('position', 'absolute')
  })
  var click = false
  $(function () {
    $dragBalls
      .mousedown(function () {
        click = true
      })
      .mousemove(function () {
        if (click) {
          var t = $(this).css('top')
          var l = $(this).css('left')
          $(this).css({
            'top': t,
            'left': l
          })
          $(this).removeClass('ball1 ball2 ball3 ball4 ball5 ball6 ball7 ball8 ball9 ball10')
        }
      })
      .mouseup(function () {
        click = false
      })
  })

  function initList () {
    try {
      $('#numbers li.numbers-element').trigger('touchstart')
    } catch (e) {}    
    try {
      $('#numbers li.operators').trigger('touchstart')
    } catch (e) {}
  }

  initList()

  function calculation () {
    one = document.getElementById('valueOne').innerHTML
    value_one = parseInt(one, 10)
    two = document.getElementById('valueTwo').innerHTML
    value_two = parseInt(two, 10)
    three = document.getElementById('valueThree').innerHTML
    value_three = parseInt(three, 10)
    checkValues()
  }

  function checkValues () {
    if (value_one > 0 && value_two > 0 && value_three > 0) {
      $('#checkBtn').removeClass('check-button-hide check-correct check-incorrect')
      $('#checkBtn').css('cursor', 'pointer')
      $('#checkBtn').addClass('check-button')
    }
  }

  function validateValues () {
    if (yellows == value_one && purples == value_two) {
      if (value_one + value_two == value_three) {
        $('#checkBtn').addClass('check-correct')
        $('#goButton').removeClass('layer layer-hide')
        $('#cleanButton').removeClass('icon-reset-active')
        $('#cleanButton').addClass('icon-reset-inactive')
      } else {
        $('#checkBtn').addClass('check-incorrect')
      }
    } else {
      $('#checkBtn').addClass('check-incorrect')
    }
  }

  function clearValues () {
    if (value_one > 0 || value_two > 0 || value_three > 0) {
      $('#cleanButton').removeClass('icon-reset-inactive')
      $('#cleanButton').addClass('icon-reset-active')
      $('#cleanButton').removeClass('icoResetMove')
      $('#checkBtn').css('cursor', 'pointer')
    }
  }

  function clean () {
    if ($('#cleanButton').attr('class') != 'icon-info icon-reset icon-reset-inactive') {
      $('#valueOne').html('')
      $('#valueTwo').html('')
      $('#valueThree').html('')
      $('#cleanButton').removeClass('icon-reset-active')
      $('#cleanButton').addClass('icon-reset-inactive')
      $('#checkBtn').removeClass('check-incorrect check-correct check-button')
      $('#cleanButton').addClass('icoResetMove')
    }
  }

  function reset () {
    $('#valueOne').html('')
    $('#valueTwo').html('')
    $('#valueThree').html('')
    $('#cleanButton').removeClass('icon-reset-active')
    $('#cleanButton').addClass('icon-reset-inactive')
  }

  function validate () {
    for (var i = 1; i <= 10; i++) {
      $('#ball' + i).removeClass('ball ball' + i + ' layer layer-hide ball-yellow ball-purple ')
    }
    $('#armBeaker').removeClass('arm-shake')
    $('#checkBtn').addClass('check-button-hide')
  }

}())
