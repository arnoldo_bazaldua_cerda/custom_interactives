concentrationGameAdd.directive('flippingCard',function($rootScope,engine,$timeout){
	return {
		controller:function($scope,$rootScope,engine){
			$scope.flipped=$scope.card.reverse;
			$scope.reverse=$scope.card.reverse
			$scope.selected=false;
			$scope.stay=false;
			$scope.count=0;
			$scope.$on('reset',function(){
				if(!$scope.reverse){
					if(!$scope.flipped || $scope.stay)return false;
				}else{
					if(!$scope.selected)return false;
				}
				$scope.$apply(function(){
					$scope.flipped=$scope.card.reverse;
					engine.busy-=1;
					$scope.selected=false;
					$scope.$parent.current='';
					$scope.$parent.resultPoll = [];
					$scope.$parent.resultPoll.length = 0
				})

			})
			$scope.flippingDone=function(event){

				if(interactiveName="imth-k-2-concentration-game-ICON-1" && startInteract){
					console.log("entra")
					return false;
				}
				//console.log(event.target.children[1])
				$(event.target.children[1]).addClass("qvh")
				// $(event.target.children[1]).removeAttr("style")
				//$(event.target.children[1]).addClass("flippedBack")

				var el=$(event.target).parent();
				if(!el.hasClass('flipped')) return false;

				$(event.target.children[1]).removeClass("qvh")
				$rootScope.$broadcast('countClick',$scope.card)
			}
			$scope.correctCallBack=function(event,data){

				if(!$scope.reverse){
					if(!$scope.flipped || $scope.stay)return false;
				}else{
					if(!$scope.selected)return false;
				}
				$scope.$apply(function(){
					$scope.stay=true;
					engine.busy=0;
					$scope.selected=false;
					if($scope.reverse){
						$scope.flipped=false;
					}
					$scope.$parent.current='';
				})
				//$scope.offReset();
			}

		},//transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd

		link:function($scope,elem,attrs){
			$($(elem).get(0)).off("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend")
			$($(elem).get(0)).on("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", $scope.flippingDone)
			// $(elem).get(0).addEventListener( 'webkitTransitionEnd', $scope.flippingDone);
			// $(elem).get(0).addEventListener( 'transitionend', $scope.flippingDone);
			// $(elem).get(0).addEventListener( 'MSTransitionEnd', $scope.flippingDone);

			$scope.$on("correct", $scope.correctCallBack);
			Hammer(elem[0]).on("tap", function(event) {

				if($scope.$parent.current==$scope.card.type){
					return false;
				}


				$scope.paux = true;
				$scope.$parent.current=$scope.card.type;
				for(var i in $scope.$parent.resultPoll){
					$scope.forbiden=$scope.card.type==$scope.$parent.resultPoll[i].type
				}
				event.gesture.preventDefault();
				if($scope.forbiden)return false;
				//if is reverse game we have to implmen this tap event in a diferente way
				//i don't think this game will have another implementation other than this
				if(!$scope.card.reverse){
					//cards are backwards
					if($scope.flipped || engine.busy==2) return false;

					$scope.$apply(function(){
						$scope.hideforbit = "hidebit";

						var tm = $timeout(function(){
							$scope.hideforbit = "";
						},200);

						$scope.flipped=true;
						engine.busy+=1;
					})


				}else{
					//if card is shown front
					if(engine.busy==2 || $scope.selected)return false;
					$scope.$apply(function(){
						$scope.selected=true;

					})
					engine.busy+=1;
					$rootScope.$broadcast('countClick',$scope.card)

				}
			});

			/*$(elem).get(0).addEventListener( 'webkitTransitionEnd', $scope.flippingDone);
			$scope.$on("correct", $scope.correctCallBack);
			$(elem).on('click',function(){
				if($scope.flipped) return false;
				$scope.$apply(function(){
					$scope.flipped=true;
				})
				//$rootScope.$broadcast('countClick',$scope.card)
				//$(elem).get(0).removeEventListener( 'webkitTransitionEnd', $scope.flippingDone);
				//$scope.offCorrectBroadcast =
			})*/
		}
	}

})

concentrationGameAdd.directive('appear',function($rootScope){

	return {
		link:function(scope,element,attrs){
			scope.$on('completeBackground',function(){
				$(element).addClass('appear');

			})

		}
	}


})
concentrationGameAdd.directive('resetState',function(){

	return{
		link:function(scope,elem,attr){
			$(elem).on('click',scope.resetState);
		}
	}
})
concentrationGameAdd.directive('menuItem',function($rootScope,$timeout){
	return {
		link:function($scope,elem,attr){
			Hammer(elem[0]).on("tap", function(event) {
				$rootScope.$broadcast('START_GAME',attr.menuItem)
			})
			if(JSON.parse(attr.menuItem).value==$scope.$parent.engineOptions.preset){
				$timeout(function(){

					$rootScope.$broadcast('START_GAME',attr.menuItem)
				},50);
			}
			//console.log($scope.$parent.engineOptions.preset);
			//console.log(_.findWhere($scope.$parent.menu.menuItems,{value}))

		}
	}
})
concentrationGameAdd.directive('audioController',function($rootScope,$timeout){
	return {
		link:function(scope,elem,attr){
			$rootScope.$on('playCorrect',function(){

				var successSound = new Audio("audio/109662__grunz__success.mp3");
				try{ successSound.pause();}catch(e){}
				try{ successSound.currentTime = 0;  }catch(e){}
				try{ successSound.play(); }catch(e){}
			});

			$rootScope.$on('playMatches',function(){

				var successSound = new Audio("audio/matches.mp3");
				try{ successSound.pause();}catch(e){}
				try{  successSound.currentTime = 0;  }catch(e){}
				try{  successSound.play(); }catch(e){}

			});



		}
	}

})
concentrationGameAdd.directive('playAgain',function(){
	return {
		link:function(scope,elem,attr){

			$(elem).on('click',scope.resetState)
		}
	}

})
