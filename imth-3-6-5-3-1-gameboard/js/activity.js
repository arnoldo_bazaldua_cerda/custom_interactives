var count = 0 
var arrPosX = [0, 0, 0, 0, 0, 0, 0, 0] 
var arrPosY = [0, 0, 0, 0, 0, 0, 0, 0] 
var priority = 0 
var probability = [0, 0, 0, 0] 
$(document).ready(function () {
	try {
		$(".icoReset").unbind('click', reset)
	} catch (e) {}
	$(".icoReset").bind('click', reset)
	numKeypad = new vKeyPad(".numTxt", "#vkeyCont", {
		useComplexMath: false,
		colCount: 3,
		useParseMath: false,
		deleteMethod: "byChar",
		isForceTaggable: true,
		isMultiline: false,
		maxLenMargin: 40,
		clearZero: false,
		multiSymbol: " \u00D7 ",
	})
	numKeypad.textLength = function (htmlText) {

		var textElem = $("<span>" + htmlText + "</span>")
		var textLen = textElem.text().replace(/\\s|-|(\+)|\./g, "").length
		textElem.remove()
		var iid = numKeypad.CurrentFocus.id
		temp = $("#" + iid).text()
		if (temp.charAt(2) == " " && temp.charAt(4) == " " && temp.charAt(7) == " " && temp.charAt(9) == " ") {
			numKeypad.maximumLength = 12
		}
		return textLen
	}
	numKeypad.onTextBoxFocus = function () {
		if (numKeypad.CurrentFocus.className == "numTxt multi") {
			numKeypad.setKeyState("multi", true, false)
		} else if (numKeypad.CurrentFocus.className == "numTxt oneChar") {
			numKeypad.setKeyState("multi", false, false)
		} else {
			numKeypad.setKeyState("multi", false, false)
		}
	}
	numKeypad.onTextChanged = function () {
		resertKeypadCtrl()
	}
	$(".contentRigthBtn").get(0).addEventListener("click", function () {
		if ($('.contentRigthBtn').css('opacity') == 1) {
			$('.contentRigthBtn').css({
				'opacity': 0.3
			})
			try {
				$('.contentLeftBtn').eq(0).removeClass('init-animation')
				$('.contentLeftBtn').eq(0).removeClass('throw-animation')
			} catch (e) {}

			if (count < 8) {

				$('.contentLeftBtn').addClass('init-animation')
				$('.contentLeftBtn').one('animationend webkitAnimationEnd oAnimationEnd', function () {
					if (count == 8) {
						$('.contentRigthBtn').css({
							'opacity': 0.3
						})
					} else {
						$('.contentRigthBtn').css({
							'opacity': 1
						})
					}
					endAnimationFunction()
					enableReset()
				})
			}
			count = count + 1
		}
	})
})

function getPlacement() {
	try {
		var num = Math.random()
		if (num < 0.7) placement = 0
		else if (num < 0.85) placement = 1
		else if (num < 0.94) placement = 2
		else placement = 3
		if (placement == 0 && probability[placement] < 10) {
			probability[placement] = probability[placement] + 1
			priority1()
			arrPosX[count - 1] = locationX
			arrPosY[count - 1] = locationY
			$('.coin').eq(count - 1).css({
				'display': 'block'
			}).animate({
				'top': locationY,
				'left': locationX
			}, 700)
		} else if (placement == 1 && probability[placement] < 2) {
			probability[placement] = probability[placement] + 1
			priority2()
			arrPosX[count - 1] = locationX
			arrPosY[count - 1] = locationY
			$('.coin').eq(count - 1).css({
				'display': 'block'
			}).animate({
				'top': locationY,
				'left': locationX
			}, 700)
		} else if (placement == 2 && probability[placement] < 2) {
			probability[placement] = probability[placement] + 1
			priority3()
			arrPosX[count - 1] = locationX
			arrPosY[count - 1] = locationY
			$('.coin').eq(count - 1).css({
				'display': 'block'
			}).animate({
				'top': locationY,
				'left': locationX
			}, 700)
		} else if (placement == 3 && probability[placement] < 1) {
			probability[placement] = probability[placement] + 1
			calculateLocation(466, 534, 151, 218)
			arrPosX[count - 1] = locationX
			arrPosY[count - 1] = locationY
			$('.coin').eq(count - 1).css({
				'display': 'block'
			}).animate({
				'top': locationY,
				'left': locationX
			}, 700)
		} else {
			getPlacement()
		}
		arrPosX[count - 1] = locationX
		arrPosY[count - 1] = locationY
	} catch (e) {

	}
}



function priority1() {
	priority = 1
	locate = Math.floor(Math.random() * 4)
	if (locate == 0) {
		x1 = 328, y1 = 13, x2 = 497, y2 = 13, x3 = 328, y3 = 181
	}
	if (locate == 1) {
		x1 = 502, y1 = 13, x2 = 671, y2 = 13, x3 = 671, y3 = 181
	}
	if (locate == 2) {
		x1 = 503, y1 = 357, x2 = 672, y2 = 357, x3 = 672, y3 = 182
	}
	if (locate == 3) {
		x1 = 328, y1 = 191, x2 = 498, y2 = 357, x3 = 328, y3 = 357
	}
	calculateLocation(x1, x2, y1, y3)
	placeinsideTriangle(x1, y1, x2, y2, x3, y3, locationX, locationY)
}

function priority2() {
	priority = 2
	locate = Math.floor(Math.random() * 2)
	if (locate == 0) {
		x1 = 370, y1 = 175, x2 = 417, y2 = 130, x3 = 417, y3 = 220, xx1 = 355, xx2 = 460, yy1 = 140, yy2 = 245
	}
	if (locate == 1) {
		x1 = 450, y1 = 250, x2 = 530, y2 = 250, x3 = 492, y3 = 295, xx1 = 450, xx2 = 530, yy1 = 250, yy2 = 295
	}
	calculateLocation(xx1, xx2, yy1, yy2)
	placeinsideTriangle(x1, y1, x2, y2, x3, y3, locationX, locationY)
}

function priority3() {
	priority = 3
	locate = Math.floor(Math.random() * 2)
	if (locate == 0) {
		x1 = 492, y1 = 56, x2 = 447, y2 = 102, x3 = 535, y3 = 102, xx1 = 447, xx2 = 535, yy1 = 56, yy2 = 102
	}
	if (locate == 1) {
		x1 = 565, y1 = 130, x2 = 565, y2 = 220, x3 = 610, y3 = 175, xx1 = 565, xx2 = 610, yy1 = 130, yy2 = 220
	}
	calculateLocation(xx1, xx2, yy1, yy2)
	placeinsideTriangle(x1, y1, x2, y2, x3, y3, locationX, locationY)
}


function calculateLocation(x1, x2, y1, y2) {
	locationX = Math.floor(Math.random() * ((x2 - x1) + 1) + x1)
	locationY = Math.floor(Math.random() * ((y2 - y1) + 1) + y1)
	return (locationX, locationY)
}

function placeinsideTriangle(p1x, p1y, p2x, p2y, p3x, p3y, px, py) {
	alpha = ((p2y - p3y) * (px - p3x) + (p3x - p2x) * (py - p3y)) / ((p2y - p3y) * (p1x - p3x) + (p3x - p2x) * (p1y - p3y))
	beta = ((p3y - p1y) * (px - p3x) + (p1x - p3x) * (py - p3y)) / ((p2y - p3y) * (p1x - p3x) + (p3x - p2x) * (p1y - p3y))
	gamma = 1.0 - alpha - beta
	if (alpha < 0 || beta < 0 || gamma < 0) {
		if (priority == 1) {
			priority1()
		}
		if (priority == 2) {
			priority2()
		}
		if (priority == 3) {
			priority3()
		}

	} else {
		for (i = 0; i < count; i++) {
			if (px < arrPosX[i] + 40 && px > arrPosX[i] - 40 && py < arrPosY[i] + 40 && py > arrPosY[i] - 40) {
				if (priority == 1) {
					priority1() 
				}
				if (priority == 2) {
					priority2() 
				}
				if (priority == 3) {
					priority3() 
				}
			}
		}
	}
}

function endAnimationFunction() {
	getPlacement() 
	$('.contentLeftBtn').addClass('throw-animation') 
}

function resertKeypadCtrl() {
	if (count != 0) {
		$(".icoReset").css({
			'opacity': '1',
			'cursor': 'pointer'
		}) 
	} else {
		for (i = 0; i < 13; i++) {
			if ($('.numTxt').eq(i).text() == '') {
				$(".icoReset").css({
					'opacity': '0.5',
					'cursor': 'pointer'
				}) 

			} else {
				$(".icoReset").css({
					'opacity': '1',
					'cursor': 'pointer'
				}) 
				break 
			}
		}
	}
}

function enableReset() {
	$(".icoReset").css({
		'opacity': '1',
		'cursor': 'pointer'
	}) 

}

function reset() {
	if ($(".icoReset").css('opacity') == 1) {
		$(".icoReset").removeClass('icoResetMove') 
		var to = setTimeout(function () {
			clearInterval(to) 
			$(".icoReset").addClass('icoResetMove') 
		}, 100)
		$(".icoReset").css({
			'opacity': '0.5',
			'cursor': 'default'
		})
		count = 0 
		numKeypad.hidePad() 
		$(".keypadContainer").css({
			'top': '345px',
			'left': '818px'
		})
		try {
			$(".icoReset").unbind('click', reset) 
		} catch (e) {}
		$('.contentRigthBtn').css({
			'opacity': '1'
		})
		$('.coin').css({
			'display': 'none',
			'top': '220px',
			'left': '205px'
		}) 
		$(".icoReset").bind('click', reset) 
		count = 0 
		arrPosX = [0, 0, 0, 0, 0, 0, 0, 0] 
		arrPosY = [0, 0, 0, 0, 0, 0, 0, 0] 
		priority = 0 
		probability = [0, 0, 0, 0] 
		for (i = 0; i < 13; i++) {
			$('.numTxt').eq(i).html('') 
		}
		tableArr = [] 
	}
}

function instruct() {
	$('.question,#blocker').hide() 
	$('.icoInfo').css({
		'opacity': '1'
	}) 
	try {
		$('#instructions-button-container').unbind('click', openInst) 
	} catch (e) {}
	$('#instructions-button-container').bind('click', openInst) 
}

function openInst() {
	$('.question,#blocker').show() 
	$('.icoInfo').css({
		'opacity': '0.5'
	}) 
	numKeypad.hidePad() 
}