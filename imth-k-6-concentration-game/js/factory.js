/*All card objects from any engine must be edited in the returning array so it can be customizable
EG: tenenginepoints has this cardObject:
{
it has 2 types in order to make a pair comparison of the value.
@type: either is 'operation','result',
template that must be implemented in the html
@template='point',
if a card is reverse
@reverse:true
}
*/
concentrationGameAdd.factory('engine',function(){
    var factory={};
    factory.busy=0;
    factory.options=options;
    factory.returnOp=function(){
        return factory.options.maxCards;
    }
    var op=factory.options.maxCards;

    factory.concentration_add_1_11=function(){
        var pool = [];
        var res=_.range(20,100,10);
        var rep=0;
        var random=_.sample(_.without(res,20),2)
        var possible=[];

        res.map(function(i,index,array){
            var eq=_.range(10,i,10).map(function(j){
                return [j,i-j];
            })
            var sample;
            if(random.indexOf(i)!=-1){
                sample=_.sample(eq,2);
            }else{
                sample=_.sample(eq,1);
            }
            sample.map(function(elem){
                possible.push({type:'result',value:i});
                possible.push({type:'operation',value:elem.join(' + ')});
            });
        })
        return _.shuffle(possible);
    }

    //factory.concentration_add_1_11();

    factory.subEngine=function(){
        var op2 =op=factory.options.maxCards/2;
        var max = 99;
        var min = 21;
        var t=max-min;
        var posibles = []
        var posible=[]
        var cards = []
        var max2 = 99;
        var min2 = 21;
        var t2=max2-min2;
        var posibles2 = []
        var posible2=[]
        for(var i =0 ; i<t;i++){
            posibles[i]= 21+i;
        }
        posible =factory.utils.shuffleArray(posibles)
        for(var i = 0;i<op;i++){
            cards.push({type:'result',value:posible[i] -posible[i]%10})
            cards.push({type:'operation',value:posible[i]+' — '+posible[i]%10})
        }
        for(var i =0 ; i<t;i++){
            posibles2[i]= 21+i;
        }
        posible2 =factory.utils.shuffleArray(posibles2)
        for(var i = 0;i<op2;i++){
            cards.push({type:'result',value: posible2[i] -Math.floor(posible2[i]/10)*10})
            cards.push({type:'operation',value: posible2[i]+' — '+Math.floor(posible2[i]/10)*10})
        }

        return factory.utils.shuffleArray(cards);
    }



    factory.fiveToTenMenu=function(templateName,ennunciated){
        var menuObj={template:templateName,ennunciated:ennunciated}
        var widthArray=[716,716,716,895,716,895];
        var arr=new Array();
        for(var i=5;i<11;i++){
            arr.push({label:i,value:i,template:'points',index:i-5,gridWidth:widthArray[i-5],ennunciated:' How many dots do I need to make '+i+'?'})
        }

        menuObj.menuItems=arr;
        return menuObj;
    }

    factory.addDecadesEngine=function(){
        //array de los posibles valores para la operacion
        var posibles =['10+10','10+20','10+30','10+40','10+50','10+60','10+70','10+80','20+10','20+20','20+30','20+40','20+50','20+60','20+70','30+10','30+20','30+30','30+40','30+50','30+60','40+10','40+20','40+30','40+40','40+50','50+10','50+20','50+30','50+40','60+10','60+20','60+30','70+10','70+20','80+10']
        /// el arreglo de las operaciones pasa a ser h
        var h = factory.utils.shuffleArray(posibles)
        // cards es un arreglo sera el consultado por el widget
        cards=[];
        for(var i = 0; i<10;i++){
            cards.push({type:'result',value:eval(h[i]).replace("—","-")})
            cards.push({type:'operation',value:h[i]})

        }
        return cards;
    }

    factory.tenEnginePointsReverse=function(){
        return factory.tenEnginePointsCustom('points',true);
    }

    factory.tenEnginePoints=function(){
        return factory.tenEnginePointsCustom('points',false);
    }
    factory.tenEnginePointsCustom=function(template,reverse,val){

        var posibles =[];
        for(var i=1, j=10; i<11; i++){
            var item1 = i;
            var item2 = j;
            posibles.push({value:i,type:'result',template:template,reverse:reverse});
            posibles.push({value:j,type:'operation',reverse:reverse});
            j--;
        }
        return _.shuffle(posibles);
    }
    factory.k6_concentration_game=function(){
        var poll10=_.range(1,11);
        console.log(poll10);
        var posibles=[];
        _.each(_.sample(poll10,10),function(ans){
            var temp=new Array()
            for(var j=0;j<10-ans+1;j++){
                temp.push([ans+j,j])
            }
            var tempSample=_.sample(temp)
            posibles.push({value:ans,type:'result'});
            posibles.push({value:tempSample.join('-'),type:'operation'});
            return ans;
        })

        console.log(posibles.length);
        return _.shuffle(posibles)
        //{value:i,type:'result',template:'points'}
    }

    factory.concentration_sub_1_11=function(){

        var poolSet1 = [],
        	poolSet2 = [],
        	posibles = [],
            key = _.shuffle(_.range(1,10));

        for (var i = 2; i < 10; i++) {
        	var dataSet1 = [],
        		dataSet2 = [];

        	for (var j = 1; j < 10; j++) {
        		var min = i*10+j;
        		dataSet1.push([min,j]);
        		if (j == key[i-2]) poolSet2.push([min,min-j]);
        	}
        	poolSet1.push(_.sample(dataSet1));
        };

    	var equationSet1 = _.sample(poolSet1,5),
    		equationSet2 = _.sample(poolSet2,5);

    	// for (var i = 0; i < equationSet1.length; i++) console.log('equationSet1',equationSet1[i]+' = ',equationSet1[i][0]-equationSet1[i][1]);
    	// for (var i = 0; i < equationSet2.length; i++) console.log('equationSet2',equationSet2[i]+' = ',equationSet2[i][0]-equationSet2[i][1]);

    	for (var i = 0; i < equationSet1.length; i++){
    		posibles.push({value:equationSet1[i][0]+' — '+equationSet1[i][1],type:'operation'},{value:equationSet1[i][0]-equationSet1[i][1],type:'result'});
    	}

    	for (var i = 0; i < equationSet2.length; i++){
    		posibles.push({value:equationSet2[i][0]+' — '+equationSet2[i][1],type:'operation'},{value:equationSet2[i][0]-equationSet2[i][1],type:'result'});
    	}

        return _.shuffle(posibles);
    }

    // for (var i = 0; i < 100; i++) {
    // 	factory.concentration_sub_1_11();
    // };

    // factory.concentration_sub_1_11();

    // parejas que sumen 10
    factory.tenEngine=function(){
        var posibles =[];
        var posibles =[];
        template=factory.options.template;
        for(var i=0, j=10; i<11; i++){
            var item1 = i;
            var item2 = j;
            posibles.push({value:i,type:'result'});
            posibles.push({value:j,type:'operation'});
            j--;
        }
        var randErase=_.sample(posibles);
        var toErase={
            value:10-randErase.value,
            type:randErase.type=='operation'?'result':'operation'
        }
        return _.shuffle(_.without(posibles,_.findWhere(posibles,toErase),randErase));
    }
    factory.game_addition_facts=function(){
        var posibles = [];

        for (var i = 1; i < 11; i++) {
            var a = Math.round(Math.random()*i),
            b = i-a,
            sum = _.shuffle([a,b]);

            posibles.push([{value:i,type:'result'},{value:sum[0]+' + '+sum[1],type:'operation'}]);
        };

        var r=[];
        _.each(_.sample(posibles,10),function(elm){
            r.push(elm[0]);
            r.push(elm[1]);
        })
        return _.shuffle(r);

    }

    factory.fiveToTenSubengine=function(param){
        //console.log(param);
        //param=JSON.parse(param);
        var posibles =[];
        var n=param.value;
        factory.options.check=n;
        for(var i=1, j=n; i<n; i++){
            j--;
            var item1 = i;
            var item2 = j;
            posibles.push({value:i,type:'result',template:'points'});
            posibles.push({value:j,type:'operation',template:'points'});
        }
        factory.options.maxCards=posibles.length/2;
        return _.shuffle(posibles);
    }


    /*factory.addToTen=function(){

      var op2 =op=factory.options.maxCards/2;

      for(var i = 0; i<10;i++){

      }
      return op2;

      }*/
    //console.log(factory.addToTen());
    //engine "add to 20"
    factory.substractionEngine=function(){
        var n=_.range(1,18);
        var a=_.range(0,10);
        //console.log(a.length,n.length)
        var cards=[];
        for(var i=0;i<10;i++ ){
            var min=i;
            var max=i+9;
            var rand=_.random(min,max);
            //console.log('s',rand,'m',(rand-i),'result will be ',i);
            cards.push({type:'result',value:i})
            cards.push({type:'operation',value:rand+"-"+(rand-i)});
        }
        return _.shuffle(cards);



    }

    factory.getSubstraend=function(dataset,minuend,res){
        var s=_.sample(dataset);
        //console.log('res::',res)
        //res,(minuend-s);
        //isInArray=._findWhere
        if(minuend<=s){
            return factory.getSubstraend(dataset,minuend,res);
        }else{
            //console.log('taken from',res,s,minuend);
            var isInArray=res.filter(function(elm){
                if(elm.type=='result'){
                    if(elm.value==minuend-s)return true;
                }
            })
            if(!isInArray.length) return s
                else console.log('must return something else!',isInArray[0].value,res)
                    return factory.getSubstraend(dataset,minuend,res);


            return s;
        }
    }


    factory.addEngine=function(){
        //array that handle the results
        var n1 = [];
        var n2 = [];
        //this will be the random numbers
        var num1 = [];
        var num2 = [];
        //temp array to push add strings
        var operation = [];
        //push results
        var result = [];
        //return cards
        var cards =[]
        //fill n1 with OP numbers
        for(var i = 0; i<op;i++){
            n1.push(i);
        }
        //fill n2 with OP numbers
        for(var i = 0; i<op;i++){
            n2.push(i)
        }
        //shuffles 2 arrays
        num1 = factory.utils.shuffleArray(n1)
        num2 = factory.utils.shuffleArray(n2)
        //return op * 2 operations
        for(var i = 0; i<op;i++){
            cards.push({type:'result',value:n1[i]+n2[i]})
            cards.push({type:'operation',value:n1[i]+"+"+n2[i]})
            /*result[i] = {value:n1[i]+n2[i],type:'result'};
              operation[i]={value:n1[i]+"+"+n2[i],type:'operation'};
              cards ={
              operation: operation,
              result: result
              }*/
        }
        return cards;

    }
    factory.utils={
        shuffleArray:function(array){
            for (var i = array.length - 1; i > 0; i--) {
                var j = Math.floor(Math.random() * (i + 1));
                var temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
            return array
        }
    }

    factory.startEngine=function(param){
        return _.shuffle(factory[factory.options.engine](param));
    }

    factory.getMenu=function(){
        return factory[factory.options.menu.fn](factory.options.menu.template,factory.options.menu.ennunciated);
    }



    factory.player=function(attrs){
        /*
         *score:{
corrrect:number,
wrong:number,
}
tiempo: number;
         *
         * */
        var self=this;
        self.data={}
        self.data.score={
            correct:0,
            wrong:0,
            tries:0
        }
        self.data.list=[];

        /*self.setData=function (attrs){
          for(var setting in attrs    ){
          if(attrs.hasOwnProperty(setting))
          self.data[setting] = attrs[setting];
          };
          }*/
        return self;
    }
    return factory;

})
