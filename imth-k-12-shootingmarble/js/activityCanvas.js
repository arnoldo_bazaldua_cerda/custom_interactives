$(function () {

	var ClickIpad = true
	var marbleImage = new Image()
	var marbleImageEasy = new Image()
	var marbleImageMedium = new Image()
	var marbleImageHard = new Image()
	var marbleImageVeryHard = new Image()
	var rightSlide = new Image()
	var pathImage = new Image()
	var bgImage = new Image()
	var gameSpeed = 1
	var mainCanvas = document.getElementById('bubbles')
	var mainContext = mainCanvas.getContext('2d')
		//mainCanvas.addEventListener('click', getPosition, false)
		//$('canvas-container').bind("tap", getPosition,false)
	document.addEventListener('DOMContentLoaded', function () {
		var fastClickButton = document.querySelector('.canvasResp');
		new FastClick(fastClickButton);
	});
	mainCanvas.addEventListener('click', getPosition, false)
	var pathCanvas = document.getElementById('path')
	var pathContext = pathCanvas.getContext('2d')
	pathImage.src = '../images/slide_background.png'
	var bgCanvas = document.getElementById('bg')
	var bgContext = bgCanvas.getContext('2d')
	bgImage.src = '../images/bg.jpg'
	var rsCanvas = document.getElementById('rs')
	var rsContext = rsCanvas.getContext('2d')
	rightSlide.src = '../images/right.png'
	var circles = []
	var numbers = []
	var num = 0
	var difficult
	var i = 0
	var k = 0
	var pause = false
	var actual
	var currentGameSpeed = 0
	var marble
	var clickMultiplier = 0
	var validateMarble = {
		validateClick: false
	}
	var pauseMarbles = true
	var marblePauseTimer = 0
	var canvasAnimation
	var winTimeout = 0
	var loseTimeout = 0
	var finishGame = false

	window.requestAnimationFrame = (function () {
		return window.requestAnimationFrame ||
			window.webkitRequestAnimationFrame ||
			window.mozRequestAnimationFrame ||
			window.msRequestAnimationFrame ||
			function (callback) {
				return window.setTimeout(callback, 0);
			};
	})();

	window.cancelAnimationFrame = (function () {
		return window.cancelAnimationFrame ||
			window.webkitCancelAnimationFrame ||
			window.mozCancelAnimationFrame ||
			window.msCancelAnimationFrame ||
			function (intervalKey) {
				window.clearTimeout(intervalKey);
			};
	})();

	// Firefox 1.0+
	var isFirefox = typeof InstallTrigger !== 'undefined';
	// At least Safari 3+: '[object HTMLElementConstructor]'
	var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
	// Internet Explorer 6-11
	var isIE = /* @cc_on!@ */ false || !!document.documentMode;

	function getPosition(event) {

		var x = event.x;
		var y = event.y;
		if (isFirefox || isIE) {
			x = event.offsetX == undefined ? event.layerX : event.offsetX;
			y = event.offsetY == undefined ? event.layerY : event.offsetY;
		}
		var offsetX = event.x
		var offsetY = event.y
		marblePauseTimer = 0
		var canvas = document.getElementById('bubbles')
		offsetX -= canvas.offsetLeft
		offsetY -= canvas.offsetTop
		validateMarble = removeMarble(x, y)
		if (validateMarble.validateClick) {
			marble = circles[validateMarble.marbleClicked]
			$('#num').addClass('numh')
			$('.correctAnswer').removeClass('layer-hide')
			if (window.matchMedia('(min-width: 769px)').matches) {
				$('.correctAnswer').text(marble.answer).css({
					top: marble.yPos + 22,
					left: marble.xPos + 22,
					visibility: 'visible'
				}).animate({
					top: '0.5%',
					left: '57.6%' //57.6
				}, 600, 'linear')
			} else {
				$('.correctAnswer').text(marble.answer).css({
					top: marble.yPos * 0.75,
					left: marble.xPos * 0.75 + 20,
					visibility: 'visible'
				}).animate({
					top: '0.4%',
					left: '56.5%'
				}, 600, 'linear')
			}
			k--
			circles.splice(validateMarble.marbleClicked, 1)
		}
	}

	function removeMarble(clickPosX, clickPosY) {

		pauseMarbles = true
		for (var i = 0; i < circles.length; i++) {
			if (window.matchMedia('(max-width: 768px)').matches) {
				if (circles[i].xPos < clickPosX / 0.75 && circles[i].xPos > clickPosX / 0.75 - 50 && circles[i].yPos < clickPosY / 0.75 && circles[i].yPos > clickPosY / 0.75 - 35 && circles[i].isClickable && ClickIpad) {
					$('.slingshot').css({
						left: clickPosX - 120
					})
					mainCanvas.removeEventListener('click', getPosition, false)
					ClickIpad = true
					$('.rock').css({
						top: $('.rock').offset().margin
					}).animate({
						top: clickPosY
					}, 250, 'linear')
					setTimeout(function () {
						$('.rock').addClass('layer-hide')
					}, 250)
					if (circles[i].answer == num) {
						audioCorrect.load()
						audioCorrect.play()
						audioS1.load()
						audioS1.play()
						return {
							validateClick: true,
							marbleClicked: i
						}
					} else {
						audioRebote.play()
					}
				}
			} else if (window.matchMedia('(min-width: 769px)').matches && window.matchMedia('(max-width: 960px)').matches) {
				if (circles[i].xPos < clickPosX / 0.95 && circles[i].xPos > clickPosX / 0.95 - 50 && circles[i].yPos < clickPosY / 0.95 + 10 && circles[i].yPos > clickPosY / 0.95 - 40 && circles[i].isClickable && ClickIpad) {

					$('.slingshot').css({
						left: clickPosX - 120
					})
					mainCanvas.removeEventListener('click', getPosition, false)
					ClickIpad = true
					$('.rock').css({
						top: $('.rock').offset().margin
					}).animate({
						top: clickPosY
					}, 250, 'linear')
					setTimeout(function () {
						$('.rock').addClass('layer-hide')
					}, 250)
					if (circles[i].answer == num) {
						audioS1.load()
						audioCorrect.play()
						audioS1.load()
						audioS1.play()
						return {
							validateClick: true,
							marbleClicked: i
						}
					} else {
						audioRebote.load()
						audioRebote.play()
					}
				}
			} else {
				if (isFirefox || isIE) {
					clickMultiplier = 0;
				} else {
					clickMultiplier = ($(window).width() - 1024) / 2
				}
				if (circles[i].xPos < clickPosX - clickMultiplier && circles[i].xPos > clickPosX - 50 - clickMultiplier && circles[i].yPos < clickPosY + 10 && circles[i].yPos > clickPosY - 35 && circles[i].isClickable && ClickIpad) {
					$('.slingshot').css({
						left: clickPosX - 125 - clickMultiplier
					})
					mainCanvas.removeEventListener('click', getPosition, false)
					ClickIpad = true
					$('.rock').css({
						top: $('.rock').offset().margin
					}).animate({
						top: clickPosY
					}, 250, 'linear')
					setTimeout(function () {
						$('.rock').addClass('layer-hide')
					}, 250)

					if (circles[i].answer == num) {
						audioCorrect.load()
						audioCorrect.play()
						audioS1.load()
						audioS1.play()
						return {
							validateClick: true,
							marbleClicked: i
						}
					} else {
						audioRebote.load()
						audioRebote.play()
					}
				}
			}
		}
		return {
			validateClick: false
		}
	}

	function initCanvas() {

		if (window.matchMedia('(max-width: 990px)').matches) {
			$('#bubbles').removeClass('bubblesL')
			$('#bubbles').addClass('bubblesP')

		}

		if (window.matchMedia('(min-width: 1020px)').matches) {
			$('#bubbles').removeClass('bubblesP')
			$('#bubbles').addClass('bubblesL')
		}
		marbleImageEasy.src = '../images/penguin1.png'
		marbleImageMedium.src = '../images/penguin3x.png'
		marbleImageHard.src = '../images/penguin2x.png'
		marbleImageVeryHard.src = '../images/penguin4.png'
		bgContext.drawImage(bgImage, 0, 0, 1070, 700)
		pathContext.drawImage(pathImage, 30, 57, 1024, 515)
		rsContext.drawImage(rightSlide, 821, 20, 220, 173)
		canvasAnimation = requestAnimationFrame(draw)
	}

	function draw() {
		i++
		marblePauseTimer++
		if (circles.length != 0) {
			if (circles[k].xPos < 840 && k < circles.length - 1) {
				k++
			}
			mainContext.clearRect(0, 0, 1024, 1081);


			if (validateMarble.validateClick && pauseMarbles) {

				for (var j = 0; j <= k; j++) {
					if (j < validateMarble.marbleClicked && validateMarble.marbleClicked != circles.length)
						circles[j].update(0)
					else
					if (validateMarble.marbleClicked == 0 || validateMarble.marbleClicked == circles.length)
						circles[j].update(gameSpeed)
					else
						circles[j].update(gameSpeed)
				}
				if (marblePauseTimer > (183 / (gameSpeed))) {
					ClickIpad = true
					$('.rock').removeClass('layer-hide')
					$('.rock').css('top', '')
					mainCanvas.addEventListener('click', getPosition, false);
					next()
					marblePauseTimer = 0
					pauseMarbles = false
				}
			} else {

				for (var j = 0; j <= k; j++) {
					circles[j].update(gameSpeed)
				}
				if (marblePauseTimer > (225 / (5 + gameSpeed))) {
					ClickIpad = true

					$('.rock').removeClass('layer-hide')
					$('.rock').css('top', '')
					mainCanvas.addEventListener('click', getPosition, false)
					marblePauseTimer = 0
					pauseMarbles = false
				}
			}

			if (circles[k].xPos < 840 && k < circles.length - 1) {
				k++
			}

			if (finishGame)
				mainContext.clearRect(0, 0, 1024, 1081)

			canvasAnimation = requestAnimationFrame(draw)
		} else {
			mainContext.clearRect(0, 0, 1024, 1080)

			$('.sling').css('visibility', 'hidden')
			$('.rock').addClass('layer-hide')

			if (window.matchMedia('(max-width: 1030px)').matches) {
				$('.penguin3').css({
					left: actual + 450
				})
			} else {
				$('.penguin3').css({
					left: actual + 150
				})
			}


			$('.penguin3').css('visibility', 'visible')
			$('.penguin3').addClass('walking')

			$('.cube').addClass('walking')
			audioAplausos.load()
			audioAplausos.play()
			setTimeout(function () {
				$('.penguin3').removeClass('walking').css({
					'visibility': 'hidden',
					'left': '400px'
				})
				$('.slingshot').addClass('layer-hide')

				//  audioWakeUp.load()
				//	audioWakeUp.play()

				$('#winScreen').removeClass('layer layer-hide').addClass('winScreen')
				$('.bear').css('display', 'none')
				$('.left-slide').css('display', 'none')

			}, 3500)
		}
	}

	function Circle(xPos, yPos, id) {
		this.id = id
		this.xPos = xPos
		this.yPos = yPos
		this.mWidth = 45
		this.mHeight = 45
		this.counter = 0
		this.isClickable = false
	}

	Circle.prototype.update = function (speedMultiplier) {
		checkRoutePosition[this.counter](this)
		var addX = routeArray[this.counter][0] * speedMultiplier
		var addY = routeArray[this.counter][1] * speedMultiplier
		mainContext.drawImage(marbleImage, this.xPos + addX, this.yPos + addY, 45, 45)
		mainContext.font = '24px mclarenregular'
		mainContext.fillStyle = 'white'
		mainContext.textAlign = 'center'
		mainContext.fillText(this.answer, this.xPos + (this.mWidth / 2.2) + addX, this.yPos + (this.mHeight / 1.7) + addY)
		this.xPos += addX
		this.yPos += addY
	}



	var a1 = function (currentMarble) {
		if (currentMarble.xPos < 792) {
			currentMarble.isClickable = true
			currentMarble.counter++
		}
	}

	var a2 = function (currentMarble) {
		if (currentMarble.xPos < 748) {
			currentMarble.counter++
		}
	}

	var a3 = function (currentMarble) {
		if (currentMarble.xPos < 710) {
			currentMarble.counter++
		}
	}
	var a4 = function (currentMarble) {
		if (currentMarble.xPos < 686) {
			currentMarble.counter++
		}
	}
	var a5 = function (currentMarble) {
		if (currentMarble.xPos < 656) {
			currentMarble.counter++
		}
	}
	var a6 = function (currentMarble) {
		if (currentMarble.xPos < 615) {
			currentMarble.counter++
		}
	}
	var a7 = function (currentMarble) {
		if (currentMarble.xPos < 570) {
			currentMarble.counter++
		}
	}

	var a8 = function (currentMarble) {
		if (currentMarble.xPos < 527) {
			currentMarble.counter++
		}
	}
	var a9 = function (currentMarble) {
		if (currentMarble.xPos < 488) {
			currentMarble.counter++
		}
	}
	var a10 = function (currentMarble) {
		if (currentMarble.xPos < 454) {
			currentMarble.counter++
		}
	}
	var a11 = function (currentMarble) {
		if (currentMarble.xPos < 427) {

			currentMarble.counter++
		}
	}
	var a12 = function (currentMarble) {
		if (currentMarble.xPos < 395) {
			currentMarble.counter++
		}
	}
	var a13 = function (currentMarble) {
		if (currentMarble.xPos < 353) {
			currentMarble.counter++
		}
	}
	var a14 = function (currentMarble) {
		if (currentMarble.xPos < 308) {
			currentMarble.counter++
		}
	}
	var a15 = function (currentMarble) {
		if (currentMarble.xPos < 265) {
			currentMarble.counter++
		}
	}
	var a16 = function (currentMarble) {
		if (currentMarble.xPos < 227) {
			currentMarble.counter++
		}
	}
	var a17 = function (currentMarble) {
		if (currentMarble.xPos < 203) {
			currentMarble.counter++
		}
	}
	var a18 = function (currentMarble) {
		if (currentMarble.xPos < 195) {
			currentMarble.counter++
		}
	}
	var a19 = function (currentMarble) {
		if (currentMarble.xPos > 205) {

			currentMarble.counter++
		}
	}
	var a20 = function (currentMarble) {
		if (currentMarble.xPos > 225) {
			currentMarble.counter++
		}
	}
	var a21 = function (currentMarble) {
		if (currentMarble.xPos > 257) {
			currentMarble.counter++
		}
	}
	var a22 = function (currentMarble) {
		if (currentMarble.xPos > 294) {
			currentMarble.counter++
		}
	}
	var a23 = function (currentMarble) {
		if (currentMarble.xPos > 337) {
			currentMarble.counter++
		}
	}
	var a24 = function (currentMarble) {
		if (currentMarble.xPos > 382) {

			currentMarble.counter++
		}
	}
	var a25 = function (currentMarble) {
		if (currentMarble.xPos > 427) {

			currentMarble.counter++
		}
	}
	var a26 = function (currentMarble) {
		if (currentMarble.xPos > 471) {
			currentMarble.counter++
		}
	}
	var a27 = function (currentMarble) {
		if (currentMarble.xPos > 513) {
			currentMarble.counter++
		}
	}
	var a28 = function (currentMarble) {
		if (currentMarble.xPos > 555) {
			currentMarble.counter++
		}
	}
	var a29 = function (currentMarble) {
		if (currentMarble.xPos > 596) {
			currentMarble.counter++
		}
	}
	var a30 = function (currentMarble) {
		if (currentMarble.xPos > 637) {
			currentMarble.counter++
		}
	}
	var a31 = function (currentMarble) {
		if (currentMarble.xPos > 680) {

			currentMarble.counter++
		}
	}
	var a32 = function (currentMarble) {
		if (currentMarble.xPos > 724) {

			currentMarble.counter++
		}
	}
	var a33 = function (currentMarble) {
		if (currentMarble.xPos > 767) {
			currentMarble.counter++
		}
	}
	var a34 = function (currentMarble) {
		if (currentMarble.xPos > 802) {
			currentMarble.counter++
		}
	}
	var a35 = function (currentMarble) {
		if (currentMarble.xPos > 826) {
			currentMarble.counter++
		}
	}
	var a36 = function (currentMarble) {
		if (currentMarble.xPos > 831) {
			currentMarble.counter++
		}
	}
	var a37 = function (currentMarble) {
		if (currentMarble.xPos < 815) {
			currentMarble.counter++
		}
	}
	var a38 = function (currentMarble) {
		if (currentMarble.xPos < 785) {
			currentMarble.counter++
		}
	}
	var a39 = function (currentMarble) {
		if (currentMarble.xPos < 744) {
			currentMarble.counter++
		}
	}
	var a40 = function (currentMarble) {
		if (currentMarble.xPos < 700) {
			currentMarble.counter++
		}
	}
	var a41 = function (currentMarble) {
		if (currentMarble.xPos < 655) {
			currentMarble.counter++
		}
	}
	var a42 = function (currentMarble) {
		if (currentMarble.xPos < 610) {

			currentMarble.counter++
		}
	}
	var a43 = function (currentMarble) {
		if (currentMarble.xPos < 565) {

			currentMarble.counter++
		}
	}
	var a44 = function (currentMarble) {
		if (currentMarble.xPos < 522) {
			currentMarble.counter++
		}
	}
	var a45 = function (currentMarble) {
		if (currentMarble.xPos < 478) {
			currentMarble.counter++
		}
	}
	var a46 = function (currentMarble) {
		if (currentMarble.xPos < 435) {
			currentMarble.counter++
		}
	}
	var a47 = function (currentMarble) {
		if (currentMarble.xPos < 392) {
			currentMarble.counter++
		}
	}
	var a48 = function (currentMarble) {
		if (currentMarble.xPos < 348) {
			currentMarble.counter++
		}
	}
	var ax = function (currentMarble) {
		if (currentMarble.xPos < 200) {
			currentMarble.counter++
				window.cancelAnimationFrame(canvasAnimation)
			gameSpeed = 0
			currentGameSpeed = 0
			finishGame = true
			summonBear()
		}
	}

	var axx = function (currentMarble) {}

	var checkRoutePosition = [
    a1,
    a2,
    a3,
    a4,
    a5,
    a6,
    a7,
    a8,
    a9,
    a10,
    a11,
    a12,
    a13,
    a14,
    a15,
    a16,
    a17,
    a18,
    a19,
    a20,
    a21,
    a22,
    a23,
    a24,
    a25,
    a26,
    a27,
    a28,
    a29,
    a30,
    a31,
    a32,
    a33,
    a34,
    a35,
    a36,
    a37,
    a38,
    a39,
    a40,
    a41,
    a42,
    a43,
    a44,
    a45,
    a46,
    a47,
    a48,
    ax,
    axx
  ]

	var routeArray = [
    [-0.22, 0], // a1
    [-0.22, 0.02], // a2
    [-0.1903, 0.1211], // a3
    [-0.1197, 0.1995], // a4
    [-0.15, 0.165], // a5
    [-0.20475, 0.08775], // a6
    [-0.225, 0.0225], // a7
    [-0.215, -0.043], // a8
    [-0.1947, -0.12367], // a9
    [-0.1698, -0.1415], // a10
    [-0.1352, -0.1730], // a11
    [-0.1601, -0.1611], // a12
    [-0.21, -0.0756], // a13  
    [-0.2243, 0.0152], // a14    
    [-0.2156, 0.0686], // a15
    [-0.19, 0.1235], // a16
    [-0.1194, 0.199], // a17
    [-0.04, 0.2175], // a18
    [0.0501, 0.2166], // a19
    [0.1003, 0.2006], // a20
    [0.1599, 0.17425], // a21
    [0.185, 0.1202], // a22
    [0.215, 0.0777], // a23
    [0.22567, 0.0256], // a24
    [0.2244, -0.0193], // a25
    [0.2203, -0.0283], // a26
    [0.2101, -0.0764], // a27
    [0.2101, -0.0764], // a28
    [0.2046, -0.093], // a29
    [0.2046, -0.093], // a30
    [0.2156, -0.0532], // a31
    [0.2201, -0.0058], // a32
    [0.2148, 0.0626], // a33
    [0.1746, 0.1358], // a34
    [0.1199, 0.1962], // a35
    [0.0248, 0.226], // a36
    [-0.0802, 0.214], // a37
    [-0.1498, 0.1665], // a38
    [-0.2053, 0.0859], // a39
    [-0.2196, 0.0334], // a40
    [-0.2257, 0.0279], // a41
    [-0.2244, 0.0], // a42
    [-0.2257, -0.0107], // a43
    [-0.2142, -0.051], // a44
    [-0.2205, -0.0525], // a45
    [-0.2152, -0.0563], // a46
    [-0.2142, -0.0663], // a47
    [-0.22, -0.065], // a48
    [-0.23, 0], // ax
    [0, 0] // axx
  ]

	function drawCircles() {
		circles = []
		var marbleXPosition = 0
		var marbleYPosition = 0
		var marble1x = 839.64
		var marble2x = 799.64
		var marble3x = 758.64
		var marble4x = 721.64
		var marble1y = 110
		var marble2y = 110
		var marble3y = 113.31
		var marble4y = 130.01
		if (window.innerWidth >= 1024) {
			marbleXPosition = mainCanvas.width * 0.86
			marbleYPosition = 110
		} else {
			marbleXPosition = mainCanvas.width * 0.865
			marbleYPosition = 120
		}
		var circle1 = new Circle(marble1x, marble1y, 0)
		var circle2 = new Circle(marble2x, marble2y, 1)
		var circle3 = new Circle(marble3x, marble3y, 2)
		var circle4 = new Circle(marble4x, marble4y, 3)
		circles.push(circle4)
		circles.push(circle3)
		circles.push(circle2)
		circles.push(circle1)
		for (var i = 4; i < 20; i++) {
			var circle = new Circle(marbleXPosition, marbleYPosition, i)
			circles.push(circle)
		}
	}

	function summonBear() {
		if (!pause) {
			$('.bear').css('display', 'none')
			$('.rock').addClass('layer-hide')
			$('.left-slide').css('background', 'none')
			$('.bearWalking').css('left', '1500px').css('visibility', 'visible')
			$('.sling').css('visibility', 'hidden')
			$('.sling2').removeClass('layer-hide')
				//$('.slingshot').addClass('layer-hide')
			$('.penguin2').css('visibility', 'visible')
			$('.penguin2').css({
					left: $('.penguin2').position().left
				}).animate({
					left: '500%'
				}, 1600, 'linear')
				//audioOsoPinguino.load()
				//audioOsoPinguino.play()
			loseTimeout = setTimeout(function () {
				//audioSorry.load()
				//audioSorry.play()
				$('#container').css('pointer-events', 'inherit')
				$('.feed-screen').css('display', 'block')
			}, 3500)
		}
	}

	function generateNumbers() {
		switch (difficult) {
		case 'Easy':
			marbleImage = marbleImageEasy
			currentGameSpeed = 1
			break
		case 'Medium':
			marbleImage = marbleImageMedium
			currentGameSpeed = 1.45
			break
		case 'Hard':
			marbleImage = marbleImageHard
			currentGameSpeed = 1.95
			break
		case 'VeryHard':
			marbleImage = marbleImageVeryHard
			currentGameSpeed = 1.95
			break
		}
		for (var i = 0; i < 20; i++) {
			numbers.push(parseInt(Math.random() * 20 + 1, 10))
		}
	}

	function getNumber() {
		var index = parseInt(Math.random() * numbers.length, 10)
		num = numbers[index]
		numbers.splice(index, 1)
	}

	function init() {
		finishGame = false
		$('.feed-screen').css('display', 'none')
		$('#winScreen').addClass('layer-hide')
		$('.sling2').addClass('layer-hide')
		validateMarble = {
			validateClick: false
		}
		pauseMarbles = true
		k = 0
		marblePauseTimer = 0
		$('#num').addClass('numh')
		$('.correctAnswer').addClass('layer-hide')
		numbers = []
		generateNumbers()
		gameSpeed = currentGameSpeed
		setNumbers()
		next()
		i = 0
		$('.rock').removeClass('layer-hide')
		$('.left-slide').css('display', 'block')
		$('.penguin3').removeClass('walking').css({
			'visibility': 'hidden',
			'left': '400px'
		})
		$('.penguin2').css('visibility', 'hidden')
		$('.penguin2').removeClass('walking')
		$('.cube').removeClass('walking')
		$('.bear').css('display', 'block')
		$('.bearWalking').css({
			'visibility': 'hidden',
			'left': '0px'
		})
		$('.sling').css('visibility', 'visible')
		$('.penguin2').removeClass('walking').css({
			'visibility': 'hidden',
			'left': '100px'
		})
		$('.slingshot').removeClass('layer-hide').css('left', '')
		$('#game').removeClass('layer-hide').addClass('game')
		$('#difficulty').removeClass('difficulty').addClass('layer-hide')
	}

	function next() {
		if (numbers.length != 0) {
			getNumber()
			$('.correctAnswer').addClass('layer-hide')
			$('#num').removeClass('numh')
			getOperandos()
		}
	}

	function getOperandos() {
		var op1, op2
		do {
			op1 = parseInt(Math.random() * 20, 10)
		} while (op1 >= num)
		op2 = Math.abs(op1 - num)
		$('#num1').html(op1)
		$('#num2').html(op2)
		$('.sign').removeClass('layer-hide')
	}

	function setNumbers() {
		for (var i = 1; i <= numbers.length; i++) {
			$('#div' + i).html(numbers[i - 1]).removeClass('hide').addClass(' marble')
			circles[i - 1].answer = numbers[i - 1]
		}
	}

	var closeButton = document.querySelector('#closeButton')
	closeButton.addEventListener('click', closeInstructions)

	$('.play').on('click', function () {
		$('#winScreen').removeClass('winScreen').addClass('layer layer-hide')
		$('.feed-screen').css('display', 'none')
		$('#game').removeClass('game').addClass('layer-hide')
		$('#difficulty').removeClass('layer-hide').addClass('difficulty')
		audioSorry.pause()
		audioSorry.pause()
		audioAplausos.pause()
		audioWakeUp.pause()
		audioAplausos.currentTime = 0
		audioWakeUp.currentTime = 0
	})

	$('.icon-reset').on('click', function () {
		window.cancelAnimationFrame(canvasAnimation)
		$('.icon-reset').addClass('icoResetMove')
		setTimeout(function () {
			$('.icon-reset').removeClass('icoResetMove')
		}, 1000)
		clearTimeout(winTimeout)
		clearTimeout(loseTimeout)
		$('.feed-screen').css('display', 'none')
		$('#game').removeClass('game').addClass('layer-hide')
		$('#difficulty').removeClass('layer-hide').addClass('difficulty')
	})



	function closeInstructions() {
		pause = false
		$('#instructionsText').removeClass('layer').addClass('layer layer-hide')
		gameSpeed = currentGameSpeed
		stopInstructions()
	}

	function stopInstructions() {
		audio.pause()
		audio.currentTime = 0
	}


	var infoButton = document.querySelector('#infoButton')
	infoButton.addEventListener('click', showInstructions)


	var audioButton = document.querySelector('#audioButton')
	audioButton.addEventListener('click', audioInstructions)

	function audioInstructions() {
		audio.pause()
		audio.currentTime = 0
		audio.load()
		audio.play()
	}

	function showInstructions() {
		pause = true
		audioInstructions()
		$('#instructionsText').removeClass('layer layer-hide').addClass('layer')
		gameSpeed = 0;
	}

	var easyButton = document.querySelector('#Easy')
	easyButton.addEventListener('click', difficultButton)
	var mediumButton = document.querySelector('#Medium')
	mediumButton.addEventListener('click', difficultButton)
	var hardButton = document.querySelector('#Hard')
	hardButton.addEventListener('click', difficultButton)
	var veryhardButton = document.querySelector('#VeryHard')
	veryhardButton.addEventListener('click', difficultButton)

	function difficultButton() {
		$('#num1').html('')
		$('#num2').html('')
		difficult = $(this).attr('id')
		window.cancelAnimationFrame(canvasAnimation)
		k = 0
		i = 0
		gameSpeed = 0
		currentGameSpeed = 0
		mainCanvas.addEventListener('click', getPosition, false)
		drawCircles()
		initCanvas()
		init()
	}



}())