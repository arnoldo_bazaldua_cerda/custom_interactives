
var count = 0
var arrPosX = [0, 0, 0, 0, 0, 0, 0, 0]
var arrPosY = [0, 0, 0, 0, 0, 0, 0, 0]
var priority = 0
var probability = [0, 0, 0, 0]
var tableArr = []
$(document).ready(function () {
	try {
		$(".icoReset").unbind('click', reset)
	} catch (e) {}
	$(".icoReset").bind('click', reset)

	numKeypad = new vKeyPad(".numTxt", "#vkeyCont", {
		useComplexMath: false,
		colCount: 3,
		useParseMath: false,
		deleteMethod: "byChar",
		isForceTaggable: true,
		isMultiline: false,
		maximumLength: 5,
		maxLenMargin: 40,
		clearZero: false,
		multiSymbol: "\u00D7",
	})

	$('.coin').eq(0).css({
		'top': '40px',
		'left': '415px'
	})
	$('.coin').eq(1).css({
		'top': '110px',
		'left': '510px'
	})
	$('.coin').eq(2).css({
		'top': '105px',
		'left': '545px'
	})
	$('.coin').eq(3).css({
		'top': '215px',
		'left': '410px'
	})
	$('.coin').eq(4).css({
		'top': '260px',
		'left': '510px'
	})
	$('.coin').eq(5).css({
		'top': '195px',
		'left': '600px'
	})
	$('.coin').eq(6).css({
		'top': '290px',
		'left': '360px'
	})
	$('.coin').eq(7).css({
		'top': '340px',
		'left': '620px'
	})


	numKeypad.textLength = function (htmlText) {
		var textElem = $("<span>" + htmlText + "</span>")
		var textLen = textElem.text().replace(/\\s|-|(\+)|\./g, "").length
		textElem.remove()
		var iid = numKeypad.CurrentFocus.id
		temp = $("#" + iid).text()
		if (temp.charAt(2) == " " && temp.charAt(4) == " " && temp.charAt(7) == " " && temp.charAt(9) == " ") {
			numKeypad.maximumLength = 12
		}
		return textLen
	}
	numKeypad.onTextBoxFocus = function () {
		if (numKeypad.CurrentFocus.className == "numTxt multi") {
			numKeypad.setKeyState("multi", true, false)
		} else if (numKeypad.CurrentFocus.className == "numTxt oneChar") {
			numKeypad.setKeyState("multi", false, false)
		} else {
			numKeypad.setKeyState("multi", false, false)
		}
	}
	numKeypad.onTextChanged = function () {
		resertKeypadCtrl()
	}
})

function resertKeypadCtrl() {
	if (count != 0) {
		$(".icoReset").css({
			'opacity': '1',
			'cursor': 'pointer'
		})
	} else {
		for (i = 0; i < 13; i++) {
			if ($('.numTxt').eq(i).text() == '') {
				$(".icoReset").css({
					'opacity': '0.5',
					'cursor': 'pointer'
				})
			} else {
				$(".icoReset").css({
					'opacity': '1',
					'cursor': 'pointer'
				})
				break
			}
		}
	}
}

function enableReset() {
	$(".icoReset").css({
		'opacity': '1',
		'cursor': 'pointer'
	})
}

function reset() {
	if ($(".icoReset").css('opacity') == 1) {
		$(".icoReset").removeClass('icoResetMove')
		var to = setTimeout(function () {
			clearInterval(to)
			$(".icoReset").addClass('icoResetMove')
		}, 100)
		$(".icoReset").css({
			'opacity': '0.5',
			'cursor': 'default'
		})
		count = 0
		numKeypad.hidePad()
		try {
			$(".icoReset").unbind('click', reset)
		} catch (e) {}
		$(".icoReset").bind('click', reset)
		count = 0
		arrPosX = [0, 0, 0, 0, 0, 0, 0, 0]
		arrPosY = [0, 0, 0, 0, 0, 0, 0, 0]
		priority = 0
		probability = [0, 0, 0, 0]
		for (i = 0; i < 13; i++) {
			$('.numTxt').eq(i).html('')
		}
		tableArr = []
		$('.keypadContainer').css({
			top: "345px",
			left: "818px"
		})
	}
}

function instruct() {
	$('.question,#blocker').hide()
	$('.icoInfo').css({
		'opacity': '1'
	})
	try {
		$('#instructions-button-container').unbind('click', openInst)
	} catch (e) {}
	$('#instructions-button-container').bind('click', openInst)
}

function openInst() {
	$('.question,#blocker').show()
	$('.icoInfo').css({
		'opacity': '0.5'
	})
	numKeypad.hidePad()
}