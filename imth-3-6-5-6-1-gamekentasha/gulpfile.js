'use strict';

var gulp = require('gulp');
var server = require('gulp-server-livereload');
var csslint = require('gulp-csslint');
var  standard = require('gulp-standard');


gulp.task('standard', function () {
  return gulp.src(['js/activityCanvas.js'])
    .pipe(standard({
       "globals": [ "$","numbers","rightNumber","leftNumber","num","gameTimer","gameTimerTotal","i","tiempo","pause","closeButton","k","infoButton","audioButton"]
    }))
    .pipe(standard.reporter('default', {
      breakOnError: true
    }))
})

gulp.task('css', function() {
  gulp.src('./css/*.css')
    .pipe(csslint())
    .pipe(csslint.reporter());
});

gulp.task('watch', function () {
  gulp.watch('./js/activityCanvas.js', ['standard']);
  gulp.watch('./css/styles.css', ['css']);
})

gulp.task('webserver', function() {
  gulp.src('.')
    .pipe(server({
      livereload: true,
      directoryListing: true,
      open: true
    }));
});

gulp.task('default',['css','watch','webserver'],function(){})
