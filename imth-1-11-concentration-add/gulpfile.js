'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var server = require('gulp-server-livereload');
var uglify = require('gulp-uglify'); 
var jsmin = require('gulp-jsmin');
var rename = require('gulp-rename');
var standard = require('gulp-standard')

gulp.task('sass', function () {
  gulp.src('./sass/styles.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css')) ;
});

gulp.task('watch', function () {
  gulp.watch('./sass/**/*.scss', ['sass']);
  gulp.watch('./js/activity.js', ['standard']);
  gulp.watch(['./js/activity.js','./test/activitySpec.js'], ['test']);
})

gulp.task('compress:js', function() {
  return gulp.src(['./js/jquery-2.1.4.min.js.js','./js/jquery-ui.min.js"'])
    .pipe(jsmin())
    .pipe(rename('.lib.min.js'))
    .pipe(gulp.dest('js'));
});

gulp.task('standard', function () {
  return gulp.src(['./js/activity.js'])
    .pipe(standard({
       "globals": [ "$"]
    }))
    .pipe(standard.reporter('default', {
      breakOnError: true
    }))
})
 
gulp.task('webserver', function() {
  gulp.src('.')
    .pipe(server({
      livereload: true,
      directoryListing: true,
      open: true
    }));
});

gulp.task('default',['sass','watch'],function(){})