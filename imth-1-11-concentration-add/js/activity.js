// avity.js
$(function () {
  var value_three, three
  var listElements = $('#numbers .numbers-element')
  var $resetButton = $('#cleanButton')
  var $equations = $('.static-equation')
  var $resultValue = $('#valueThree')

  // audio
  var audio = document.getElementById('audio')

  // button controlls
  var closeButton = document.querySelector('#closeButton')
  var infoButton = document.querySelector('#infoButton')
  var audioButton = document.querySelector('#audioButton')

  closeButton.addEventListener('click', closeInstructions)
  infoButton.addEventListener('click', showInstructions)
  audioButton.addEventListener('click', audioInstructions)

  function audioInstructions () {
    audio.currentTime = 0
    audio.play()
  }

  function stopInstructions () {
    audio.pause()
    audio.currentTime = 0
  }

  function closeInstructions () {
    $('#instructionsText').toggleClass('layer layer-hide')
    stopInstructions()
  }

  function showInstructions () {
    audioInstructions()
    $('#instructionsText').toggleClass('layer layer-hide')
  }

/*
** NUMBERS
*/

/**
 * Drag and Drop function for the numbers.
 * The dropped value is also draggable and it validates if
 * there is a dropped value to delete from the results area.
 * @function
 * @name dragNum
 * @param
 */

  var $dragNum = $('#numbers .numbers-element')

  $dragNum.on('touchstart', function (e) {
    $($dragNum).draggable({
      helper: 'clone',
      revert: 'invalid'
    }, false)
  })

  $('#results .numbers-element').droppable({
    drop: function (event, ui) {
      var droppable = $(this)
      var innerDraggable = ui.draggable[0].innerText
      var draggable = $(ui.draggable).clone().addClass('numbers-element-dragged')
      $(droppable).innerText = innerDraggable
      $(droppable).html(draggable)

      $resetButton.removeClass('icon-reset-inactive')
      clearValues(ui.draggable[0].innerText)
      $(draggable).draggable({
        helper: 'clone'
      })
      // var x = $('.monsters').data('slide')
      event.preventDefault()
    },
    out: function (event, ui) {
      var draggable = ui.draggable.clone()
      $(draggable).fadeOut(1000)
      if (draggable[0].isEqualNode($resultValue.find('.numbers-element.ui-draggable.ui-draggable-handle')[0])) {
        $($resultValue.find('.numbers-element.ui-draggable.ui-draggable-handle')[0]).remove()
        if ($('.monster-laying').length === 0) {
          $resetButton.addClass('icon-reset-inactive')
        }
      }
      event.preventDefault()
    }
  })

  $resetButton.on('click', function () {
    var x = $('.monsters').data('slide')
    if (!$(this).hasClass('icon-reset-inactive')) {
      if (x === parseInt($('.monsters').attr('data-slide'), 10)) {
        $($('.monsters')[x - 1]).find('.monster').removeClass('monster-shake monster-laying')
        $resetButton.addClass('icon-reset-inactive')
        $($equations[x - 1]).find('.numbers__resultNumber').text('')
      }
    }
  })

/**
 * This function will trigger the touch event so that the list can be draggable
 * @function
 * @name initList
*/
  function initList () {
    try {
      listElements.trigger('touchstart')
    } catch (e) {}
  }

  initList()

/**
 * This function will check if there is a value in the droppable area, the clear button will be activated.
 * @function
 * @name clearValues
 */
  function clearValues (value) {
    three = value || 0
    value_three = parseInt(three, 10)
    console.trace('inactive')
    if (value_three !== '') {
      $resetButton.removeClass('icon-reset-inactive')
    } else {
      $resetButton.addClass('icon-reset-inactive')
    }
  }

/*
Comienza juan.c.gamboa
*/

//  Tumba al monstruo
  $('.monster').click(function () {
    var $this = $(this)

    $this.addClass('monster-shake')
    setTimeout(function () {
      $this.addClass('monster-down')
    }, 200)
    setTimeout(function () {
      $this.addClass('monster-laying')
      $this.removeClass('monsters-hide monster-down')
    }, 200)
    $resetButton.removeClass('icon-reset-inactive')
  })

// Boton Next
  $('body').on('click', '.arrow.arrow-next', function () {
    var $slide = $($('.monsters')[0]).data('slide')
    var $monsters = $('.monsters')

    if ($slide < 6) {
      var x = $('.monsters').data('slide') + 1
      var $nextEquation = $($equations[x - 1])
      var $beforeEquation = $($equations[x - 2])
      var $resultTmp = $($equations[$slide]).find('.numbers__resultNumber').text()
      var $nextMonster = $($monsters[$slide])
      var $beforeMonster = $($monsters[$slide - 1])
      var $falledMonsters = $($monsters[$slide]).find('.monster-laying')

      if ($resultTmp !== '' || $falledMonsters.length > 0) {
        $resetButton.removeClass('icon-reset-inactive')
      } else {
        $resetButton.addClass('icon-reset-inactive')
      }

      $($('.monsters')[0]).data('slide', x)
      $($('.monsters')[0]).attr('data-slide', x)
      $('.arrow.arrow-prev.arrow-hide').removeClass('arrow-hide')
      // $('.monster').removeClass('monster-shake monster-down');

      $nextEquation.removeClass('static-equation-hide')
      $beforeEquation.addClass('static-equation-hide')
      $nextMonster.removeClass('monsters-hide')
      $beforeMonster.addClass('monsters-hide')

      if ($slide === 5) {
        $('.arrow.arrow-next').addClass('arrow-hide')
        $('.arrow.arrow-prev.arrow-hide').removeClass('arrow-hide')
      }

      $('.slide.slide-active').removeClass('slide-active').addClass('slide-visited').next().addClass('slide-active')
    }
  })

// Boton Prev
  $('body').on('click', '.arrow.arrow-prev', function () {
    var $slide = $('.monsters').data('slide')
    var $monsters = $('.monsters')

    if ($slide > 1) {
      var x = $('.monsters').data('slide') - 1
      var $beforeEquation = $($equations[x])
      var $nextEquation = $($equations[x - 1])
      var $resultTmp = $($equations[$slide - 2]).find('.numbers__resultNumber').text()
      var $nextMonster = $($monsters[x - 1])
      var $beforeMonster = $($monsters[x])
      var $falledMonsters = $($monsters[x - 1]).find('.monster-laying')

      if ($resultTmp !== '' || $falledMonsters.length > 0) {
        $resetButton.removeClass('icon-reset-inactive')
      } else {
        $resetButton.addClass('icon-reset-inactive')
      }

      $('.monsters').data('slide', x)
      $('.monsters').attr('data-slide', x)
      $('.monster').removeClass('monster-shake')
      // clearValues()
      $nextEquation.removeClass('static-equation-hide')
      $beforeEquation.addClass('static-equation-hide')
      $nextMonster.removeClass('monsters-hide')
      $beforeMonster.addClass('monsters-hide')

      if ($('.monsters').data('slide') === 1) {
        $('.arrow.arrow-prev').addClass('arrow-hide')
      }
      if ($('.monsters').data('slide') <= 6) {
        $('.arrow.arrow-next.arrow-hide').removeClass('arrow-hide')
      }

      $('.slide.slide-active').removeClass('slide-active').addClass('slide-visited').prev().addClass('slide-active')
    }
  })

/*
Termina juan.c.gamboa
*/
}())
