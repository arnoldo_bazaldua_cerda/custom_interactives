'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var server = require('gulp-server-livereload');
var csslint = require('gulp-csslint');
var scsslint = require('gulp-scss-lint'); 
var  standard = require('gulp-standard');

 
gulp.task('standard', function () {
  return gulp.src(['js/activityCanvas.js'])
    .pipe(standard({
       "globals": [ "$","numbers","rightNumber","leftNumber","num","gameTimer","gameTimerTotal","i","tiempo","pause","closeButton","k","infoButton","audioButton"]
    }))
    .pipe(standard.reporter('default', {
      breakOnError: true
    }))
})
 
gulp.task('css', function() {
  gulp.src('./stylesheet/*.css')
    .pipe(csslint())
    .pipe(csslint.reporter());
});

gulp.task('watch', function () {
  gulp.watch('./js/activityCanvas.js', ['standard']);
  gulp.watch('./stylesheet/styles.css', ['css']);
})
 
/*
gulp.task('sass', function () {
  gulp.src('./sass/styles.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./stylesheet')) ;
});
 */
 
gulp.task('webserver', function() {
  gulp.src('.')
    .pipe(server({
      livereload: true,
      directoryListing: true,
      open: true
    }));
});

gulp.task('default',['css','watch','webserver'],function(){})